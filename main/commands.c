/* TODO
 *
 * Messages passed to a command task is by referece. The memory is allocated in the read_message_queue_task. This should be freed at the end of the task.
 * Task handles are not used... taskdelete does not return handle to null. This makes tracking of task running status basedf on the handle not possible...
 * The command list tree is an ugly solution. Would be easier and to allocate a [26][26] array to encompass all two letter commands.
 */
//#include "sensor_ads131m04_functions.h"
#include "main.h"
#include "commands.h"
#include "misc.h"
#include "flash_functions.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_spp_api.h"

/* ============== */
/* Private Macros */
/* ============== */
// Print to the function name, a tag, and the messsage length and value
#define COMMAND_HELPER(msg) ESP_LOGI(__func__, "Handle: %d Length: %d Message:%s", msg->handle, msg->length, msg->message)

/* Private Typedefs */
typedef struct{
  uint8_t branch_id;
  uint8_t num_twigs;
  twig_t **twigs;
}branch_t;

typedef branch_t* tree; // Explantion of structure at bottom of file

/* ================= */
/* Private Variables */
/* ================= */
tree command_list[26];

/* ================ */
/* Public Functions */
/* ================ */
// Lookup if command exists and start associated task if found
// Note this curently relies on the fact that commands are always [A-Z][A-Z]
// Will need to change if not the case
int32_t lookup_command(message_package_t *new_message){
  bool command_found = FALSE;

  // Check command chars between A-Z
  if(new_message->message[COMMAND_LOWER_BYTE_POS] < 'A' || new_message->message[COMMAND_LOWER_BYTE_POS] > 'Z' ){
    return -1;
  }
  if(new_message->message[COMMAND_UPPER_BYTE_POS] < 'A' || new_message->message[COMMAND_UPPER_BYTE_POS] > 'Z' ){
    return -2;
  }
  // Jump to branch i.e. first command letter branch
  branch_t* branch_address = command_list[(new_message->message[COMMAND_LOWER_BYTE_POS] - 'A')];

  // Check branch id matches lower command char
  if(branch_address->branch_id != new_message->message[COMMAND_LOWER_BYTE_POS]){
    return -3;
  }
  // Check if branch has twigs
  if(branch_address->num_twigs == 0){
    return -4;
  }
  // Find twig for second command char
  uint32_t i;
  for(i = 0; i < branch_address->num_twigs; i++){
    if(branch_address->twigs[i] != NULL){
      if(new_message->message[COMMAND_UPPER_BYTE_POS] == branch_address->twigs[i]->twig_id
	 &&  branch_address->twigs[i]->leaf_function != NULL){
	command_found = TRUE;
	break;
      }
    }
  }
  if(command_found == TRUE){
    xTaskCreate(branch_address->twigs[i]->leaf_function, "", 4096, (void *) new_message, 10, &(branch_address->twigs[i]->handle));
    return 0; // Task started
  }
  return -5; // Command not found
}
/* ================= */
/* Private Functions */
/* ================= */
// Functions associated with command. Most are templates that send a standard reply.

void command_AD(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_AS(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_AT(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_AV(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

/**************/
/* Disable BT */
/**************/
// TODO currently no checks to see if BT init/deinit before deinit/init
void command_BF(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    // Disable BT
    esp_err_t err = esp_spp_deinit();
    if(err != ESP_OK){
      ESP_LOGE(__func__, "Failed to deinit BT");
    }
    
    xEventGroupClearBits(deviceStatusGroup, BT_ACTIVE);
    
    vTaskDelete(NULL);
  }
}

/*************/
/* Enable BT */
/*************/
void command_BN(message_package_t *message_package){
  for(;;){
    COMMAND_HELPER(message_package);
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    // Enable BT
    esp_err_t err = esp_spp_init(ESP_SPP_MODE_CB);
    if(err != ESP_OK){
      ESP_LOGE(__func__, "Failed to init BT");
    }
    
    vTaskDelete(NULL);
  }
}

void command_CC(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_CM(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_CR(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_CU(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

/***************/
/* Device Info */
/***************/
void command_DI(message_package_t *message_package){
  for(;;){
    COMMAND_HELPER(message_package);
    /* Custom device info */
    message_package->length = 23;
    // Fixed Header
    message_package->message[5]  =  0x08;
    message_package->message[6]  =  '*';
    message_package->message[7]  =  '$';
    // Device info
    message_package->message[8]   =  '0';  // V1
    message_package->message[9]   =  '1';  // V2
    message_package->message[10]  =  'I'; //DEVICE CODE
    message_package->message[11]  =  '4'; // SV1
    message_package->message[12]  =  '1'; // SV2
    message_package->message[13]  =  '0'; // BUILD1
    message_package->message[14]  =  '0'; // BUILD2
    message_package->message[15]  =  '5'; // BUILD2
    message_package->message[16]  =  '0'; // BUILD2
    message_package->message[17]  =  'X'; // MCU type
    message_package->message[18]  =  (device_state.device_usage_count >> 24) & 0xFF; // Usage count bytewise 
    message_package->message[19]  =  (device_state.device_usage_count >> 16) & 0xFF;
    message_package->message[20]  =  (device_state.device_usage_count >>  8) & 0xFF;
    message_package->message[21]  =  (device_state.device_usage_count >>  0) & 0xFF;
    message_package->message[22]  =  MSG_FOOTER;

    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

/***************/
/* Display VIN */
/***************/
// TODO: check functionality
void command_DV(message_package_t *message_package){
  uint32_t num_chars;
  for(;;){
    COMMAND_HELPER(message_package);
    
    num_chars = message_package->message[2] >> 4;
    free(device_state.VIN_number);
    device_state.VIN_number = calloc(sizeof(char), MAX_VIN_LENGTH + 1); // Extra char for null termination
    device_state.VIN_number[MAX_VIN_LENGTH] = '\0'; // Null terminate
    device_state.VIN_length = num_chars;
    device_state.VIN_number[num_chars] = '\0'; // Null terminate

    // Code adapter from STM32 EZ Speed
    device_state.VIN_number[0]='1';
    //The remaining device_state.VIN_number number is encoded into each sequential group of six bits
    device_state.VIN_number[1]=(char) (((((message_package->message[2]&0xF) << 2) & 0x3C)|((message_package->message[3] & 0xC0) >> 6)) + 48);
    device_state.VIN_number[2]=(char) ((message_package->message[3]&0x3F) + 48);
    device_state.VIN_number[3]=(char) ((message_package->message[4] >> 2) + 48);
    device_state.VIN_number[4]=(char) ((((message_package->message[4] << 4) & 0x30)|(message_package->message[5] >> 4)) + 48);
    if(num_chars>4){
      device_state.VIN_number[5]=(char) (((((message_package->message[5] & 0xF) << 2) & 0x3C)|(message_package->message[6] >> 6)) + 48);
      if(device_state.VIN_number>5){
	device_state.VIN_number[6]=(char)((message_package->message[6]&0x3F) + 48);
      }else{
	device_state.VIN_number[6]='|';
      }
    }else{
      device_state.VIN_number[5]='|';
      device_state.VIN_number[6]='|';
    }
    ESP_LOGI(__func__, "New VIN received: %s", device_state.VIN_number);
    
    // TODO: Set new vin flag
    
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[SERIAL_NUM_LOWER_BYTE_POS] = SERIAL_NUM_LOWER_BYTE;
    message_package->message[SERIAL_NUM_UPPER_BYTE_POS] = SERIAL_NUM_UPPER_BYTE;
    message_package->message[COMMAND_LOWER_BYTE_POS] = 'D';
    message_package->message[COMMAND_UPPER_BYTE_POS] = 'V';
    message_package->message[5] = MSG_BLANK;
    message_package->message[6] = '}';
        
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);

    vTaskDelete(NULL);  
  }
}

void command_EN(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_ER(message_package_t *message_package){
  for(;;){  message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_EV(message_package_t *message_package){
  for(;;){  message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_FD(message_package_t *message_package){
  for(;;){  message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_FL(message_package_t *message_package){
  for(;;){  message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_FN(message_package_t *message_package){
  for(;;){  message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_FR(message_package_t *message_package){
  for(;;){  message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

/*******************************/
/* Firmware update mode enable */
/*******************************/
void command_FU(message_package_t *message_package){
  // Expected message * SN SN F U 1 2 3 4 ~ 1234 = length u32 int
  float_int_byte_union_t firmware_size_bytes_union;
  firmware_size_bytes_union.num_d32 = 0;
  esp_err_t err;

  for(;;){
    ESP_LOGI(__func__, "Firmware update mode command received");

    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);

    // Convert 4 bytes to 32bit int
    firmware_size_bytes_union.num_byte[3] = message_package->message[5];
    firmware_size_bytes_union.num_byte[2] = message_package->message[6];
    firmware_size_bytes_union.num_byte[1] = message_package->message[7];
    firmware_size_bytes_union.num_byte[0] = message_package->message[8];

    firmware_size_bytes = firmware_size_bytes_union.num_d32;
    ESP_LOGI(__func__, "Firmware size %d bytes", firmware_size_bytes);

    // Get destination partition for writing image
    ota_destination_partition = NULL;
    ota_destination_partition = esp_ota_get_next_update_partition(NULL);
    if (ota_destination_partition->size >= firmware_size_bytes){
      ESP_LOGI(__func__, "Writing to partition subtype %d at offset 0x%x",ota_destination_partition->subtype, ota_destination_partition->address);
      assert(ota_destination_partition != NULL);
        
      // Prepare writing to flash
      err = esp_ota_begin(ota_destination_partition, OTA_SIZE_UNKNOWN, &ota_firmware_handle);
 
      if(err != ESP_OK){
	ESP_LOGE(__func__,"Failed to begin update");
      }else{
	xEventGroupSetBits(deviceStatusGroup, FIRMWARE_UPDATE_MODE);
	ESP_LOGI(__func__, "Firmware update mode flag set");
      }
    }
    vTaskDelete(NULL);
  }
}

void command_FV(message_package_t *message_package){
  for(;;){
    /* Fake measurement copied from a pressure device */
    message_package->length = 36;
    message_package->message[0] = 0x2A;
    message_package->message[1] = SERIAL_NUM_LOWER_BYTE;
    message_package->message[2] = SERIAL_NUM_UPPER_BYTE;
    message_package->message[3] = 0x4A;
    message_package->message[4] = 0x56;
    message_package->message[5] = 0x1D;
    message_package->message[6] = 0x2A;
    message_package->message[7] = 0x24;
    message_package->message[8] = 0x43;
    message_package->message[9] = 0x44;
    message_package->message[10] = 0x6;
    message_package->message[11] = 0x43;
    message_package->message[12] = 0x56;
    message_package->message[13] = 0x80;
    message_package->message[14] = 0xFF;
    message_package->message[15] = 0x48;
    message_package->message[16] = 0x47;
    message_package->message[17] = 0xF1;
    message_package->message[18] = 0xD8;
    message_package->message[19] = 0x4C;
    message_package->message[20] = 0x47;
    message_package->message[21] = 0x0F;
    message_package->message[22] = 0x27;
    message_package->message[23] = 0x41;
    message_package->message[24] = 0x47;
    message_package->message[25] = 0x0;
    message_package->message[26] = 0x0;
    message_package->message[27] = 0x4C;
    message_package->message[28] = 0x44;
    message_package->message[29] = 0x0;
    message_package->message[30] = 0x0;
    message_package->message[31] = 0x43;
    message_package->message[32] = 0x54;
    message_package->message[33] = 0x1;
    message_package->message[34] = 0x0;
    message_package->message[35] = MSG_FOOTER;

    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_GT(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_IP(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_JV(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_LA(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

/*********************/
/* Turn off all LEDs */
/*********************/
void command_LF(message_package_t *message_package){
  for(;;){
    COMMAND_HELPER(message_package);
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    // TODO: Turn off all LEDs
    // unset led_status register
    // xEventGroupSetBits(deviceStatusGroup, LEDS_NEED_CHANGE);

    vTaskDelete(NULL);
  }
}

void command_LG(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

/********************/
/* Turn on all LEDs */
/********************/
void command_LN(message_package_t *message_package){
  for(;;){
    COMMAND_HELPER(message_package);
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    // TODO: Turn on all LEDs
    // set led_status register
    // xEventGroupSetBits(deviceStatusGroup, LEDS_NEED_CHANGE);
    
    vTaskDelete(NULL);
  }
}

void command_LR(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_LS(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_LT(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_LW(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_MA(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_PR(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_PV(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

/*****************/
/* Reboot device */
/*****************/
void command_RB(message_package_t *message_package){
  for(;;){
    COMMAND_HELPER(message_package);
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    esp_restart(); // Reboot CPU
    vTaskDelete(NULL);
  }
}

void command_R1(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_RF(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_RG(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_RL(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_RP(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

/**********************************/
/* Send all recorded measurements */
/**********************************/
// Message is split into multiple message to respect message buffer size
// Total message length is 4-5000 bytes, depending on ADC buffer size 
//void command_RR(message_package_t *message_package){
//  int32_t i, j = 0;
//  float_int_byte_union_t checksum = {.num_d32 = 0};
//  float_int_byte_union_t length = {.num_d32 = 3*adc_meas_buffer_counter}; // axis*num_measurement
//
//  for(;;){
//    ESP_LOGI("command RR", "Start sending %d measurements", adc_meas_buffer_counter);
//    // Header, serial, command already in message_package (*__RR)
//    // Send length of measurements
//    message_package->message[5] = length.num_byte[1];
//    message_package->message[6] = length.num_byte[0];
//    message_package->message[7] = 'N';
//    message_package->message[8] = '*';
//    message_package->message[9] = '$';
//    // Total message length
//    message_package->length = 10;
//    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
//    vTaskDelay(1000 / portTICK_PERIOD_MS); // Dirty hack to prevent BT congestion bug, which causes lost messages
//
//    // Send all ADC force measurements XYZ, XYZ, ...
//    message_package->length = 12;
//    while(j < adc_meas_buffer_counter){
//      float_to_byte(adc_measurement_buffer[0][j], (message_package->message + 0));
//      float_to_byte(adc_measurement_buffer[1][j], (message_package->message + 4));
//      float_to_byte(adc_measurement_buffer[2][j], (message_package->message + 8));
//      vTaskDelay(12 / portTICK_PERIOD_MS); // Dirty hack to prevent BT congestion bug
//      xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
//      for(i = 0; i < message_package->length; i++){
//	checksum.num_d32 += message_package->message[i];
//      }
//      j++;
//    }
//
//    // Send checksum
//    message_package->message[0] = checksum.num_byte[3];
//    message_package->message[1] = checksum.num_byte[2];
//    message_package->message[2] = checksum.num_byte[1];
//    message_package->message[3] = checksum.num_byte[0];
//    // Send footer
//    message_package->message[4] = MSG_FOOTER;
//    // Total message length
//    message_package->length = 5;
//    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
//    ESP_LOGI("command RR", "Finished sending %d measurements", adc_meas_buffer_counter);
//
//    /*
//      printf("buffer count in dec %d\t", adc_meas_buffer_counter);
//      printf("buffer [0]  0x%02x\t", length.num_byte[0]);
//      printf("buffer [1]  0x%02x\t", length.num_byte[1]);
//      printf("buffer [2]  0x%02x\t", length.num_byte[2]);
//      printf("buffer [3]  0x%02x\r\n", length.num_byte[3]);
//    */
//    vTaskDelete(NULL);
//  }
//}

/*******************/
/* Set Active Door */
/*******************/
//void command_SD(message_package_t *message_package){
//  for(;;){
//    // TODO: check & implement what the door bit array function is in STM32 code
//    COMMAND_HELPER(message_package);
//
//    uint8_t door = message_package->message[5];
//    if(door <= OTHER_DOOR){
//      device_state.active_door = (door_t) door;
//    }else{
//      device_state.active_door = OTHER_DOOR;
//    }
//    ESP_LOGI(__func__,"Active door = %d. TR=0, RR=1, FR=2, RL=3, FL=4, HD=5, OTHER=6", device_state.active_door);
//    // TODO: set flags for new door for screen update
//
//    message_package->length = REPLY_MSG_LENGTH;
//    message_package->message[5] = MSG_BLANK;
//    message_package->message[6] = MSG_FOOTER;
//    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
//
//    vTaskDelete(NULL);
//  }
//}
//
///***************/
///* Seesion End */
///***************/
//void command_SE(message_package_t *message_package){
//  xQueueHandle trans_completed;
//  for(;;){
//    message_package->length = REPLY_MSG_LENGTH;
//    message_package->message[5] = MSG_BLANK;
//    message_package->message[6] = MSG_FOOTER;
//    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
//
//    if(device_state.device_locked == FALSE){
//      device_state.device_usage_count++;
//      ESP_LOGI(__func__, "Device usage count %d", device_state.device_usage_count);
//
//      // Increase usage counter in flash every 10 cycles
//      if(device_state.device_usage_count % 10 == 0){
//	uint32_t comp_flag;
//	trans_completed = xQueueCreate(1,sizeof(uint32_t)); // Queue to block code during write op
//
//	nvs_package_t nvs_package  = {.nvs_key = NVS_KEY_U32_DEVICE_USAGE_COUNT, .data_type=NVS_TYPE_U32, .data_to_write=&device_state.device_usage_count,
//				      .read_data = NULL, .read_completed_notif = trans_completed};
//	xQueueSendToBack(nvs_transaction_queue, &nvs_package, portMAX_DELAY);
//	if(xQueueReceive(trans_completed, &comp_flag,portMAX_DELAY)){ // Wait until value read form NVS
//	  ESP_LOGI(__func__, "Device usage count updated: %d", device_state.device_usage_count);
//	}else{
//	  ESP_LOGE(__func__, "Device usage count failed to update");
//	}
//	vQueueDelete(trans_completed);
//      }
//
//    }else{
//      ESP_LOGE(__func__, "Usage exceeded. Device LOCKED");
//    }
//    // TODO: add flag for code entry
//    // TODO: add flag for in-session - usage count will increase every time SE command called regardless of in/out of session
//
//    // Lock down device
//    if(device_state.device_usage_count+1 >= USAGE_LIMIT /* && not_code_entered */ && device_state.device_locked==FALSE){
//
//      device_state.device_locked = TRUE;
//      ESP_LOGW(__func__, "Device usage count exceeds limit (%d)", USAGE_LIMIT);
//
//      // TODO: check lock down in app_main()/somewhere appropriate
//      uint8_t device_locked = 1;
//      uint32_t comp_flag;
//      trans_completed = xQueueCreate(1,sizeof(uint32_t)); // Queue to block code during write op
//
//      nvs_package_t  nvs_package = {.nvs_key = NVS_KEY_U8_DEVICE_LOCKED, .data_type=NVS_TYPE_U8, .data_to_write=&device_locked,
//				   .read_data = NULL, .read_completed_notif = trans_completed};
//
//      xQueueSendToBack(nvs_transaction_queue, &nvs_package, portMAX_DELAY);
//      if(xQueueReceive(trans_completed, &comp_flag,portMAX_DELAY)){ // Wait until value read form NVS
//	ESP_LOGI(__func__, "Device locked flag set in NVS");
//      }else{
//	ESP_LOGE(__func__, "Device lock failed to set in NVS");
//      }
//      vQueueDelete(trans_completed);
//    }
//
//    free(message_package);
//    vTaskDelete(NULL);
//  }
//}

/**********************/
/* Send Serial number */
/**********************/
void command_SN(message_package_t *message_package){
  for(;;){
    COMMAND_HELPER(message_package);
    
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[SERIAL_NUM_LOWER_BYTE_POS] = SERIAL_NUM_LOWER_BYTE;
    message_package->message[SERIAL_NUM_UPPER_BYTE_POS] = SERIAL_NUM_UPPER_BYTE;
    message_package->message[5] = MSG_BLANK;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
 
    free(message_package);
    vTaskDelete(NULL);
  }
}

/*****************/
/* Session Start */
/*****************/
void command_SS(message_package_t *message_package){
  for(;;){
    COMMAND_HELPER(message_package);
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[5] = MSG_BLANK;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);

    //Change to measurement screen
    // Update screen
    /* screen_handler_queue_package_t screen_message = {.command = MEASUREMENT}; */
    /* memcpy(&screen_message.analysis_result, &(measurement_queue_package.analysis_result), sizeof(analysis_result_t)); */
    /* xQueueSendToBack(screen_handler_queue, &screen_message, portMAX_DELAY); */
    free(message_package);  
    vTaskDelete(NULL);
  }
}

void command_SV(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);

    vTaskDelete(NULL);
  }
}

void command_TP(message_package_t *message_package){
  for(;;){
    adc_stop_conversion();

    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);

    xTaskCreate(command_FV, "command fv", 4096, (void *) message_package, 10, NULL);
    vTaskDelete(NULL);
  }
}

/************************/
/* Light Tolerance LEDs */
/************************/    
void command_TR(message_package_t *message_package){
  for(;;){
    COMMAND_HELPER(message_package);
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    // TODO: light(tolerance_leds);
    vTaskDelete(NULL);
  }
}
void command_TS(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    reset_adc_meas_buffer_counter();
    adc_start_conversion();

    vTaskDelete(NULL);
  }
}

void command_UT(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_UW(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

void command_WF(message_package_t *message_package){
  for(;;){
    message_package->length = REPLY_MSG_LENGTH;
    message_package->message[6] = MSG_FOOTER;
    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
    vTaskDelete(NULL);
  }
}

/* Command List Structure */
/* ======================
 * This structure works but is rather ugly unforunately....
 * May be better/easier to understand if there was a 26x26 array for all
 * possible alpha commands at the expense of memory
 * Commands start with char [A-Z] followed by second char
 *     Tree
 *      | - A Branch  - AD Twigs
 *      |                  - ID i.e. upper command byte - D
 *      |                  - function pointer
 *      |                  - task handle (used to check if already running)
 *      |             - AS Twigs
 *      |                  - ...
 *      |                  - ...
 *      | - ...
 */

//To add a command,
//  - update #define NUM_X_COMMANDS to reflect total number of commands : #define NUM_A_COMMANDS 4
//  - find appopriate twigs corresponding to first command char
//  - add a twig with the twig_id reflecting upper byte char and leaf_function with address of routine : twig_t twig_AD = {.twig_id = 'D', .leaf_function = &command_AD};
//  - update twig array with new command addresss : twig_t *A_twigs[NUM_A_COMMANDS] =  {&twig_AD, &twig_AS, &twig_AT, &twig_AV};

#define NUM_A_COMMANDS 4
twig_t twig_AD = {.twig_id = 'D', .leaf_function = &command_AD};
twig_t twig_AS = {.twig_id = 'S', .leaf_function = &command_AS};
twig_t twig_AT = {.twig_id = 'T', .leaf_function = &command_AT};
twig_t twig_AV = {.twig_id = 'V', .leaf_function = &command_AV};
twig_t *A_twigs[NUM_A_COMMANDS] =  {&twig_AD, &twig_AS, &twig_AT, &twig_AV};

#define NUM_B_COMMANDS 2
twig_t twig_BF = {.twig_id = 'F', .leaf_function = &command_BF};
twig_t twig_BN = {.twig_id = 'N', .leaf_function = &command_BN};
twig_t *B_twigs[NUM_B_COMMANDS] = {&twig_BF, &twig_BN};

#define NUM_C_COMMANDS 4
twig_t twig_CC = {.twig_id = 'C', .leaf_function = &command_CC};
twig_t twig_CM = {.twig_id = 'M', .leaf_function = &command_CM};
twig_t twig_CR = {.twig_id = 'R', .leaf_function = &command_CR};
twig_t twig_CU = {.twig_id = 'U', .leaf_function = &command_CU};
 twig_t *C_twigs[NUM_C_COMMANDS] =  {&twig_CC, &twig_CM, &twig_CR, &twig_CU};

#define NUM_D_COMMANDS 2
 twig_t twig_DI = {.twig_id = 'I', .leaf_function = &command_DI};
 twig_t twig_DV = {.twig_id = 'V', .leaf_function = &command_DV};
 twig_t *D_twigs[NUM_D_COMMANDS] =  {&twig_DI, &twig_DV};

#define NUM_E_COMMANDS 3
twig_t twig_EN = {.twig_id = 'N', .leaf_function = &command_EN};
twig_t twig_ER = {.twig_id = 'R', .leaf_function = &command_ER};
twig_t twig_EV = {.twig_id = 'V', .leaf_function = &command_EV};
twig_t *E_twigs[NUM_E_COMMANDS] =  {&twig_EN, &twig_ER, &twig_EV};

#define NUM_F_COMMANDS 6
twig_t twig_FD = {.twig_id = 'D', .leaf_function = &command_FD};
twig_t twig_FL = {.twig_id = 'L', .leaf_function = &command_FL};
twig_t twig_FN = {.twig_id = 'N', .leaf_function = &command_FN};
twig_t twig_FR = {.twig_id = 'R', .leaf_function = &command_FR};
twig_t twig_FU = {.twig_id = 'U', .leaf_function = &command_FU};
twig_t twig_FV = {.twig_id = 'V', .leaf_function = &command_FV};
twig_t *F_twigs[NUM_F_COMMANDS] =  {&twig_FD, &twig_FL, &twig_FN, &twig_FR,  &twig_FU, &twig_FV};

#define NUM_G_COMMANDS 1
twig_t twig_GT = {.twig_id = 'T', .leaf_function = &command_GT};
twig_t *G_twigs[NUM_F_COMMANDS] = {&twig_GT};

#define NUM_H_COMMANDS 1
twig_t *H_twigs[NUM_H_COMMANDS] = {NULL};

#define NUM_I_COMMANDS 1
twig_t twig_IP = {.twig_id = 'P', .leaf_function = &command_IP};
twig_t *I_twigs[NUM_I_COMMANDS] =  {&twig_IP};

#define NUM_J_COMMANDS 1
twig_t twig_JV = {.twig_id = 'V', .leaf_function = &command_JV};
twig_t *J_twigs[NUM_J_COMMANDS] = {&twig_JV};

#define NUM_K_COMMANDS 1
twig_t *K_twigs[NUM_K_COMMANDS] = {NULL};

#define NUM_L_COMMANDS 8
twig_t twig_LA = {.twig_id = 'A', .leaf_function = &command_LA};
twig_t twig_LF = {.twig_id = 'F', .leaf_function = &command_LF};
twig_t twig_LG = {.twig_id = 'G', .leaf_function = &command_LG};
twig_t twig_LN = {.twig_id = 'N', .leaf_function = &command_LN};
twig_t twig_LR = {.twig_id = 'R', .leaf_function = &command_LR};
twig_t twig_LS = {.twig_id = 'S', .leaf_function = &command_LS};
twig_t twig_LT = {.twig_id = 'T', .leaf_function = &command_LT};
twig_t twig_LW = {.twig_id = 'W', .leaf_function = &command_LW};
twig_t *L_twigs[NUM_L_COMMANDS] = {&twig_LA, &twig_LF, &twig_LG, &twig_LN, &twig_LR, &twig_LS, &twig_LT, &twig_LW};

#define NUM_M_COMMANDS 1
twig_t twig_MA = {.twig_id = 'A', .leaf_function = &command_MA};
twig_t *M_twigs[NUM_M_COMMANDS] = {&twig_MA};

#define NUM_N_COMMANDS 1
twig_t *N_twigs[NUM_M_COMMANDS] = {NULL};

#define NUM_O_COMMANDS 1
twig_t *O_twigs[NUM_O_COMMANDS] = {NULL};

#define NUM_P_COMMANDS 2
twig_t twig_PR = {.twig_id = 'R', .leaf_function = &command_PR};
twig_t twig_PV = {.twig_id = 'V', .leaf_function = &command_PV};
twig_t *P_twigs[NUM_P_COMMANDS] = {&twig_PR, &twig_PV};

#define NUM_Q_COMMANDS 1
twig_t *Q_twigs[NUM_Q_COMMANDS] = {NULL};

#define NUM_R_COMMANDS 6
twig_t twig_R1 = {.twig_id = '1', .leaf_function = &command_R1};
twig_t twig_RB = {.twig_id = 'B', .leaf_function = &command_RB};
twig_t twig_RF = {.twig_id = 'F', .leaf_function = &command_RF};
twig_t twig_RG = {.twig_id = 'G', .leaf_function = &command_RG};
twig_t twig_RL = {.twig_id = 'L', .leaf_function = &command_RL};
twig_t twig_RP = {.twig_id = 'P', .leaf_function = &command_RP};
twig_t twig_RR = {.twig_id = 'R', .leaf_function = &command_RR};
twig_t *R_twigs[NUM_R_COMMANDS] = {&twig_R1, &twig_RB, &twig_RG, &twig_RL, &twig_RP, &twig_RR};

#define NUM_S_COMMANDS 5
twig_t twig_SD = {.twig_id = 'D', .leaf_function = &command_SD};
twig_t twig_SE = {.twig_id = 'E', .leaf_function = &command_SE};
twig_t twig_SN = {.twig_id = 'N', .leaf_function = &command_SN};
twig_t twig_SS = {.twig_id = 'S', .leaf_function = &command_SS};
twig_t twig_SV = {.twig_id = 'V', .leaf_function = &command_SV};
twig_t *S_twigs[NUM_R_COMMANDS] = {&twig_SD, &twig_SE, &twig_SN, &twig_SS, &twig_SV};

#define NUM_T_COMMANDS 3
twig_t twig_TP = {.twig_id = 'P', .leaf_function = &command_TP};
twig_t twig_TR = {.twig_id = 'R', .leaf_function = &command_TR};
twig_t twig_TS = {.twig_id = 'S', .leaf_function = &command_TS};
twig_t *T_twigs[NUM_T_COMMANDS] = {&twig_TP, &twig_TR, &twig_TS};

#define NUM_U_COMMANDS 2
twig_t twig_UT = {.twig_id = 'T', .leaf_function = &command_UT};
twig_t twig_UW = {.twig_id = 'W', .leaf_function = &command_UW};
twig_t *U_twigs[NUM_U_COMMANDS] = {&twig_UT, &twig_UW};

#define NUM_V_COMMANDS 1
twig_t *V_twigs[NUM_V_COMMANDS] = {NULL};

#define NUM_W_COMMANDS 1
twig_t twig_WF = {.twig_id = 'F', .leaf_function = &command_WF};
twig_t *W_twigs[NUM_Q_COMMANDS] = {&twig_WF};

#define NUM_X_COMMANDS 1
twig_t *X_twigs[NUM_X_COMMANDS] = {NULL};

#define NUM_Y_COMMANDS 1
twig_t *Y_twigs[NUM_Y_COMMANDS] = {NULL};

#define NUM_Z_COMMANDS 1
twig_t *Z_twigs[NUM_Z_COMMANDS] = {NULL};

branch_t A_branch = {.branch_id = 'A', .num_twigs = NUM_A_COMMANDS, .twigs = A_twigs};
branch_t B_branch = {.branch_id = 'B', .num_twigs = NUM_B_COMMANDS, .twigs = B_twigs};
branch_t C_branch = {.branch_id = 'C', .num_twigs = NUM_C_COMMANDS, .twigs = C_twigs};
branch_t D_branch = {.branch_id = 'D', .num_twigs = NUM_D_COMMANDS, .twigs = D_twigs};
branch_t E_branch = {.branch_id = 'E', .num_twigs = NUM_E_COMMANDS, .twigs = E_twigs};
branch_t F_branch = {.branch_id = 'F', .num_twigs = NUM_F_COMMANDS, .twigs = F_twigs};
branch_t G_branch = {.branch_id = 'G', .num_twigs = NUM_G_COMMANDS, .twigs = G_twigs};
branch_t H_branch = {.branch_id = 'H', .num_twigs = NUM_H_COMMANDS, .twigs = H_twigs};
branch_t I_branch = {.branch_id = 'I', .num_twigs = NUM_I_COMMANDS, .twigs = I_twigs};
branch_t J_branch = {.branch_id = 'J', .num_twigs = NUM_J_COMMANDS, .twigs = J_twigs};
branch_t K_branch = {.branch_id = 'K', .num_twigs = NUM_K_COMMANDS, .twigs = K_twigs};
branch_t L_branch = {.branch_id = 'L', .num_twigs = NUM_L_COMMANDS, .twigs = L_twigs};
branch_t M_branch = {.branch_id = 'M', .num_twigs = NUM_M_COMMANDS, .twigs = M_twigs};
branch_t N_branch = {.branch_id = 'N', .num_twigs = NUM_N_COMMANDS, .twigs = N_twigs};
branch_t O_branch = {.branch_id = 'O', .num_twigs = NUM_O_COMMANDS, .twigs = O_twigs};
branch_t P_branch = {.branch_id = 'P', .num_twigs = NUM_P_COMMANDS, .twigs = P_twigs};
branch_t Q_branch = {.branch_id = 'Q', .num_twigs = NUM_Q_COMMANDS, .twigs = Q_twigs};
branch_t R_branch = {.branch_id = 'R', .num_twigs = NUM_R_COMMANDS, .twigs = R_twigs};
branch_t S_branch = {.branch_id = 'S', .num_twigs = NUM_S_COMMANDS, .twigs = S_twigs};
branch_t T_branch = {.branch_id = 'T', .num_twigs = NUM_T_COMMANDS, .twigs = T_twigs};
branch_t U_branch = {.branch_id = 'U', .num_twigs = NUM_U_COMMANDS, .twigs = U_twigs};
branch_t V_branch = {.branch_id = 'V', .num_twigs = NUM_V_COMMANDS, .twigs = V_twigs};
branch_t W_branch = {.branch_id = 'W', .num_twigs = NUM_W_COMMANDS, .twigs = W_twigs};
branch_t X_branch = {.branch_id = 'X', .num_twigs = NUM_X_COMMANDS, .twigs = X_twigs};
branch_t Y_branch = {.branch_id = 'Y', .num_twigs = NUM_Y_COMMANDS, .twigs = Y_twigs};
branch_t Z_branch = {.branch_id = 'Z', .num_twigs = NUM_Z_COMMANDS, .twigs = Z_twigs};

tree command_list[26] = {&A_branch,&B_branch,&C_branch,&D_branch,&E_branch,&F_branch,&G_branch,&H_branch,&I_branch,&J_branch,&K_branch,&L_branch,&M_branch,&N_branch,&O_branch,&P_branch,&Q_branch,&R_branch,&S_branch,&T_branch,&U_branch,&V_branch,&W_branch,&X_branch,&Y_branch,&Z_branch};
