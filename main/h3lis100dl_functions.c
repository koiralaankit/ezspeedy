/*
 ******************************************************************************
 * @file    read_data_polling.c
 * @author  Sensors Software Solution Team
 * @brief   This file show the simplest way to get data from sensor.
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 *******************************************************************************/
/*Includes ------------------------------------------------------------------*/

#include "sensor_ism330dlc_functions.h"

#include <sensor_algorithm.h>
#include <string.h>
#include <stdio.h>

#include "main.h"

#include "esp_log.h"
#include "driver/i2c.h"
#include "driver/gpio.h"

#include "h3lis100dl_reg.h"
#include <assert.h>
#include "bt.h"

stmdev_ctx_t dev_ctx;
/* Private macro -------------------------------------------------------------*/
#define    BOOT_TIME            20 //ms

/* Private variables ---------------------------------------------------------*/
static int16_t data_raw_acceleration[3];
static float acceleration_mg[3];
static uint8_t whoamI= 0x00;
static uint8_t tx_buffer[1000];

/* Extern variables ----------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
/*
 *   WARNING:
 *   Functions declare in this section are defined at the end of this file
 *   and are strictly related to the hardware platform used.
 *
 */
static int32_t h3lis100dl_write(void *handle, uint8_t reg,
                              uint8_t *bufp,
                              uint16_t len);
static int32_t h3lis100dl_read(void *handle, uint8_t reg, uint8_t *bufp,
                             uint16_t len);
//static void tx_com(uint8_t *tx_buffer, uint16_t len);
//static void platform_init(void);
static int32_t h3lis100dl_write(void *handle, uint8_t Reg, uint8_t *Bufp, uint16_t len)
{
  esp_err_t ret = ESP_OK;
  static spi_transaction_t t;
  //Reg |= 0x40;
  memset(&t, 0, sizeof(t));					//Zero out the transaction
  t.length=len*8;	//Len is in bytes, transaction length is in bits.
  t.addr = Reg;
  t.tx_buffer = Bufp;               		//TXData
  t.rx_buffer = NULL;
  //ret= spi_device_polling_transmit(ism330dlc_handle, &t);  //Transmit!
  ret= spi_device_transmit(h3lis100dl_handle, &t);  //Transmit!
  assert(ret==ESP_OK);            //Should have had no issues.
  return 0;
}
// Read len number of btyes into *Bufp from register address, Reg
static int32_t h3lis100dl_read(void *handle, uint8_t Reg, uint8_t *Bufp, uint16_t len)
{
  Reg |= 0x80; // Required by ISM chip to set read/write

  esp_err_t ret = ESP_OK;
  static spi_transaction_t t;
  memset(&t, 0, sizeof(t));					//Zero out the transaction

  t.length=len*8;	//Len is in bytes, transaction length is in bits.
  t.addr = Reg;
  t.tx_buffer = NULL;
  t.rx_buffer = Bufp;               		//TXData
 // ESP_LOGW(__func__, "kk %0x ", *(uint8_t *)t.rx_buffer);
  ret= spi_device_transmit(h3lis100dl_handle, &t);
  //ESP_LOGW(__func__, "kk %0x ", *(uint8_t *)t.rx_buffer);
  assert(ret==ESP_OK);            //Should have had no issues.

  return 0;
}


/* Main Example --------------------------------------------------------------*/

void init_h3lis100dl()
{


	int1_on_th_conf_t int_route;
	dev_ctx.write_reg = h3lis100dl_write;
	dev_ctx.read_reg = h3lis100dl_read;
	dev_ctx.handle = &h3lis100dl_handle;

	/* Disable HP filter */
	h3lis100dl_hp_path_set(&dev_ctx, H3LIS100DL_HP_DISABLE);

	/* Set minimum event duration for free fall */
//	h3lis100dl_int1_dur_set(&dev_ctx, 3);
	/* Apply free-fall axis threshold */
	/* Set threshold to 350mg*/
	//h3lis100dl_int1_treshold_set(&dev_ctx, 1);
	/* Enable interrupt generation on free fall INT1 pin */
	//h3lis100dl_int1_on_threshold_mode_set(&dev_ctx,H3LIS100DL_INT1_ON_THRESHOLD_OR);
//	h3lis100dl_int1_on_threshold_conf_get(&dev_ctx, &int_route);
//	int_route.int1_xlie = PROPERTY_ENABLE;
//	int_route.int1_ylie = PROPERTY_ENABLE;
//	int_route.int1_zlie = PROPERTY_ENABLE;
//	h3lis100dl_int1_on_threshold_conf_set(&dev_ctx, int_route);

	h3lis100dl_int1_notification_set(&dev_ctx, H3LIS100DL_INT1_PULSED);
	h3lis100dl_pin_mode_set(&dev_ctx, H3LIS100DL_PUSH_PULL);
	h3lis100dl_pin_polarity_set(&dev_ctx,H3LIS100DL_ACTIVE_LOW);
//	h3lis100dl_int1_on_threshold_conf_set(&dev_ctx, int_route);

	h3lis100dl_pin_int1_route_set(&dev_ctx,  H3LIS100DL_PAD1_DRDY);
	h3lis100dl_data_rate_set(&dev_ctx, H3LIS100DL_ODR_5Hz);

//	  h3lis100dl_int1_treshold_set(&dev_ctx, 22);
//	  /* Enable interrupt generation on free fall INT1 pin */
//	  h3lis100dl_int1_on_threshold_mode_set(&dev_ctx,
//	                                        H3LIS100DL_INT1_ON_THRESHOLD_OR);
//	  h3lis100dl_int1_on_threshold_conf_get(&dev_ctx, &int_route);
//	  int_route.int1_xlie = PROPERTY_ENABLE;
//	  int_route.int1_ylie = PROPERTY_ENABLE;
//	  int_route.int1_zlie = PROPERTY_ENABLE;
//	  h3lis100dl_int1_on_threshold_conf_set(&dev_ctx, int_route);


	while(1) {
    //h3lis100dl_pin_int1_route_set(&dev_ctx,  H3LIS100DL_PAD1_DRDY);
	uint8_t reg3 = 0x02;
	h3lis100dl_reg_t reg;
	h3lis100dl_status_reg_get(&dev_ctx, &reg.status_reg);

	    if (reg.status_reg.zyxda) {

//	h3lis100dl_read_reg(&dev_ctx, 0x29, &reg3, 1);
//	h3lis100dl_read_reg(&dev_ctx, 0x2B, &reg3, 1);
//	h3lis100dl_read_reg(&dev_ctx, 0x2D, &reg3, 1);
//	vTaskDelay(pdMS_TO_TICKS(1000));
	    	}

//	uint8_t reg3_write = 0x82;
//	uint8_t reg1 = 0x27;
//	uint8_t int1_cfg = 0x2A;
//
//	h3lis100dl_write_reg(&dev_ctx, 0x30,&int1_cfg, 1);
//    uint8_t int1_ths = 0x7f;
//    h3lis100dl_write_reg(&dev_ctx, 0x32,&int1_ths, 1);
//    uint8_t int1_dur = 0x01;
//    h3lis100dl_write_reg(&dev_ctx, 0x33,&int1_dur, 1);
////
//
//
//
//	h3lis100dl_write_reg(&dev_ctx, 0x20,&reg1, 1);
//	h3lis100dl_write_reg(&dev_ctx, 0x22,&reg3_write, 1);

//	h3lis100dl_write_reg(&dev_ctx, 0x22,&reg3, 1);
//	/* Set Output Data Rate */
//
//	reg3 = 0x02;
//	h3lis100dl_read_reg(&dev_ctx, 0x22, &reg3, 1 );
//	h3lis100dl_read_reg(&dev_ctx, 0x31, &reg3, 1 );
	//printf(" ctrl_reg3 value = %0x", reg3);


//	while(1)
//	{
		 /* Read output only if new value is available */
//		    h3lis100dl_reg_t reg;
//		    h3lis100dl_status_reg_get(&dev_ctx, &reg.status_reg);
//
//		    if (reg.status_reg.zyxda)
//		    {
//		    	//ESP_LOGI(__func__,"new");
//		    	memset(data_raw_acceleration, 0x00, 3 * sizeof(int16_t));
//		    	h3lis100dl_acceleration_raw_get(&dev_ctx, data_raw_acceleration);
//		    	h3lis100dl_read_reg(&dev_ctx, 0x31, &reg3, 1 );
//		    }
	}


}
void h3lis100dl_read_data_polling(void *args)
{

  /* Initialize mems driver interface */
  uint8_t out_z = 0x00;
  ESP_LOGI(__func__," j ");
  dev_ctx.write_reg = h3lis100dl_write;
  dev_ctx.read_reg = h3lis100dl_read;
  dev_ctx.handle = &h3lis100dl_handle;
  /* Initialize platform specific hardware */
  //platform_init();
  /* Wait sensor boot time */
  /* Check device ID */
  //gpio_set_level(27, 0);
  h3lis100dl_device_id_get(&dev_ctx, &whoamI);
  //gpio_set_level(27,1);

  printf(" device = %0x", whoamI);
  ESP_LOGI(__func__, " hh ");
 // if (whoamI != H3LIS100DL_ID) {
//    while (1) {
//      h3lis100dl_device_id_get(&dev_ctx, &whoamI);
//
//      ESP_LOGW(__func__, "WHO AM I = %0x", whoamI);
//      break;
//
//  }

  /* Configure filtering chain */
  /* Accelerometer - High Pass / Slope path */
      uint8_t reg1 = 0x27;
      uint8_t reg3 = 0x42;
  	  h3lis100dl_write_reg(&dev_ctx, 0x20, &reg1, 1 );
  	  h3lis100dl_write_reg(&dev_ctx, 0x22, &reg3, 1 );
	  h3lis100dl_hp_path_set(&dev_ctx, H3LIS100DL_HP_DISABLE);
	  h3lis100dl_hp_path_set(&dev_ctx, H3LIS100DL_HP_ON_OUT);
	  h3lis100dl_hp_reset_get(&dev_ctx);
  /* Set Output Data Rate */
  	  h3lis100dl_data_rate_set(&dev_ctx, H3LIS100DL_ODR_5Hz);


  /* Read samples in polling mode (no int) */
  //while (1) {
    /* Read output only if new value is available */
    h3lis100dl_reg_t reg;
    h3lis100dl_status_reg_get(&dev_ctx, &reg.status_reg);
//
    if (reg.status_reg.zyxda) {
//      /* Read acceleration data */
//      memset(data_raw_acceleration, 0x00, 3 * sizeof(int16_t));
////      h3lis100dl_acceleration_raw_get(&dev_ctx, data_raw_acceleration);
////      acceleration_mg[0] =
////        h3lis100dl_from_fs100g_to_mg(data_raw_acceleration[0]);
////      acceleration_mg[1] =
////        h3lis100dl_from_fs100g_to_mg(data_raw_acceleration[1]);
////      acceleration_mg[2] =
////        h3lis100dl_from_fs100g_to_mg(data_raw_acceleration[2]);
	    out_z = 0x00 ;
        h3lis100dl_read_reg(&dev_ctx, 0x2D,  &out_z, 1);
//      sprintf((char *)tx_buffer,
//              "Acceleration [mg]:%4.2f\t%4.2f\t%4.2f\r\n",
//              acceleration_mg[0], acceleration_mg[1], acceleration_mg[2]);
    //  ESP_LOGI(__func__, " Z-accerelatoin = %0x ", out_z);
     // tx_com(tx_buffer, strlen((char const *)tx_buffer));
   // }
  }
}
void h3lis100dl_free_fall(void *args)
{
  /* Initialize mems driver interface */

  int1_on_th_conf_t int_route;
  dev_ctx.write_reg = h3lis100dl_write;
  dev_ctx.read_reg = h3lis100dl_read;
  dev_ctx.handle = &h3lis100dl_handle;
  uint8_t whoamI = 0x00;
  /* Initialize platform specific hardware */
//  platform_init();
  /* Check device ID */
  //h3lis100dl_device_id_get(&dev_c0x0F, &whoamI, 1);

  h3lis100dl_read_reg(&dev_ctx, 0X0F, &whoamI, 1);

  printf("device id %0x ", whoamI);
  if (whoamI != H3LIS100DL_ID) {
    while (1) {
      /* manage here device not found */
    	ESP_LOGI(__func__, "ID not matched ");
    	break;
    }
  }
  ESP_LOGI(__func__, " ");
  /* Disable HP filter */
  h3lis100dl_hp_path_set(&dev_ctx, H3LIS100DL_HP_DISABLE);
  /* Set minimum event duration for free fall */
  h3lis100dl_int1_dur_set(&dev_ctx, 1);
  /* Apply free-fall axis threshold
   * Set threshold to 350mg
   */
  h3lis100dl_int1_treshold_set(&dev_ctx, 22);
  /* Enable interrupt generation on free fall INT1 pin */
  h3lis100dl_int1_on_threshold_mode_set(&dev_ctx,
                                        H3LIS100DL_INT1_ON_THRESHOLD_OR);
  h3lis100dl_int1_on_threshold_conf_get(&dev_ctx, &int_route);
  int_route.int1_xlie = PROPERTY_ENABLE;
  int_route.int1_ylie = PROPERTY_ENABLE;
  int_route.int1_zlie = PROPERTY_ENABLE;
  h3lis100dl_int1_notification_set(&dev_ctx, H3LIS100DL_INT1_LATCHED);
  	h3lis100dl_pin_mode_set(&dev_ctx, H3LIS100DL_PUSH_PULL);
  	h3lis100dl_pin_polarity_set(&dev_ctx,H3LIS100DL_ACTIVE_LOW);
  h3lis100dl_int1_on_threshold_conf_set(&dev_ctx, int_route);
  /* Set Output Data Rate */
  h3lis100dl_data_rate_set(&dev_ctx, H3LIS100DL_ODR_100Hz);
  uint8_t data = 0x02;

  /* Wait Events */
  while (1) {
    h3lis100dl_int1_src_t all_source;
////    /* Check free fall events
////     * You can test the event of zl && yl &&xl or check
////     * the interrupt int1 pin
////     */
    ESP_LOGI(__func__, "Device id %x", whoamI);
    h3lis100dl_int1_src_get(&dev_ctx, &all_source);
  //  ESP_LOGI(__func__, "int active = %0x", all_source.ia);
//    if (all_source.xl && all_source.yl && all_source.zl) {
//    //  ESP_LOGI(__func__, "free falling");
////      sprintf((char *)tx_buffer, "Free fall\r\n");
////      //tx_com(tx_buffer, strlen((char const *)tx_buffer));
////     // esp_err_t  ret = esp_spp_write(bt_handle,15,(uint8_t *)tx_buffer);
    	vTaskDelay(pdMS_TO_TICKS(1000));
//    }
  }
}

