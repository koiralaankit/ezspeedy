/*
 * wifi.c
 *
 *  Created on: Oct 29, 2020
 *      Author: Ankit koirala
 */
// Wifi Includes

#include <string.h>
#include "esp_wifi.h"
#include "esp_wifi_default.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>
#include <screen_functions.h>
//#include <sensor_ads131m04_functions.h>
#include <sensor_algorithm.h>
#include <wifi.h>
#include "main.h"

#if MAIN_WIFI == ENABLE

static EventGroupHandle_t s_connect_event_group;
static esp_ip4_addr_t s_ip_addr;
static const char *s_connection_name;
static esp_netif_t *s_esp_netif = NULL;
extern TaskHandle_t tcp_server_task_handle;
extern TaskHandle_t wifi_mode_change_task_handle;
extern wifi_mode_t wifi_mode;

int listen_sock, client_sock;

// @function description
// Set flags when client connects to device
// @params
// @return
void on_got_client_ip(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data) {
  // Log IP address
  ESP_LOGI(__func__, "Got IP event!");
  ip_event_got_ip_t *event = (ip_event_got_ip_t*) event_data;
  memcpy(&s_ip_addr, &event->ip_info.ip, sizeof(s_ip_addr));
  xEventGroupSetBits(s_connect_event_group, GOT_IPV4_BIT);
  xEventGroupSetBits(deviceStatusGroup, START_TCP_SERVER);
  xEventGroupSetBits(deviceStatusGroup, TCP_CLIENT_ESTABLISHED);
}
// @function description
// Set flags when device connects to AP
// @params
// @return
esp_err_t wifi_connect_task(void *args) {
  // Used to initiate WiFi connection
  if (s_connect_event_group != NULL) {
    return ESP_ERR_INVALID_STATE;
  }
  s_connect_event_group = xEventGroupCreate();
  start();
  ESP_ERROR_CHECK(esp_register_shutdown_handler(&stop));
  ESP_LOGI(__func__, "Waiting for IP");
  for(;;){
    xEventGroupWaitBits(s_connect_event_group, CONNECTED_BITS, true, true,portMAX_DELAY);
    ESP_LOGI(__func__, "Connected to %s", s_connection_name);
    ESP_LOGI(__func__, "IPv4 address: " IPSTR, IP2STR(&s_ip_addr));
    vTaskDelete(NULL);
    // SHould get here....
    return ESP_OK;

  }
}

// @function description
// Start Wifi server in AP or STA mode
// @params
// @return
void start(void) {
  //
  if(wifi_mode == WIFI_MODE_STA) {
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_netif_config_t netif_config = ESP_NETIF_DEFAULT_WIFI_STA();

    esp_netif_t *netif = esp_netif_new(&netif_config);
    assert(netif);
    esp_netif_attach_wifi_station(netif);
    esp_wifi_set_default_wifi_sta_handlers();

    s_esp_netif = netif;
    // Register callbacks (event handlers)
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &on_wifi_disconnect_from_ap, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_got_client_ip, NULL));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config = { .sta = { .ssid = STA_WIFI_SSID,
	.password = STA_WIFI_PASS, }, };
    ESP_LOGI(__func__, "Connecting to %s...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_ERROR_CHECK(esp_wifi_connect());
    s_connection_name = STA_WIFI_SSID;
  } else if (wifi_mode == WIFI_MODE_AP) {
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_netif_config_t netif_config = ESP_NETIF_DEFAULT_WIFI_AP();

    esp_netif_t *netif = esp_netif_new(&netif_config);

    assert(netif);

    esp_netif_attach_wifi_ap(netif);
    esp_wifi_set_default_wifi_ap_handlers();

    s_esp_netif = netif;

    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_AP_STAIPASSIGNED, &on_got_client_ip, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_AP_STADISCONNECTED, &on_client_disconnect_from_ap,NULL));

    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config = { .ap = { .ssid = AP_WIFI_SSID, .ssid_len =
	strlen(AP_WIFI_SSID), .password = AP_WIFI_PASS,
	.max_connection = MAX_AP_CONN, .authmode =
	WIFI_AUTH_WPA_WPA2_PSK }, };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI("WiFi AP", "wifi_init_softap finished.SSID:%s password:%s",
	     AP_WIFI_SSID, AP_WIFI_PASS);
    s_connection_name = AP_WIFI_SSID;
  }
}

// @function description
// Private helper of wifi_disconnect. Clean stop of ESP Wifi driver - unregisters callbacks, resets Wifi interface etc.
// @params
// wifi_mode_t mode - current mode of Wifi operation
// @return
void stop(void) {
  if (wifi_mode == WIFI_MODE_STA) {
    ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT,WIFI_EVENT_STA_DISCONNECTED, &on_wifi_disconnect_from_ap));
    ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP,on_got_client_ip));
  } else if (wifi_mode == WIFI_MODE_AP) {
    ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT,IP_EVENT_AP_STAIPASSIGNED, &on_got_client_ip));
    ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT, WIFI_EVENT_AP_STADISCONNECTED, &on_client_disconnect_from_ap));
  }

  esp_err_t err = esp_wifi_stop();
  if (err == ESP_ERR_WIFI_NOT_INIT) {
    return;
  }
  ESP_ERROR_CHECK(err);
  ESP_ERROR_CHECK(esp_wifi_deinit());
  ESP_ERROR_CHECK(esp_wifi_clear_default_wifi_driver_and_handlers(s_esp_netif));
  esp_netif_destroy(s_esp_netif);
  s_esp_netif = NULL;

  ESP_LOGI(__func__, "Close socket");
  close(listen_sock);

  xEventGroupClearBits(deviceStatusGroup, WIFI_ACTIVE);
}
// @function description
// Shutdown wifi
// @params
// @return
esp_err_t wifi_disconnect(void) {
  // Used to close Wifi session
  if (s_connect_event_group == NULL) {
    return ESP_ERR_INVALID_STATE;
  }
  vEventGroupDelete(s_connect_event_group);
  s_connect_event_group = NULL;
  stop();
  ESP_ERROR_CHECK(esp_unregister_shutdown_handler(&stop));
  ESP_LOGI(__func__, "Disconnected from %s", s_connection_name);
  s_connection_name = NULL;
  return ESP_OK;
}
// @function description
// On disconnect from AP try reconnect
// @params
// @return
void on_wifi_disconnect_from_ap(void *arg, esp_event_base_t event_base,
				       int32_t event_id, void *event_data) {
  ESP_LOGI(__func__, "Wi-Fi disconnected, trying to reconnect...");
  xEventGroupSetBits(deviceStatusGroup, STOP_TCP_SERVER);
  esp_err_t err = esp_wifi_connect();
  if (err == ESP_ERR_WIFI_NOT_STARTED) {
    // Should not be here
  }
  ESP_ERROR_CHECK(err);
}
// @function description
// On client disconnect from device AP
// @params
// @return
void on_client_disconnect_from_ap(void){
  ESP_LOGI(__func__, "Client disconnected from device");
  xEventGroupSetBits(deviceStatusGroup, STOP_TCP_SERVER);
}

// @function description
// Transmit what we receive
// @params
// const int sock - which socket to send to
// @return
void copy_message(const int sock) {
  // Sends message received back to sender
  int len;
  char rx_buffer[128];
  // Original functions which send messages received back to sender
  do {
    len = recv(sock, rx_buffer, sizeof(rx_buffer) - 1, 0);
    if (len < 0) {
      ESP_LOGE(__func__, "Error occurred during receiving: errno %d", errno);
    } else if (len == 0) {
      ESP_LOGW(__func__, "Connection closed");
      // If client on same AP disconnects there is no IP event
      // We can only tell if client disconnects if the socket dies
      //xEventGroupClearBits(deviceStatusGroup, TCP_CLIENT_ESTABLISHED);
    } else {
      rx_buffer[len] = 0; // Null-terminate whatever is received and treat it like a string
      ESP_LOGI(__func__, "Received %d bytes: %s", len, rx_buffer);
      message_package_t message_package = { .handle = sock, .length = len };
      memcpy((char*) message_package.message, rx_buffer, len);

      if (xQueueSendToBack(wifi_message_queue, (void* ) &message_package,
			   portMAX_DELAY)) {
	ESP_LOGI(__func__, "WiFi Data enqueued len=%d handle=%d", message_package.length, message_package.handle);
	esp_log_buffer_hex(__func__,message_package.message,message_package.length);
      } else {
	ESP_LOGE(__func__, "RECEIVE QUEUE FULL. MESSAGE NOT QUEUED len=%d handle=%d", len, sock);
	esp_log_buffer_hex(__func__,rx_buffer,len);
      }
    }
  } while (len > 0);

}

// @function description
// Starts TCP server and listen for messages. Log and handle incoming messages
// @params
// void *pvParameters - required from task prototype, not used
// @return
void tcp_server_task(void *pvParameters) {
  char addr_str[128];
  int addr_family;
  int ip_protocol;

  struct sockaddr_in dest_addr;

  dest_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  dest_addr.sin_family = AF_INET;
  dest_addr.sin_port = htons(PORT);
  addr_family = AF_INET;
  ip_protocol = IPPROTO_IP;
  inet_ntoa_r(dest_addr.sin_addr, addr_str, sizeof(addr_str) - 1);

  listen_sock = socket(addr_family, SOCK_STREAM, ip_protocol);
  if (listen_sock < 0) {
    ESP_LOGE(__func__, "Unable to create socket: errno %d", errno);
    vTaskDelete(NULL);
    return;
  }
  ESP_LOGI(__func__, "Socket created");
  wifi_listen_sock = listen_sock;

  static uint32_t flag = 1;
  int err = setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
  if(err != 0){
    ESP_LOGE(__func__, "Failed to set resuse address flag");
  }else{
    ESP_LOGI(__func__, "Sock resuse address flag set");
  }

  err = bind(listen_sock, (struct sockaddr*) &dest_addr,
	     sizeof(dest_addr));
  if (err != 0) {
    ESP_LOGE(__func__, "Socket unable to bind: errno %d", errno);
    goto CLEAN_UP;
  }
  ESP_LOGI(__func__, "Socket bound, port %d", PORT);

  err = listen(listen_sock, 1);
  if (err != 0) {
    ESP_LOGE(__func__, "Error occurred during listen: errno %d", errno);
    goto CLEAN_UP;
  }
  ESP_LOGI(__func__, "Socket listening");

  while (1) {
    if(xEventGroupWaitBits(deviceStatusGroup, TCP_CLIENT_ESTABLISHED, pdFALSE, pdFALSE, portMAX_DELAY)){

      struct sockaddr_in6 source_addr; // Large enough for both IPv4 or IPv6
      uint addr_len = sizeof(source_addr);
      client_sock = accept(listen_sock, (struct sockaddr*) &source_addr,
			   &addr_len);
      if (client_sock < 0) {
	ESP_LOGE(__func__, "Unable to accept connection: errno %d", errno);
	break;
      }
      wifi_client_sock = client_sock;

      // Convert ip address to string
      if (source_addr.sin6_family == PF_INET) {
	inet_ntoa_r(((struct sockaddr_in* )&source_addr)->sin_addr.s_addr,
		    addr_str, sizeof(addr_str) - 1);
      } else if (source_addr.sin6_family == PF_INET6) {
	inet6_ntoa_r(source_addr.sin6_addr, addr_str, sizeof(addr_str) - 1);
      }
      ESP_LOGI(__func__, "Socket accepted ip address: %s", addr_str);

      xEventGroupSetBits(deviceStatusGroup, WIFI_ACTIVE);

  //    copy_message(client_sock); // Do something with received message
//
//      shutdown(client_sock, 0);
//      close(client_sock);
        vTaskDelay(500/portTICK_PERIOD_MS); // Send task to background
    }
  }

 CLEAN_UP:
  shutdown(listen_sock, 0);
  close(listen_sock);
  vTaskDelete(NULL);
}

// @function description
// Creates and deletes TCP server task depending on if a wifi client is connected/disconnected
// @params
// void *pvParameters - required from task prototype, not used
// @return
void tcp_server_handler_task(void *args){
  EventBits_t uxBits = 0x0000;
  while(1){
    uxBits = xEventGroupWaitBits(deviceStatusGroup, (STOP_TCP_SERVER|START_TCP_SERVER), pdTRUE, pdFALSE, portMAX_DELAY);
    if((uxBits & STOP_TCP_SERVER) == STOP_TCP_SERVER){
      ESP_LOGI(__func__, "Killing TCP server");
      shutdown(listen_sock, 0);
      close(listen_sock);
      vTaskDelete(tcp_server_task_handle);
    }else if((uxBits & START_TCP_SERVER) == START_TCP_SERVER){
      ESP_LOGI(__func__,"Creating TCP server");
      xTaskCreate(tcp_server_task, "tcp server", 4096, NULL, 9, &tcp_server_task_handle);
    }
  }
}
// @function description
// Supervisor task to handle starting/stopping/restarting to Wifi driver
// @params
// @return
void wifi_mode_change_task(void *args) {
  // Task to cleanup wifi state and restart when mode change required
  EventBits_t uxBits;
  while (1) {
    uxBits = xEventGroupWaitBits(deviceStatusGroup, WIFI_MODE_CHANGE_REQUIRED, pdTRUE, pdFALSE, portMAX_DELAY);
    if ((uxBits & WIFI_MODE_CHANGE_REQUIRED) == WIFI_MODE_CHANGE_REQUIRED) {
      if (wifi_mode == WIFI_MODE_AP) {
	wifi_disconnect();
	vTaskDelete(tcp_server_task_handle);
	wifi_mode = WIFI_MODE_STA;
	//wifi_connect();
	xTaskCreate(wifi_connect_task, "wifi task", 4096, NULL, 10, NULL);
      } else if (wifi_mode == WIFI_MODE_STA) {
	wifi_disconnect();
	vTaskDelete(tcp_server_task_handle);
	wifi_mode = WIFI_MODE_AP;
	//wifi_connect();
	xTaskCreate(wifi_connect_task, "wifi task", 4096, NULL, 10, NULL);
	//wifi_connect_task();
      }
    }
  }
}
#endif
