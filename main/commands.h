#ifndef __COMMAND_H__
#define __COMMAND_H__

#include "main.h"
#include "esp_ota_ops.h"
#include "esp_log.h"
#include "esp_err.h"

/* ============ */
/* Public Types */
/* ============ */
typedef struct{
  uint8_t twig_id;
  void *leaf_function;
  TaskHandle_t handle;
}twig_t;

/* ================= */
/* Public Prototypes */
/* ================= */
int32_t lookup_command(message_package_t *new_message);
void command_FU(message_package_t *message);

/* ================ */
/* Public Variables */
/* ================ */
uint32_t firmware_size_bytes;
esp_ota_handle_t ota_firmware_handle;
const esp_partition_t *ota_destination_partition;

#endif
