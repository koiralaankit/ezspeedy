/*
  sd card control
*/
#include <stdio.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdspi_host.h"
#include "driver/spi_common.h"
#include "sdmmc_cmd.h"
#include "sdkconfig.h"
#include "sd_card_reader.h"
#include "bt.h"
#include "main.h"
#include "esp_spp_api.h"
#include "driver/sdmmc_host.h"
#include "sd_card_reader.h"

static const char *TAG = "SD_CARD";

// Pin mapping when using SPI mode.
// With this mapping, SD card can be used both in SPI and 1-line SD mode.
#define PIN_NUM_MISO 23
#define PIN_NUM_MOSI 22
#define PIN_NUM_CLK  21
#define PIN_NUM_CS   2

void sd_card_start(void *args)
{
  vTaskDelay(pdMS_TO_TICKS(20000));
  ESP_LOGI(TAG, "Initializing SD card");
//  ESP_LOGI(TAG, "Using SPI peripheral");

  sdmmc_host_t host = SDSPI_HOST_DEFAULT();
  sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
  slot_config.gpio_miso = (gpio_num_t)PIN_NUM_MISO;
  slot_config.gpio_mosi = (gpio_num_t)PIN_NUM_MOSI;
  slot_config.gpio_sck  = (gpio_num_t)PIN_NUM_CLK;
  slot_config.gpio_cs   = (gpio_num_t)PIN_NUM_CS;
  // This initializes the slot without card detect (CD) and write protect (WP) signals.
  // Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals.

  // Options for mounting the filesystem.
  // If format_if_mount_failed is set to true, SD card will be partitioned and
  // formatted in case when mounting fails.
  esp_vfs_fat_sdmmc_mount_config_t mount_config = {
      .format_if_mount_failed = true,
      .max_files = 5,
      .allocation_unit_size = 16 * 1024
  };

  // Use settings defined above to initialize SD card and mount FAT filesystem.
  // Note: esp_vfs_fat_sdmmc_mount is an all-in-one convenience function.
  // Please check its source code and implement error recovery when developing
  // production applications.
  sdmmc_card_t* card;
  esp_err_t ret = esp_vfs_fat_sdspi_mount("/sdcard", &host, &slot_config, &mount_config, &card);
  char err[25] = "error";
  if(ret != ESP_OK)
  {

	  strncpy(err, esp_err_to_name(ret), 25);
      esp_spp_write(bt_handle, 25,(uint8_t*)err);
  }
   strncpy(err, "NO EROWWR", 25);
   esp_spp_write(bt_handle, 25,(uint8_t*)err);
}
