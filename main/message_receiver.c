/*
 * message_handler.c
 *
 *  Created on: Dec 24, 2020
 *      Author: Ankit koirala
 */

#include "main.h"
#include "message_receiver.h"
#include "commands.h"
#include "esp_spp_api.h"
#include "lwip/sockets.h"
#include "misc.h"
#include "esp_log.h"
#include "esp_err.h"


//void command_FU(message_package_t *message_package){
//  // Expected message * SN SN F U 1 2 3 4 ~ 1234 = length u32 int
//  float_int_byte_union_t firmware_size_bytes_union;
//  firmware_size_bytes_union.num_d32 = 0;
//  esp_err_t err;
//
//  for(;;){
//    ESP_LOGI(__func__, "Firmware update mode command received");
//
//    message_package->length = REPLY_MSG_LENGTH;
//    message_package->message[6] = MSG_FOOTER;
//    xQueueSendToBack(transmit_queue, (void *) message_package, portMAX_DELAY);
//
//    // Convert 4 bytes to 32bit int
//    firmware_size_bytes_union.num_byte[3] = message_package->message[5];
//    firmware_size_bytes_union.num_byte[2] = message_package->message[6];
//    firmware_size_bytes_union.num_byte[1] = message_package->message[7];
//    firmware_size_bytes_union.num_byte[0] = message_package->message[8];
//
//    firmware_size_bytes = firmware_size_bytes_union.num_d32;
//    ESP_LOGI(__func__, "Firmware size %d bytes", firmware_size_bytes);
//
//    // Get destination partition for writing image
//    ota_destination_partition = NULL;
//    ota_destination_partition = esp_ota_get_next_update_partition(NULL);
//    if (ota_destination_partition->size >= firmware_size_bytes){
//      ESP_LOGI(__func__, "Writing to partition subtype %d at offset 0x%x",ota_destination_partition->subtype, ota_destination_partition->address);
//      assert(ota_destination_partition != NULL);
//
//      // Prepare writing to flash
//      err = esp_ota_begin(ota_destination_partition, OTA_SIZE_UNKNOWN, &ota_firmware_handle);
//
//      if(err != ESP_OK){
//	ESP_LOGE(__func__,"Failed to begin update");
//      }else{
//	xEventGroupSetBits(deviceStatusGroup, FIRMWARE_UPDATE_MODE);
//	ESP_LOGI(__func__, "Firmware update mode flag set");
//      }
//    }
//    vTaskDelete(NULL);
//  }
//}
void receive_from_bt(void *args)
{
	message_package_t msg;
	bool msg_started = false;
	static uint32_t thandle=0;
	static uint32_t tlength=0;
	static uint8_t  tmsg[MSG_MAX_LEN]={0x00};
	uint8_t byte_msg = 0x00;
	while(1)
	{

		if(xQueueReceive(bt_message_queue, &msg, (TickType_t)10)==pdPASS)
		{

				  if(*msg.message == MSG_HEADER)
				  {
					  msg_started = true;
				  }
				  if(msg_started)
				  {

					  memcpy(&tmsg[tlength], msg.message,  msg.length);
					  tlength = tlength + msg.length;
					  if(*msg.message == MSG_FOOTER) msg_started = false;

				  }
				  if(tlength >0 && !msg_started)
				  {
					  esp_spp_write(bt_handle, sizeof(tmsg), tmsg);
					  printf("received bt message");
					  //lookup_command(tmsg);

				  }
			     memset(msg.message,  0x00, msg.length);
		}

	}
}

void receive_from_wifi(void *args)
{
	message_package_t msg;
	char printmsg[MSG_MAX_LEN] ={0};
	while(1)
	{
		if(xQueueReceive(wifi_message_queue, &msg, (TickType_t)10)==pdPASS)
		{
			//command_FU(&msg);
			if(msg.message[0]== MSG_HEADER && msg.message[msg.length-1]==MSG_FOOTER)
			 {
				  send(wifi_client_sock, msg.message, msg.length, 0);
				  strncpy(printmsg, (const char*)msg.message, msg.length);
				  printmsg[msg.length] = '\0';
				  printf("sending message to wifi client %s", printmsg);

			 }
		}
	}
}


