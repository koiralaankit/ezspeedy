#ifndef __SENSOR_ISM330DLC_FUNCITONS_H
#define __SENSOR_ISM330DLC_FUNCITONS_H
#include "main.h"
/* Public functions */
uint32_t init_ism330dlc(void);
uint32_t init_ism330dlc_activity_detect_mode(void);
uint32_t init_ism330dlc_ulp_mode(void);
uint32_t unint_is330dlc_activity_detect_mode(void);
void ism330dlc_read_task(void * arg);
void ism330dlc_read_fifo_task(void * arg);
void ism330dlc_read_into_buffer_task(void * arg);
void ism_test_read(void *args);
void inertialSensor_read(void *args);
#endif /*__SENSOR_ISM330DLC_FUNCITONS_H*/
