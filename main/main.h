/*
 * main.h
 *
 *  Created on: Nov 17, 2020
 *      Author: Ankit koirala
 */


#ifndef MAIN_MAIN_H_
#define MAIN_MAIN_H_
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

/* ============= */
/* ESP Libraries */
/* ============= */
#include "driver/spi_master.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_err.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "I2Cbus.h"

#include "sensor_algorithm.h"

#define ENABLE 0
#define DISABLE 1

#define SPI_MISO_Pin 	              23
#define SPI_MOSI_Pin 	              22
#define SPI_CLK_Pin  	              21
#define MAX_NUM_SPI_DEVICES 			4
#define MAX_VIN_LENGTH                  17

#define HIGH 	                        1
#define LOW 	                        0
#define TRUE                            true
#define FALSE                           false
#define MSG_MAX_LEN                     (64+6) // Max length of message_package_t payload bytes + start,sn,footer

/*DISABLE/ENABLE Sensors and Peripherals*/
#define MAIN_BT ENABLE
#define MAIN_WIFI ENABLE
/*********************/
/* Tolerance Defines */
/*********************/
#define LOWER_TOLERANCE_ENABLE_BIT     (0)
#define LOWER_WARNING_ENABLE_BIT       (1)
#define UPPER_TOLERANCE_ENABLE_BIT     (2)
#define UPPER_WARNING_ENABLE_BIT       (3)
#define GAP_TOLERANCE_ENABLE_BIT      (4)
#define M_ANGLE_ENABLE_BIT             (5)

#define LOWER_TOLERANCE_ENABLE_BIT_MASK     (1<<LOWER_TOLERANCE_ENABLE_BIT)
#define LOWER_WARNING_ENABLE_BIT_MASK       (1<<LOWER_WARNING_ENABLE_BIT)
#define UPPER_TOLERANCE_ENABLE_BIT_MASK     (1<<UPPER_TOLERANCE_ENABLE_BIT)
#define UPPER_WARNING_ENABLE_BIT_MASK       (1<<UPPER_WARNING_ENABLE_BIT)
#define GAP_TOLERANCE_ENABLE_BIT_MASK      (1<<GAP_TOLERANCE_ENABLE_BIT)
#define M_ANGLE_ENABLE_BIT_MASK             (1<<M_ANGLE_ENABLE_BIT)

/* ================= */
/* EZ Device Defines */
/* ================= */
#define SERIAL_NUM                      (uint32_t) 0x0000
#define SERIAL_NUM_LOWER_BYTE           (uint8_t) ((SERIAL_NUM & 0x00FF)>>0) //*[lower][upper][cmd][cmd]...~
#define SERIAL_NUM_UPPER_BYTE           (uint8_t) ((SERIAL_NUM & 0xFF00)>>8)
#define MSG_HEADER_POS	                0
#define SERIAL_NUM_LOWER_BYTE_POS 	1
#define SERIAL_NUM_UPPER_BYTE_POS 	2
#define COMMAND_LOWER_BYTE_POS		3
#define COMMAND_UPPER_BYTE_POS		4
#define DATA_START_POS	   	        5
#define MSG_HEADER 		        ((uint8_t) 0x2A) // *
#define MSG_FOOTER 		        ((uint8_t) 0x7E) // ~
#define MSG_BLANK		        ((uint8_t) 0x00)
#define REPLY_MSG_LENGTH                7
#define COMMAND_MSG_LENGTH              8
#define USAGE_LIMIT                     5000
#define MAX_VIN_LENGTH                  17


/* =================== */
/* Event Group Defines */
/* =================== */
#define ADC_INITIALISED                 (1ULL<<0)
#define ADC_NULL_TASK_FINISHED          (1ULL<<1)
#define ADC_GAIN_TASK_FINISHED          (1ULL<<2)
#define ADC_CONVERTING                  (1ULL<<3)
#define ISM330DLC_INITIALISED			(1ULL<<5)
//#define ISM330DLC_DEV_FOUND				(1ULL<<6)
#define LIS331H_DEV_FOUND				(1ULL<<7)
#define ADS131M04_DEV_FOUND				(1ULL<<8)
#define FIRMWARE_UPDATE_MODE			(1ULL<<9)
#define SCREEN_NEEDS_UPDATE				(1ULL<<10)
#define WIFI_ACTIVE						(1ULL<<11)
#define BT_ACTIVE						(1ULL<<12)
#define BATTERY_SOC_CHANGE				(1ULL<<13)
#define INERTIAL_RECORD_MODE			(1ULL<<14)
#define INERTIAL_BUFFER_FULL			(1ULL<<15)
#define LEDS_NEED_CHANGE				(1ULL<<16)
#define WIFI_MODE_CHANGE_REQUIRED		(1ULL<<17)
#define START_TCP_SERVER				(1ULL<<18)
#define STOP_TCP_SERVER					(1ULL<<19)
#define TCP_CLIENT_ESTABLISHED			(1ULL<<20)


/* =============== */
/* Flash Locations */
/* =============== */
#define NVS_TEST_KEY				"test"

#define FLASH_STORAGE_AREA 			    "nvs_area"
#define NVS_KEY_U8_DEVICE_STATE_INITIALISED         "devstateinit"
#define NVS_KEY_U32_DEVICE_BOOT_COUNT               "devbootcnt"
#define NVS_KEY_U32_DEVICE_USAGE_COUNT              "devcnt"
#define NVS_KEY_U8_DEVICE_LOCKED                    "devlock"
#define NVS_KEY_U8_DEVICE_SLEEP_TIMER_ENABLE        "devsleep"
#define NVS_KEY_U8_DEVICE_CODE_ENTERED              "devcode"
#define NVS_KEY_U8_CALIBRATION_DAY                  "calday"
#define NVS_KEY_U8_CALIBRATION_MONTH                "calmonth"
#define NVS_KEY_U16_CALIBRATION_YEAR                "calyear"
#define NVS_KEY_F_CALIBRATION_FACTOR_SPEED          "calfactor"
#define NVS_KEY_F_CALIBRATION_PREV_FACTOR_SPEED     "calprevfact"
#define NVS_KEY_F_TOLERANCE_UPPER_TOLERANCE         "uptol"
#define NVS_KEY_F_TOLERANCE_UPPER_WARNING           "upwar"
#define NVS_KEY_F_TOLERANCE_LOWER_TOLERANCE         "lotol"
#define NVS_KEY_F_TOLERANCE_LOWER_WARNING           "lotol"
#define NVS_KEY_U8_TOLERANCE_ENABLE_REG             "tolenable"
#define NVS_KEY_F_TOLERANCE_M_ANGLE                 "mangle"
#define NVS_KEY_F_TOLERANCE_CURRENT_RADIUS          "currad"
#define NVS_KEY_F_TOLERANCE_ANGULAR_SPEED           "angspd"
#define NVS_KEY_F_TOLERANCE_GAP_TOLERANCE          "gaptol"

spi_device_handle_t ism330dlc_handle, h3lis100dl_handle;
extern I2C_t* i2c;
extern gpio_config_t io_conf, spi_epd_cs_pin, spi_epd_busy_pin, spi_epd_rst_pin, spi_epd_dc_pin;
SemaphoreHandle_t i2c_semaphore, inertialSensor_drdy_semaphore, spi_bus_mutex;
xQueueHandle measurement_handler_queue;
xQueueHandle new_measurement_queue;
xQueueHandle bt_message_queue, wifi_message_queue;
xQueueHandle nvs_transaction_queue;
EventGroupHandle_t deviceStatusGroup;
TickType_t uxBits;
void inertialSensor_ISR(void *args);

typedef struct{
  float lower_tolerance;
  float lower_warning;
  bool lower_tolerance_enable;
  bool lower_warning_enable;

  float upper_tolerance;
  float upper_warning;
  bool upper_tolerance_enable;
  bool upper_warning_enable;

  float current_radius;
  float angular_speed;

  float gap_tolerance;
  float m_angle; // What is m???
  bool gap_tolerance_enable;
  bool m_angle_enable;

} tolerance_t;

typedef struct{
  uint8_t  day;
  uint8_t month;
  uint16_t year;
  float factor_speed;
  float prev_factor_speed;
} calibration_t;

typedef struct{
  bool gap_mode_enabled;

  int32_t lowest_closed_1;
  int32_t lowest_closed_2;
  int32_t lowest_closed_speed_1;
  int32_t lowest_closed_speed_2;

  int32_t highest_not_closed_1;
  int32_t highest_not_closed_2;
  int32_t highest_not_closed_speed_1;
  int32_t highest_not_closed_speed_2;

  float min_close_speed;
} gap_t;

typedef enum{
	     TRUNK_DOOR,
	     REAR_RIGHT_DOOR,
	     FRONT_RIGHT_DOR,
	     REAR_LEFT_DOOR,
	     FRONT_LEFT_DOOR,
	     HOOD,
	     OTHER_DOOR,
} door_t;
typedef enum{
	     MM_PER_SEC,
	     M_PER_SEC,
	     FEET_PER_SEC,
	     DEGS_PER_SEC,
} speed_units_t;

typedef enum{
	     NEW_ANALYSIS_RESULT,
	     NEW_VIN_NUMBER,
	     NEW_GAP,
	     NEW_TOLERANCE,
	     NEW_DOOR,
}measurement_queue_command_t;


typedef struct{
  char *VIN_number;
  uint32_t VIN_length;
  tolerance_t tolerance;
  calibration_t calibration;
  gap_t gap;
  door_t active_door;
  bool device_code_entered;
  uint32_t device_usage_count;
  bool device_locked;
  bool device_sleep_timer_enabled;
}device_state_t;

typedef struct {
  uint32_t handle;
  uint32_t length;
  uint8_t message[MSG_MAX_LEN]; // NOTE: may be a good idea to change this to just a pointer & malloc to needed length to save space/enable long messages without wasting memory in the queue
} message_package_t;

typedef struct {
  char *VIN_number;
  uint32_t VIN_length;
  door_t active_door;
  measurement_queue_command_t command;
  analysis_result_t analysis_result;
  //device_state_t device_state; // Don't need whole state - probably enough to have vin, door
} measurement_handler_queue_package_t;

typedef struct {
	char ssid[24];
	char password[24];
}wifi_info_t;
 uint8_t status_bits; //later change to Eventgroup bits
 xQueueHandle receive_queue, transmit_queue;
 uint32_t bt_handle;
 int wifi_client_sock, wifi_listen_sock;
 uint16_t temp_soc1, temp_soc2, temp_full, temp_pg;

#endif /* MAIN_MAIN_H_ */
