/*
 * flash_functions.h
 *
 *  Created on: Jul 10, 2020
 *      Author: JFung
 */

#ifndef MAIN_FLASH_FUNCTIONS_H_
#define MAIN_FLASH_FUNCTIONS_H_

#include "main.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "freertos/task.h"
#include "freertos/queue.h"

typedef struct{
	char nvs_key[15];
	nvs_type_t data_type;
	size_t length; // In bytes, for reading str & blob and for write blob
	void *data_to_write;
	void *read_data;
	xQueueHandle read_completed_notif; // Used to block code until read returns value
} nvs_package_t;

esp_err_t nvs_read_write_task(void *args);

void init_nvs_tasks(void);
void show_boot_count(void);
void load_device_state_from_nvs(device_state_t *dev);
void write_device_state_to_nvs(device_state_t *dev);
bool check_device_state_in_nvs(void);
void set_device_state_in_nvs(void);

#endif /* MAIN_FLASH_FUNCTIONS_H_ */
