#ifndef __MISC_H__
#define __MISC_H__

#include "main.h"

// Helper to read/write memory in different
// The bits in memory are exactly the same, only read out in different sizes
typedef union{
  float num_f;
  int32_t num_d32;
  uint32_t num_u32;
  uint16_t num_d16[2];
  uint8_t num_byte[4];
} float_int_byte_union_t;

inline uint32_t float_to_u32(float a);

int32_t round_f_to_d32(float in);

typedef void (*func)(); // Convert function pointer to callable function

void float_to_byte(float num, uint8_t *byte);
void debug_print_device_state(device_state_t *dev);
void debug_change_device_state(device_state_t *dev);
  
#endif
