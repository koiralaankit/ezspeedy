/*
 * h3lis100dl_functions.h
 *
 *  Created on: Dec 16, 2020
 *      Author: Ankit koirala
 */

#ifndef MAIN_H3LIS100DL_FUNCTIONS_H_
#define MAIN_H3LIS100DL_FUNCTIONS_H_


void init_h3lis100dl();
void h3lis100dl_read_data_polling(void *args);
void h3lis100dl_free_fall(void *args);


#endif /* MAIN_H3LIS100DL_FUNCTIONS_H_ */
