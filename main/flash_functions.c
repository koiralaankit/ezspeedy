/*
 * flash_functions.c
 *
 *  Created on: Jul 10, 2020
 *      Author: JFung
 */

/* Prototypes */
#include "flash_functions.h"
#include "main.h"
#include "misc.h"
#include "esp_log.h"
#include "esp_err.h"

#define NVS_VALUE_U8_DEVICE_STATE_INITIALISED (uint8_t) 0x10101010

void init_nvs_tasks(void){
  // NVS tasks
  nvs_transaction_queue = xQueueCreate(10, sizeof(nvs_package_t)); //Pending read/write queue
  xTaskCreate(nvs_read_write_task, "nvs task", 4096, NULL, 2, NULL); // Task to act on queued read/writes
}

void show_boot_count(void){
  ////////
  //
  // Increment value in nvs each boot
  // Notes
  // nvs partition is at the beginning of flash.
  // locations within the nvs are handle automatically by the esp core library
  // locations are specified by strings e.g. NVS_BOOT_COUNT_KEY.
  //	 the sting must be less than 15 char
  // general access flow
  // READ: open flash -> get -> close flash
  // WRITE: open flash -> set -> commit changes i.e. write to flash ->close flash
  // check nvs.h to access flash in different sizes e.g. uint8, int64, array, string
  /*
    nvs_handle_t nvs_handle;
    int32_t nvs_test_value;
    // Open nvs
    ret = nvs_open(FLASH_STORAGE_AREA, NVS_READWRITE, &nvs_handle);
    if (ret != ESP_OK) ESP_LOGW("NVS_TEST", "Failed to open");
    // Read
    ret = nvs_get_i32(nvs_handle, NVS_BOOT_COUNT_KEY, &nvs_test_value);
    if (ret != ESP_OK && ret != ESP_ERR_NVS_NOT_FOUND) nvs_test_value = 0;
    ESP_LOGI("NVS TEST", "Boot Count: %d", nvs_test_value);
    ESP_LOGI("NVS_TEST", "Increment NVS test");
    nvs_test_value++;
    // Write incremented value to nvs
    ret = nvs_set_i32(nvs_handle, NVS_BOOT_COUNT_KEY, nvs_test_value);
    nvs_test_value = -1; // reset value to detect if not changed in nvs
    if ( ret!=ESP_OK ) ESP_LOGW("NVS_TEST","Failed to set blob");
    // Commit changes to nvs & close
    ret = nvs_commit(nvs_handle);
    if ( ret!=ESP_OK ) ESP_LOGW("NVS_TEST","Failed to commit to nvs");
    nvs_close(nvs_handle);
  */

  // NVS Boot counter using nvs_task instead of direct access i.e below
  uint32_t count = 0, comp_flag=0;
  xQueueHandle trans_completed = xQueueCreate(1,sizeof(uint32_t)); // Queue to block code during read op

  nvs_package_t nvs_package = {.nvs_key = NVS_KEY_U32_DEVICE_BOOT_COUNT, .data_type=NVS_TYPE_U32, .data_to_write=NULL,
			       .read_data = &count, .read_completed_notif = trans_completed};
  xQueueSendToBack(nvs_transaction_queue, &nvs_package, portMAX_DELAY);
  if(xQueueReceive(trans_completed, &comp_flag,portTICK_PERIOD_MS)){ // Wait until value read form NVS
    ESP_LOGI("Boot Count", "Boot count: %d", count);
  }else{
    ESP_LOGW("Boot Count", "Write timed out");
  }
  count++;
  nvs_package.data_to_write = &count;
  nvs_package.read_data = NULL;
  xQueueSendToBack(nvs_transaction_queue, &nvs_package, portMAX_DELAY);
  if(xQueueReceive(trans_completed, &comp_flag,portTICK_PERIOD_MS)){ // Wait until value read form NVS
    ESP_LOGI("Boot Count", "Boot count incremented");
  }else{
    ESP_LOGW("Boot Count", "Read timed out");
  }
  vQueueDelete(trans_completed);



}

esp_err_t nvs_read_write_task(void *args){
  nvs_package_t nvs_transaction;
  esp_err_t ret;
  nvs_handle_t nvs_handle;
  while(1){
    if(xQueueReceive(nvs_transaction_queue, &nvs_transaction, portMAX_DELAY)){
      // Error checks
      if(nvs_transaction.nvs_key == NULL){
	return ESP_FAIL;
      }

      // Transaction
      if(nvs_transaction.data_to_write==NULL
	 && nvs_transaction.read_data!=NULL){
	// Read only
	ret = nvs_open(FLASH_STORAGE_AREA, NVS_READONLY, &nvs_handle);
	if (ret != ESP_OK) ESP_LOGW(__func__, "Failed to open");

	switch(nvs_transaction.data_type){
	case NVS_TYPE_U8:
	  ret = nvs_get_u8(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((uint8_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	case NVS_TYPE_I8:
	  ret = nvs_get_i8(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((int8_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	case NVS_TYPE_U16:
	  ret = nvs_get_u16(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((uint16_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	case NVS_TYPE_I16:
	  ret = nvs_get_i16(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((int16_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	case NVS_TYPE_U32:
	  ret = nvs_get_u32(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((uint32_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	case NVS_TYPE_I32:
	  ret = nvs_get_i32(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((int32_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	case NVS_TYPE_U64:
	  ret = nvs_get_u64(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((uint64_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	case NVS_TYPE_I64:
	  ret = nvs_get_i64(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((int64_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	case NVS_TYPE_STR:
	  ret = nvs_get_str(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data, &nvs_transaction.length);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((uint8_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	case NVS_TYPE_BLOB:
	  ret = nvs_get_blob(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.read_data, &nvs_transaction.length);
	  if (ret != ESP_OK){
	    ESP_LOGE(__func__, "NVS read failed");
	    *((uint8_t *) nvs_transaction.read_data)=0;
	  }
	  break;
	default:
	  ESP_LOGE(__func__, "Unknown NVS data type");
	}
	nvs_close(nvs_handle); // End
	if(nvs_transaction.read_completed_notif != NULL){
	  xQueueSendToBack(nvs_transaction.read_completed_notif, &ret , portMAX_DELAY);
	}
      }else if(nvs_transaction.data_to_write!=NULL && nvs_transaction.read_data==NULL){
	// Write only
	if(nvs_transaction.data_type >= NVS_TYPE_STR){
	  if(nvs_transaction.length == 0){
	    ESP_LOGE(__func__, "NVS write length not specified");
	    return ESP_FAIL;
	  }
	}
	ret = nvs_open(FLASH_STORAGE_AREA, NVS_READWRITE, &nvs_handle);
	if (ret != ESP_OK) ESP_LOGW("NVS_TEST", "Failed to open");
	switch(nvs_transaction.data_type){
	case NVS_TYPE_U8:
	  ret = nvs_set_u8(nvs_handle, nvs_transaction.nvs_key, *((uint8_t *) nvs_transaction.data_to_write));
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	case NVS_TYPE_I8:
	  ret = nvs_set_i8(nvs_handle, nvs_transaction.nvs_key, *((int8_t *) nvs_transaction.data_to_write));
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	case NVS_TYPE_U16:
	  ret = nvs_set_u16(nvs_handle, nvs_transaction.nvs_key, *((uint16_t *) nvs_transaction.data_to_write));
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	case NVS_TYPE_I16:
	  ret = nvs_set_i8(nvs_handle, nvs_transaction.nvs_key, *((int16_t *) nvs_transaction.data_to_write));
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	case NVS_TYPE_U32:
	  ret = nvs_set_u32(nvs_handle, nvs_transaction.nvs_key, *((uint32_t *) nvs_transaction.data_to_write));
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	case NVS_TYPE_I32:
	  ret = nvs_set_i32(nvs_handle, nvs_transaction.nvs_key, *((int32_t *) nvs_transaction.data_to_write));
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	case NVS_TYPE_U64:
	  ret = nvs_set_u64(nvs_handle, nvs_transaction.nvs_key, *((uint64_t *) nvs_transaction.data_to_write));
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	case NVS_TYPE_I64:
	  ret = nvs_set_i64(nvs_handle, nvs_transaction.nvs_key, *((int64_t*) nvs_transaction.data_to_write));
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	case NVS_TYPE_STR:
	  ret = nvs_set_str(nvs_handle, nvs_transaction.nvs_key, (char *) nvs_transaction.data_to_write);
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	case NVS_TYPE_BLOB:
	  ret = nvs_set_blob(nvs_handle, nvs_transaction.nvs_key, nvs_transaction.data_to_write, nvs_transaction.length);
	  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS write failed");
	  break;
	default:
	  ESP_LOGE(__func__, "Unknown NVS data type");
	}
	ret = nvs_commit(nvs_handle);
	if ( ret!=ESP_OK ) ESP_LOGW(__func__,"Failed to commit to NVS");
	nvs_close(nvs_handle); // End
	if(nvs_transaction.read_completed_notif != NULL){
	  xQueueSendToBack(nvs_transaction.read_completed_notif, &ret , portMAX_DELAY);
	}
      }else{
	ESP_LOGE(__func__, "Unknown transaction");
	return ESP_FAIL;
      }
    }
  }
}

void load_device_state_from_nvs(device_state_t *dev){
  nvs_handle_t nvs_handle;
  esp_err_t ret;
  
  // Open nvs
  ret = nvs_open(FLASH_STORAGE_AREA, NVS_READONLY, &nvs_handle);
  if (ret != ESP_OK) ESP_LOGW(__func__, "Failed to open NVS");

  // Device
  ret = nvs_get_u32(nvs_handle, NVS_KEY_U32_DEVICE_USAGE_COUNT, &(dev->device_usage_count));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_U32_DEVICE_USAGE_COUNT);}

  ret = nvs_get_u8(nvs_handle, NVS_KEY_U8_DEVICE_CODE_ENTERED , &(dev->device_code_entered));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_U8_DEVICE_CODE_ENTERED);}

  ret = nvs_get_u8(nvs_handle, NVS_KEY_U8_DEVICE_LOCKED , &(dev->device_locked));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_U8_DEVICE_LOCKED);}
 
  ret = nvs_get_u8(nvs_handle, NVS_KEY_U8_DEVICE_SLEEP_TIMER_ENABLE, &(dev->device_sleep_timer_enabled));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_U8_DEVICE_SLEEP_TIMER_ENABLE);}

  // Calibration
  ret = nvs_get_u8(nvs_handle, NVS_KEY_U8_CALIBRATION_DAY, &(dev->calibration.day));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_U8_CALIBRATION_DAY);}

  ret = nvs_get_u8(nvs_handle, NVS_KEY_U8_CALIBRATION_MONTH, &(dev->calibration.month));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_U8_CALIBRATION_MONTH);}

  ret = nvs_get_u16(nvs_handle, NVS_KEY_U16_CALIBRATION_YEAR, &(dev->calibration.year));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_U16_CALIBRATION_YEAR);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_CALIBRATION_FACTOR_SPEED, &(dev->calibration.factor_speed));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_F_CALIBRATION_FACTOR_SPEED);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_CALIBRATION_PREV_FACTOR_SPEED, &(dev->calibration.prev_factor_speed));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_F_CALIBRATION_PREV_FACTOR_SPEED);}

  // Tolerance
  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_TOLERANCE_UPPER_TOLERANCE, &(dev->tolerance.upper_tolerance));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_F_TOLERANCE_UPPER_TOLERANCE);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_TOLERANCE_UPPER_WARNING, &(dev->tolerance.upper_warning));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_F_TOLERANCE_UPPER_WARNING);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_TOLERANCE_LOWER_TOLERANCE, &(dev->tolerance.lower_tolerance));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_F_TOLERANCE_LOWER_TOLERANCE);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_TOLERANCE_LOWER_WARNING, &(dev->tolerance.lower_warning));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_F_TOLERANCE_LOWER_WARNING);}

  uint8_t tolerance_enable_reg;
  ret = nvs_get_u8(nvs_handle, NVS_KEY_U8_TOLERANCE_ENABLE_REG, &(tolerance_enable_reg));
  if (ret != ESP_OK){
    ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_U8_TOLERANCE_ENABLE_REG);
  }else{
    dev->tolerance.lower_tolerance_enable = (tolerance_enable_reg & LOWER_TOLERANCE_ENABLE_BIT_MASK) >> LOWER_TOLERANCE_ENABLE_BIT;
    dev->tolerance.lower_warning_enable = (tolerance_enable_reg & LOWER_WARNING_ENABLE_BIT_MASK) >> LOWER_WARNING_ENABLE_BIT;
    dev->tolerance.upper_tolerance_enable = (tolerance_enable_reg & UPPER_TOLERANCE_ENABLE_BIT_MASK) >> UPPER_TOLERANCE_ENABLE_BIT;
    dev->tolerance.upper_warning_enable = (tolerance_enable_reg & UPPER_WARNING_ENABLE_BIT_MASK) >> UPPER_WARNING_ENABLE_BIT;
    dev->tolerance.gap_tolerance_enable = (tolerance_enable_reg & GAP_TOLERANCE_ENABLE_BIT_MASK) >> GAP_TOLERANCE_ENABLE_BIT;
    dev->tolerance.m_angle_enable = (tolerance_enable_reg & M_ANGLE_ENABLE_BIT_MASK) >> M_ANGLE_ENABLE_BIT;
  }
  
  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_TOLERANCE_CURRENT_RADIUS, &(dev->tolerance.current_radius));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_CURRENT_RADIUS);}
  
  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_TOLERANCE_ANGULAR_SPEED, &(dev->tolerance.angular_speed));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_ANGULAR_SPEED);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_TOLERANCE_GAP_TOLERANCE, &(dev->tolerance.gap_tolerance));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_GAP_TOLERANCE);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_TOLERANCE_M_ANGLE, &(dev->tolerance.m_angle));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_M_ANGLE);}
  
  /*
  // Gap - keys do not exist
  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_GAP_LOWEST_CLOSED_1, &(dev->gap.lowest_closed_1));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_LOWEST_CLOSED_1);}
  
  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_GAP_LOWEST_CLOSED_2, &(dev->gap.lowest_closed_2));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_LOWEST_CLOSED_2);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_GAP_LOWEST_CLOSED_SPEED_1, &(dev->gap.lowest_closed_1));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_LOWEST_CLOSED_SPEED_1);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_GAP_LOWEST_CLOSED_SPEED_2, &(dev->gap.lowest_closed_2));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_LOWEST_CLOSED_SPEED_2);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_GAP_HIGHEST_NOT_CLOSED_1, &(dev->gap.lowest_closed_1));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_HIGHEST_NOT_CLOSED_1);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_GAP_HIGHEST_NOT_CLOSED_2, &(dev->gap.lowest_closed_2));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_HIGHEST_NOT_CLOSED_2);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_GAP_HIGHEST_CLOSED_SPEED_1, &(dev->gap.lowest_closed_1));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_HIGHEST_CLOSED_SPEED_1);}

  ret = nvs_get_u32(nvs_handle, NVS_KEY_F_GAP_HIGHEST_CLOSED_SPEED_2, &(dev->gap.lowest_closed_2));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_HIGHEST_CLOSED_SPEED_2);}
  */

  nvs_close(nvs_handle);

}

void write_device_state_to_nvs(device_state_t *dev){
  nvs_handle_t nvs_handle;
  esp_err_t ret;
  
  // Open nvs
  ret = nvs_open(FLASH_STORAGE_AREA, NVS_READWRITE, &nvs_handle);
  if (ret != ESP_OK) ESP_LOGW(__func__, "Failed to open NVS");

  // Device
  ret = nvs_set_u32(nvs_handle, NVS_KEY_U32_DEVICE_USAGE_COUNT, dev->device_usage_count);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_U32_DEVICE_USAGE_COUNT);}

  ret = nvs_set_u8(nvs_handle, NVS_KEY_U8_DEVICE_CODE_ENTERED , dev->device_code_entered);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_U8_DEVICE_CODE_ENTERED);}

  ret = nvs_set_u8(nvs_handle, NVS_KEY_U8_DEVICE_LOCKED , dev->device_locked);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_U8_DEVICE_LOCKED);}
 
  ret = nvs_set_u8(nvs_handle, NVS_KEY_U8_DEVICE_SLEEP_TIMER_ENABLE, dev->device_sleep_timer_enabled);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_U8_DEVICE_SLEEP_TIMER_ENABLE);}

  // Calibration
  ret = nvs_set_u8(nvs_handle, NVS_KEY_U8_CALIBRATION_DAY, dev->calibration.day);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_U8_CALIBRATION_DAY);}

  ret = nvs_set_u8(nvs_handle, NVS_KEY_U8_CALIBRATION_MONTH, dev->calibration.month);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_U8_CALIBRATION_MONTH);}

  ret = nvs_set_u16(nvs_handle, NVS_KEY_U16_CALIBRATION_YEAR, dev->calibration.year);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_U16_CALIBRATION_YEAR);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_CALIBRATION_FACTOR_SPEED, float_to_u32(dev->calibration.factor_speed));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_CALIBRATION_FACTOR_SPEED);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_CALIBRATION_PREV_FACTOR_SPEED, float_to_u32(dev->calibration.prev_factor_speed));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_CALIBRATION_PREV_FACTOR_SPEED);}
  
  // Tolerance
  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_TOLERANCE_UPPER_TOLERANCE, float_to_u32(dev->tolerance.upper_tolerance));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_UPPER_TOLERANCE);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_TOLERANCE_UPPER_WARNING, float_to_u32(dev->tolerance.upper_warning));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_UPPER_WARNING);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_TOLERANCE_LOWER_TOLERANCE, float_to_u32(dev->tolerance.lower_tolerance));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_LOWER_TOLERANCE);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_TOLERANCE_LOWER_WARNING, float_to_u32(dev->tolerance.lower_warning));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_LOWER_WARNING);}

  uint8_t tolerance_enable_reg = 0;
  tolerance_enable_reg |= (dev->tolerance.lower_tolerance_enable << LOWER_TOLERANCE_ENABLE_BIT) & LOWER_TOLERANCE_ENABLE_BIT_MASK;
  tolerance_enable_reg |= (dev->tolerance.lower_warning_enable << LOWER_WARNING_ENABLE_BIT) & LOWER_WARNING_ENABLE_BIT_MASK;
  tolerance_enable_reg |= (dev->tolerance.upper_tolerance_enable << UPPER_TOLERANCE_ENABLE_BIT) & UPPER_TOLERANCE_ENABLE_BIT_MASK;
  tolerance_enable_reg |= (dev->tolerance.upper_warning_enable << UPPER_WARNING_ENABLE_BIT) & UPPER_WARNING_ENABLE_BIT_MASK; 
  tolerance_enable_reg |= (dev->tolerance.gap_tolerance_enable << GAP_TOLERANCE_ENABLE_BIT) & GAP_TOLERANCE_ENABLE_BIT_MASK;
  tolerance_enable_reg |= (dev->tolerance.m_angle_enable << M_ANGLE_ENABLE_BIT) & M_ANGLE_ENABLE_BIT_MASK;

  ret = nvs_set_u8(nvs_handle, NVS_KEY_U8_TOLERANCE_ENABLE_REG, tolerance_enable_reg);
  if (ret != ESP_OK) ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_U8_TOLERANCE_ENABLE_REG);

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_TOLERANCE_ANGULAR_SPEED, float_to_u32(dev->tolerance.angular_speed));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_ANGULAR_SPEED);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_TOLERANCE_GAP_TOLERANCE, float_to_u32(dev->tolerance.gap_tolerance));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_GAP_TOLERANCE);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_TOLERANCE_M_ANGLE, float_to_u32(dev->tolerance.m_angle));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_M_ANGLE);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_TOLERANCE_CURRENT_RADIUS, float_to_u32(dev->tolerance.current_radius));
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_TOLERANCE_CURRENT_RADIUS);}

  /*
  // Gap - keys do not exist
  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_GAP_LOWEST_CLOSED_1, dev->gap.lowest_closed_1);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_LOWEST_CLOSED_1);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_GAP_LOWEST_CLOSED_2, dev->gap.lowest_closed_2);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_LOWEST_CLOSED_2);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_GAP_LOWEST_CLOSED_SPEED_1, dev->gap.lowest_closed_1);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_LOWEST_CLOSED_SPEED_1);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_GAP_LOWEST_CLOSED_SPEED_2, dev->gap.lowest_closed_2);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_LOWEST_CLOSED_SPEED_2);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_GAP_HIGHEST_NOT_CLOSED_1, dev->gap.lowest_closed_1);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_HIGHEST_NOT_CLOSED_1);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_GAP_HIGHEST_NOT_CLOSED_2, dev->gap.lowest_closed_2);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_HIGHEST_NOT_CLOSED_2);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_GAP_HIGHEST_CLOSED_SPEED_1, dev->gap.lowest_closed_1);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_HIGHEST_CLOSED_SPEED_1);}

  ret = nvs_set_u32(nvs_handle, NVS_KEY_F_GAP_HIGHEST_CLOSED_SPEED_2, dev->gap.lowest_closed_2);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_F_GAP_HIGHEST_CLOSED_SPEED_2);}
  */
  
  ret = nvs_commit(nvs_handle);
  if ( ret!=ESP_OK ) ESP_LOGE(__func__,"Failed to commit to NVS");
  
  nvs_close(nvs_handle);
 
}

/************************************************************/
/* Check if device state has previously been saved in flash */
/************************************************************/
bool check_device_state_in_nvs(void){
  nvs_handle_t nvs_handle;
  esp_err_t ret;
  uint8_t init;

  // Open nvs
  ret = nvs_open(FLASH_STORAGE_AREA, NVS_READONLY, &nvs_handle);
  if (ret != ESP_OK) ESP_LOGW(__func__, "Failed to open NVS");

  // Check previously settings initalised
  ret = nvs_get_u8(nvs_handle, NVS_KEY_U8_DEVICE_STATE_INITIALISED, &init);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Read ERROR: %s", NVS_KEY_U8_DEVICE_STATE_INITIALISED);}

  nvs_close(nvs_handle);
  
  if(init == NVS_VALUE_U8_DEVICE_STATE_INITIALISED){
    // NVS has been previously initialised
    // Load settings
    return TRUE;
  }

  return FALSE;
}

/*********************************************/
/* Set flag indicated device state is in NVS */
/*********************************************/
void set_device_state_in_nvs(void){
  nvs_handle_t nvs_handle;
  esp_err_t ret = ESP_OK;
  const uint8_t init = NVS_VALUE_U8_DEVICE_STATE_INITIALISED;

  // Open nvs
  ret |= nvs_open(FLASH_STORAGE_AREA, NVS_READWRITE, &nvs_handle);
  if (ret != ESP_OK) ESP_LOGW(__func__, "Failed to open NVS");

  // Set settings initalised 
  ret |= nvs_set_u8(nvs_handle, NVS_KEY_U8_DEVICE_STATE_INITIALISED, init);
  if (ret != ESP_OK){ ESP_LOGE(__func__, "NVS Write ERROR: %s", NVS_KEY_U8_DEVICE_STATE_INITIALISED);}

  ret |= nvs_commit(nvs_handle);
  if ( ret!=ESP_OK ) ESP_LOGE(__func__,"Failed to commit to NVS");
  
  nvs_close(nvs_handle);

  if(ret == ESP_OK){
    ESP_LOGI(__func__, "Device state init flag set");
  }
} 
