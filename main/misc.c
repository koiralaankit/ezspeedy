#include "misc.h"

// Convert bewteen float and bitstring, which is seperated into bytes
// e.g. 12.3456f =  0x41458794
// byte[0] = 0x41
// byte[1] = 0x45
// etc. (endian might be swapped)
void float_to_byte(float num, uint8_t *byte){
  float_int_byte_union_t tmp = {.num_f = num};
  byte[3] = tmp.num_byte[0];
  byte[2] = tmp.num_byte[1];
  byte[1] = tmp.num_byte[2];
  byte[0] = tmp.num_byte[3];
}

extern inline uint32_t float_to_u32(float a){
  float_int_byte_union_t tmp = {.num_f = a};
  return tmp.num_u32;
}

int32_t round_f_to_d32(float in){
  if(in == 0){
    return 0;
  }
  if(in > 0){
    in += 0.5;
  }else{
    // in < 0
    in -= 0.5;
  }
  return (int32_t) in;
}

void debug_print_device_state(device_state_t *dev){
  ESP_LOGI(__func__, "START Device state");
  
  // Tolerances
  ESP_LOGI(__func__, "Tolerances LT %f LW %f LTEN %d LWEN %d", dev->tolerance.lower_tolerance, dev->tolerance.lower_warning, dev->tolerance.lower_tolerance_enable, dev->tolerance.lower_warning_enable);
  ESP_LOGI(__func__, "Tolerances UT %f UW %f UTEN %d UWEN %d", dev->tolerance.upper_tolerance, dev->tolerance.upper_warning, dev->tolerance.upper_tolerance_enable, dev->tolerance.upper_warning_enable);
  ESP_LOGI(__func__, "Tolerances Current Radius %f Anuglar Speed %f", dev->tolerance.current_radius, dev->tolerance.angular_speed);
  ESP_LOGI(__func__, "Tolerances Gyro tol %f M angle %f Gyro tol EN %d m angle EN %d", dev->tolerance.gap_tolerance, dev->tolerance.m_angle, dev->tolerance.gap_tolerance_enable, dev->tolerance.m_angle_enable);

  // Calibration
  ESP_LOGI(__func__, "Calibration Day %d Month %d Year %d Factor Speed %f Prev Factor Speed %f", dev->calibration.day, dev->calibration.month, dev->calibration.year, dev->calibration.factor_speed, dev->calibration.prev_factor_speed);

  // Gap
  ESP_LOGI(__func__, "Gap LC1 %d LC2 %d LCS1 %d LCS2 %d", dev->gap.lowest_closed_1, dev->gap.lowest_closed_2, dev->gap.lowest_closed_speed_1, dev->gap.lowest_closed_speed_2);
  ESP_LOGI(__func__, "Gap HC1 %d HC2 %d HCS1 %d HCS2 %d", dev->gap.highest_not_closed_1, dev->gap.highest_not_closed_2, dev->gap.highest_not_closed_speed_1, dev->gap.highest_not_closed_speed_2);

  // Door
  ESP_LOGI(__func__,"Active door = %d. TR=0, RR=1, FR=2, RL=3, FL=4, HD=5, OTHER=6", dev->active_door);

  // Device code
  ESP_LOGI(__func__,"Code entered %d", dev->device_code_entered);

  // Usage count
  ESP_LOGI(__func__,"Usage Count %d", dev->device_usage_count);

  // Dev lock
  ESP_LOGI(__func__,"Dev locked %d", dev->device_locked);

  // Sleep timer en
  ESP_LOGI(__func__,"Sleep Timer EN %d", dev->device_sleep_timer_enabled);

  ESP_LOGI(__func__, "END device state");
}

void debug_change_device_state(device_state_t *dev){
  ESP_LOGI(__func__,"Changing device state values");
  dev->tolerance.lower_tolerance = 1234.5;
  dev->tolerance.lower_warning = 888.8;
  dev->tolerance.lower_tolerance_enable = TRUE;
  dev->tolerance.lower_warning_enable = FALSE;
  dev->tolerance.upper_tolerance = 5678.9;
  dev->tolerance.upper_warning = 666.6;
  dev->tolerance.upper_tolerance_enable = FALSE;
  dev->tolerance.upper_warning_enable = TRUE;
  dev->tolerance.current_radius = 1.0001;
  dev->tolerance.angular_speed = 2.0002;
  dev->tolerance.gap_tolerance = 0.1234;
  dev->tolerance.m_angle = 0.5678;
  dev->tolerance.gap_tolerance_enable = TRUE;
  dev->tolerance.m_angle_enable = FALSE;
  dev->calibration.day = 12;
  dev->calibration.month = 3;
  dev->calibration.year = 2004;
  dev->calibration.factor_speed = 3.1415;
  dev->calibration.prev_factor_speed = 3.1;

  dev->gap.lowest_closed_1 = 555;
  dev->gap.lowest_closed_2 = 444;
  dev->gap.lowest_closed_speed_1 = 333;
  dev->gap.lowest_closed_speed_2 = 222;
  dev->gap.highest_not_closed_1 = 111;
  dev->gap.highest_not_closed_2 = 999;
  dev->gap.highest_not_closed_speed_1 = 888;
  dev->gap.highest_not_closed_speed_2 = 777;
  dev->active_door = 1;

  dev->device_usage_count = 34850;
  dev->device_code_entered = TRUE;
  dev->device_locked = FALSE;
  dev->device_sleep_timer_enabled = TRUE;
}
