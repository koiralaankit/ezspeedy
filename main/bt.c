/*
 * bt.c
 *
 *  Created on: Oct 29, 2020
 *      Author:
 */
#include "bt.h"
#include "esp_bt.h"
#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_bt_device.h"
#include "esp_spp_api.h"

#include "main.h"

#if 1
static const esp_spp_sec_t sec_mask = ESP_SPP_SEC_AUTHENTICATE;
static const esp_spp_role_t role_slave = ESP_SPP_ROLE_MASTER;
static const esp_spp_mode_t esp_spp_mode = ESP_SPP_MODE_CB;//ESP_SPP_MODE_CB;

//temp print function to print message- comment out
void print(uint8_t* a, uint8_t len)
{
	printf(" Message = ");
	for(int i = 0; i<len; i++)
		printf("%x", a[i]);

}

// @function description
// BT event handler
// @params
// Default params from BT examples
// @return
// None
void esp_spp_cb(esp_spp_cb_event_t event, esp_spp_cb_param_t *param) {
  //static uint8_t byteCount = 0; //comment out- counter to count number of bytes received from bluetooth
 // uint8_t dummyPackage[8] = {0x2A,0x00,0x00,0x53,0x4E,0x00,0x00,0x7E};
  static bool msg_started = false;
  message_package_t message_package;
  static uint32_t thandle=0;
  static uint32_t tlength=0;
  static uint8_t  tmsg[MSG_MAX_LEN]={0x00};
  switch (event) {
  case ESP_SPP_INIT_EVT:
    ESP_LOGI(SPP_TAG, "ESP_SPP_INIT_EVT");
    esp_bt_dev_set_device_name(BT_DEVICE_NAME);
    esp_bt_gap_set_scan_mode(ESP_BT_CONNECTABLE,
			     ESP_BT_GENERAL_DISCOVERABLE);
    esp_spp_start_srv(sec_mask, role_slave, 0, SPP_SERVER_NAME);
    break;
  case ESP_SPP_DISCOVERY_COMP_EVT:
    ESP_LOGI(SPP_TAG, "ESP_SPP_DISCOVERY_COMP_EVT");
    break;
  case ESP_SPP_OPEN_EVT: // On connect - set connection handle and eventbits
    ESP_LOGI(SPP_TAG, "ESP_SPP_OPEN_EVT");
    bt_handle = param->open.handle;

    break;
  case ESP_SPP_CLOSE_EVT: // On disconnect
    ESP_LOGI(SPP_TAG, "ESP_SPP_CLOSE_EVT");
    xEventGroupClearBits( deviceStatusGroup, BT_ACTIVE);
    // Disable ADC
    //adc_stop_conversion();
    break;
  case ESP_SPP_START_EVT:
    ESP_LOGI(SPP_TAG, "ESP_SPP_START_EVT");
    break;
  case ESP_SPP_CL_INIT_EVT:
    ESP_LOGI(SPP_TAG, "ESP_SPP_CL_INIT_EVT");
    break;
  case ESP_SPP_DATA_IND_EVT: // On data received
    //ESP_LOGI(SPP_TAG, "ESP_SPP_DATA_IND_EVT len=%d handle=%d", param->data_ind.len, param->data_ind.handle);
    //esp_log_buffer_hex(SPP_TAG, param->data_ind.data,param->data_ind.len);
	  memset(message_package.message, 0x00, param->data_ind.len);
	  message_package.handle = param->data_ind.handle;
	  message_package.length = param->data_ind.len;
	  memcpy((uint8_t *)message_package.message, param->data_ind.data, param->data_ind.len);
	  xQueueSendToBack(bt_message_queue, (void *)&message_package, portMAX_DELAY);
//	  thandle = param->data_ind.handle;
//	  bt_handle = param->data_ind.handle;
//	  ESP_LOGI(__func__, "handle ----- == %d", bt_handle);
//	  if(*param->data_ind.data == MSG_HEADER)
//	  {
//		  msg_started = true;
//	  }
//	  if(msg_started)
//	  {
//
//		  memcpy(&tmsg[tlength], param->data_ind.data,  param->data_ind.len);
//		  tlength = tlength + param->data_ind.len;
//		  if(*param->data_ind.data== MSG_FOOTER) msg_started = false;
//
//	  }
//	  if(tlength >0 && !msg_started)
//	  {
//		//  memset(message_package.message, 0, MSG_MAX_LEN);
//		//  memcpy(message_package.message, dummyPackage, 8);
//		  message_package.handle = thandle;
//		  message_package.length = tlength;
//		  memcpy(message_package.message, tmsg, MSG_MAX_LEN);
//		  if (xQueueSendToBack(receive_queue, (void* ) &message_package,
//		  			 portMAX_DELAY))
//		  {
//
//			  	memset(tmsg, 0, MSG_MAX_LEN);
//			    thandle = 0;
//			    tlength = 0;
//
//		        ESP_LOGI(SPP_TAG, "SPP Data enqueued len=%d handle=%d", message_package.length, message_package.handle);
//		        print(&message_package.message[0], message_package.length);
//		        //esp_log_buffer_hex(SPP_TAG,message_package.message,message_package.length);
//		      } else {
//		        ESP_LOGE(SPP_TAG, "RECEIVE QUEUE FULL. MESSAGE NOT QUEUED");
//		        ESP_LOGE(SPP_TAG, "ESP_SPP_DATA_IND_EVT len=%d handle=%d",
//		  	       param->data_ind.len, param->data_ind.handle);
//		        esp_log_buffer_hex(SPP_TAG,param->data_ind.data,param->data_ind.len);
//		      }
//
//	  }

    // Copy data in param into message_package format - uncomment AK
//    message_package.handle = param->data_ind.handle;
//    message_package.length = param->data_ind.len;
//    memcpy((char*) message_package.message, (char*) param->data_ind.data, (size_t) param->data_ind.len);
//    if (xQueueSendToBack(receive_queue, (void* ) &message_package,
//			 portMAX_DELAY)) {
//      ESP_LOGI(SPP_TAG, "SPP Data enqueued len=%d handle=%d", message_package.length, message_package.handle);
//      //esp_log_buffer_hex(SPP_TAG,message_package.message,message_package.length);
//    } else {
//      ESP_LOGE(SPP_TAG, "RECEIVE QUEUE FULL. MESSAGE NOT QUEUED");
//      ESP_LOGE(SPP_TAG, "ESP_SPP_DATA_IND_EVT len=%d handle=%d",
//	       param->data_ind.len, param->data_ind.handle);
//      esp_log_buffer_hex(SPP_TAG,param->data_ind.data,param->data_ind.len);
//    }
    break;
  case ESP_SPP_CONG_EVT: // Congestion
    ESP_LOGE(SPP_TAG, "ESP_SPP_CONG_EVT");
    break;
  case ESP_SPP_WRITE_EVT: // On data transmit
    //ESP_LOGI(SPP_TAG, "ESP_SPP_WRITE_EVT");
    break;
  case ESP_SPP_SRV_OPEN_EVT:
    ESP_LOGI(SPP_TAG, "ESP_SPP_SRV_OPEN_EVT");
    bt_handle = param->data_ind.handle;
    uxBits = xEventGroupSetBits(deviceStatusGroup, BT_ACTIVE);
    break;
  default:
    break;
  }
}

// @function description
// Generic from example code to handle pairing security
// @params
// @return
void esp_bt_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param) {
  switch (event) {
  case ESP_BT_GAP_AUTH_CMPL_EVT:
    if (param->auth_cmpl.stat == ESP_BT_STATUS_SUCCESS) {
      ESP_LOGI(SPP_TAG, "authentication success: %s",
	       param->auth_cmpl.device_name);
      esp_log_buffer_hex(SPP_TAG, param->auth_cmpl.bda, ESP_BD_ADDR_LEN);
    } else {
      ESP_LOGE(SPP_TAG, "authentication failed, status:%d",
	       param->auth_cmpl.stat);
    }
    break;
  case ESP_BT_GAP_PIN_REQ_EVT:
    ESP_LOGI(SPP_TAG, "ESP_BT_GAP_PIN_REQ_EVT min_16_digit:%d",
	     param->pin_req.min_16_digit);
    if (param->pin_req.min_16_digit) {
      ESP_LOGI(SPP_TAG, "Input pin code: 0000 0000 0000 0000");
      esp_bt_pin_code_t pin_code = { 0 };
      esp_bt_gap_pin_reply(param->pin_req.bda, true, 16, pin_code);
    } else {
      ESP_LOGI(SPP_TAG, "Input pin code: 1234");
      esp_bt_pin_code_t pin_code;
      pin_code[0] = '1';
      pin_code[1] = '2';
      pin_code[2] = '3';
      pin_code[3] = '4';
      esp_bt_gap_pin_reply(param->pin_req.bda, true, 4, pin_code);
    }
    break;
  case ESP_BT_GAP_CFM_REQ_EVT:
    ESP_LOGI(SPP_TAG,
	     "ESP_BT_GAP_CFM_REQ_EVT Please compare the numeric value: %d",
	     param->cfm_req.num_val);
    esp_bt_gap_ssp_confirm_reply(param->cfm_req.bda, true);
    break;
  case ESP_BT_GAP_KEY_NOTIF_EVT:
    ESP_LOGI(SPP_TAG, "ESP_BT_GAP_KEY_NOTIF_EVT passkey:%d",
	     param->key_notif.passkey);
    break;
  case ESP_BT_GAP_KEY_REQ_EVT:
    ESP_LOGI(SPP_TAG, "ESP_BT_GAP_KEY_REQ_EVT Please enter passkey!");
    break;
  default:
    ESP_LOGI(SPP_TAG, "event: %d", event);
    break;
  }
  return;
}

// @function description
// Generic code to initialise BT stack
// @params
// @return
void init_bt(void) {
  esp_err_t ret;

  ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_BLE));

  esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
  if ((ret = esp_bt_controller_init(&bt_cfg)) != ESP_OK) {
    ESP_LOGE(SPP_TAG, "%s initialize controller failed: %s\n", __func__, esp_err_to_name(ret));
    return;
  }

  if ((ret = esp_bt_controller_enable(ESP_BT_MODE_CLASSIC_BT)) != ESP_OK) {
    ESP_LOGE(SPP_TAG, "%s enable controller failed: %s\n", __func__,
	     esp_err_to_name(ret));
    return;
  }

  if ((ret = esp_bluedroid_init()) != ESP_OK) {
    ESP_LOGE(SPP_TAG, "%s initialize bluedroid failed: %s\n", __func__,
	     esp_err_to_name(ret));
    return;
  }

  if ((ret = esp_bluedroid_enable()) != ESP_OK) {
    ESP_LOGE(SPP_TAG, "%s enable bluedroid failed: %s\n", __func__,
	     esp_err_to_name(ret));
    return;
  }

  if ((ret = esp_bt_gap_register_callback(esp_bt_gap_cb)) != ESP_OK) {
    ESP_LOGE(SPP_TAG, "%s gap register failed: %s\n", __func__,
	     esp_err_to_name(ret));
    return;
  }

  if ((ret = esp_spp_register_callback(esp_spp_cb)) != ESP_OK) {
    ESP_LOGE(SPP_TAG, "%s spp register failed: %s\n", __func__,
	     esp_err_to_name(ret));
    return;
  }

  if ((ret = esp_spp_init(esp_spp_mode)) != ESP_OK) {
    ESP_LOGE(SPP_TAG, "%s spp init failed: %s\n", __func__,
	     esp_err_to_name(ret));
    return;
  }

  /* Set default parameters for Secure Simple Pairing */
  esp_bt_sp_param_t param_type = ESP_BT_SP_IOCAP_MODE;
  esp_bt_io_cap_t iocap = ESP_BT_IO_CAP_IO;
  esp_bt_gap_set_security_param(param_type, &iocap, sizeof(uint8_t));

  /*
   * Set default parameters for Legacy Pairing
   * Use variable pin, input pin code when pairing
   */
  esp_bt_pin_type_t pin_type = ESP_BT_PIN_TYPE_VARIABLE;
  esp_bt_pin_code_t pin_code;
  esp_bt_gap_set_pin(pin_type, 0, pin_code);
}

#endif
void send_soc_to_bt(void *args)
{
	char *soc = calloc(32, sizeof(char));
	char *rateS = calloc(15, sizeof(char));
	static uint16_t incbyone = 0;

	uint16_t lastsoc = temp_soc1;
	while(1)
	{

		sprintf(soc, "soc1 = %d soc2 = %d", temp_pg, temp_soc2);
		if(temp_soc1 != lastsoc)
		{

		    sprintf(rateS , "inc_at = %ds\n",incbyone);
			incbyone = 0;
			lastsoc = temp_soc1;
			//esp_spp_write(bt_handle, 15, (uint8_t*)rateS);

		}

		incbyone++;
		sprintf(rateS, "full = %d\n", temp_full);
		esp_spp_write(bt_handle, 15, (uint8_t*)rateS);
		esp_spp_write(bt_handle, 32, (uint8_t*)soc);

		vTaskDelay(pdMS_TO_TICKS(500));
	}

}
