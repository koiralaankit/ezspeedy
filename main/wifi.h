/*
 * wifi.h
 *
 *  Created on: Oct 29, 2020
 *      Author: Ankit koirala
 */

#ifndef MAIN_WIFI_H_
#define MAIN_WIFI_H_

/* =================== */
/* WiFi Server Defines */
/* =================== */
#define AP_WIFI_SSID "EZ Pinch 9001"
#define AP_WIFI_PASS "1234567890"
#define STA_WIFI_SSID "EZMetrology"//"ATT952AqNa"//"EZMetrology"
#define STA_WIFI_PASS "EZMetrology2006"//"i?hsjgx?9%ej"//"EZMetrology2006"
#define MAX_AP_CONN 1
#define MAX_STA_CONN_RETRY 3 // not implemented atm
#define PORT 5020 // Can be anything from 1-65536 (Though should not use 1-1024)
// Wifi event group but definitions
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1
#define GOT_IPV4_BIT BIT(0)
#define GOT_IPV6_BIT BIT(1)

#ifdef CONFIG_EXAMPLE_CONNECT_IPV6
#define CONNECTED_BITS (GOT_IPV4_BIT | GOT_IPV6_BIT)
#else
#define CONNECTED_BITS (GOT_IPV4_BIT)
#endif

/* ============== */
/* WiFi Functions */
/* ============== */
/* set up connection, Wi-Fi or Ethernet */
void start(void);

/* tear down connection, release resources */
void stop(void);
void tcp_server_task(void *pvParameters);
void on_wifi_disconnect_from_ap(void *arg, esp_event_base_t event_base,
				       int32_t event_id, void *event_data);
void wifi_mode_change_task(void *args);
esp_err_t wifi_disconnect(void);
void on_client_disconnect_from_ap(void);
void on_got_client_ip(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
esp_err_t wifi_connect_task(void *args);
void copy_message(const int sock);
void tcp_server_handler_task(void *args);





#endif /* MAIN_WIFI_H_ */
