
/*
******************************************************************************
* @file    read_data_simple.c
* @author  Sensors Software Solution Team
* @brief   ISM330DLC driver file
******************************************************************************
* @attention
*
* <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
* All rights reserved.</center></h2>
*
* This software component is licensed by ST under BSD 3-Clause license,
* the "License"; You may not use this file except in compliance with the
* License. You may obtain a copy of the License at:
*                        opensource.org/licenses/BSD-3-Clause
*
******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "sensor_ism330dlc_functions.h"

#include <sensor_algorithm.h>
#include <string.h>
#include <stdio.h>

#include "main.h"

#include "esp_log.h"
#include "driver/i2c.h"
#include "driver/gpio.h"

#include "ism330dlc_reg.h"
#include <assert.h>
#include "bt.h"

typedef union{
  int16_t i16bit[3];
  uint8_t u8bit[6];
} axis3bit16_t;

typedef union{
  int16_t i16bit;
  uint8_t u8bit[2];
} axis1bit16_t;

/* Private Defines -------------------------------------------------------------*/
#define SENSOR1

#ifdef SENSOR1
#define CS_PIN 		PIN_NUM_SENSOR1_CS
#define CS_PIN_MASK PIN_NUM_SENSOR1_CS_MASK
#endif 
#ifdef SENSOR2
#define CS_PIN 		PIN_NUM_SENSOR2_CS
#define CS_PIN_MASK PIN_NUM_SENSOR2_CS_MASK
#endif
#ifdef SENSOR3
#define CS_PIN 		PIN_NUM_SENSOR3_CS
#define CS_PIN_MASK PIN_NUM_SENSOR3_CS_MASK
#endif
#ifdef SENSOR4
#define CS_PIN 		PIN_NUM_SENSOR4_CS
#define CS_PIN_MASK PIN_NUM_SENSOR4_CS_MASK
#endif

#define BUFFER_SIZE 1
#define FIFO_THRESHOLD 1000

/* Private variables ---------------------------------------------------------*/
static uint32_t ism330dlc_sensor_num = 0;
static axis3bit16_t data_raw_acceleration;
static axis3bit16_t data_raw_angular_rate;
static axis1bit16_t data_raw_temperature;
static float acceleration_mg[3];
static float angular_rate_mdps[3];
static float temperature_degC;

uint16_t buffer_xl[3][BUFFER_SIZE];
uint16_t buffer_gy[3][BUFFER_SIZE];
const EventBits_t start_condition = 1;//(ISM330DLC_DEV_FOUND | ISM330DLC_INITIALISED);		// All ISM3330DLC require this bit to set before starting
/* Extern variables ----------------------------------------------------------*/
extern SemaphoreHandle_t spi_bus_mutex;
/* Private functions ---------------------------------------------------------*/

stmdev_ctx_t dev_ctx;

/*
 *   Replace the functions "platform_write" and "platform_read" with your
 *   platform specific read and write function.
 *   This example use an STM32 evaluation board and CubeMX tool.
 *   In this case the "*handle" variable is useful in order to select the
 *   correct interface but the usage of "*handle" is not mandatory.
 */
 //Write to register address, reg, len number of btyes held in *Bufp
static int32_t platform_write(void *handle, uint8_t Reg, uint8_t *Bufp,
			      uint16_t len)
{
  esp_err_t ret = ESP_OK;
  static spi_transaction_t t;
  memset(&t, 0, sizeof(t));					//Zero out the transaction
  t.length=len*8;	//Len is in bytes, transaction length is in bits.
  t.addr = Reg;
  t.tx_buffer = Bufp;               		//TXData
  t.rx_buffer = NULL;
  //ret= spi_device_polling_transmit(ism330dlc_handle, &t);  //Transmit!
  ret= spi_device_transmit(ism330dlc_handle, &t);  //Transmit!
  assert(ret==ESP_OK);            //Should have had no issues.
  return 0;
}
// Read len number of btyes into *Bufp from register address, Reg
static int32_t platform_read(void *handle, uint8_t Reg, uint8_t *Bufp,
			     uint16_t len)
{
  Reg |= 0x80; // Required by ISM chip to set read/write
  esp_err_t ret = ESP_OK;
  static spi_transaction_t t;
  memset(&t, 0, sizeof(t));					//Zero out the transaction
  t.length=len*8;	//Len is in bytes, transaction length is in bits.
  t.addr = Reg;
  t.tx_buffer = NULL;
  t.rx_buffer = Bufp;               		//TXData
  //ret= spi_device_polling_transmit(ism330dlc_handle, &t);  //Transmit!
  ret= spi_device_transmit(ism330dlc_handle, &t);  //Transmit!
  assert(ret==ESP_OK);            //Should have had no issues.

  return 0;
}
/* Main Task --------------------------------------------------------------*/
uint32_t init_ism330dlc(void)
{
  //Configure ISM330DLC spi
  uint8_t ISM330DLC_DEV_FOUND = 1;
  init_buffers();
  spi_device_interface_config_t ism330dlc_dev_config = { .clock_speed_hz = 10
							 * 1000 * 1000, .mode = 3,  .queue_size = 20,.flags = SPI_TRANS_VARIABLE_ADDR, .address_bits=8};

  //Attach the Load Cell to the SPI bus


  /*
   *  Initialize mems driver interface
   */
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = NULL;
  uint8_t whoamI, rst;

//  // Find which CS pin is connected to device out of list of CS pins
//  for(ism330dlc_sensor_num = 0; ism330dlc_sensor_num < MAX_NUM_SPI_DEVICES; ism330dlc_sensor_num++){
//    //
//    //  Check device ID
//    //
//    ESP_LOGI(__func__,"Trying Sensor %d CS: %d", ism330dlc_sensor_num+1, sensor_cs_array[ism330dlc_sensor_num]);

    ism330dlc_dev_config.spics_io_num = 26;
    spi_bus_add_device(VSPI_HOST, &ism330dlc_dev_config, &ism330dlc_handle);
//    ESP_ERROR_CHECK(spi_bus_add_device(VSPI_HOST, &ism330dlc_dev_config, &ism330dlc_handle));
//
//    whoamI = 0;
      ism330dlc_device_id_get(&dev_ctx, &whoamI);
      ESP_LOGI(__func__, " who am I = %0x", whoamI);
//
//    if ( whoamI != ISM330DLC_ID ){
//      ESP_LOGE(__func__,"ISM330DLC device not found %d",whoamI);
//      if(ism330dlc_sensor_num == MAX_NUM_SPI_DEVICES - 1){
//	// Device not found
//	return -1;
//      }
//      spi_bus_remove_device(ism330dlc_handle);
//    }else{
//      // ism330dlc_sensor_num now hold the correct position of sensor
//      ESP_LOGI(__func__,"ISM330DLC found in sensor %d position. Device found flag set",ism330dlc_sensor_num+1);
//     // xEventGroupSetBits(deviceStatusGroup, ISM330DLC_DEV_FOUND);
//      ISM330DLC_DEV_FOUND = 1;
//      break;
//    }
// }
  if(ISM330DLC_DEV_FOUND){
    /*
     *  Restore default configuration
     */
    ESP_LOGI(__func__,"Resetting ISM330DLC");
    ism330dlc_reset_set(&dev_ctx, PROPERTY_DISABLE);
    vTaskDelay(10/portTICK_PERIOD_MS);

    /*
      do {
      ism330dlc_reset_get(&dev_ctx, &rst);
      ESP_LOGI(__func__,"Resetting ISM330DLC");
      } while (rst);
    */

    /*
     *  Enable Block Data Update
     */
    ism330dlc_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);
    /*
     * Set Output Data Rate
     */
    ism330dlc_xl_data_rate_set(&dev_ctx, ISM330DLC_XL_ODR_416Hz);
    ism330dlc_gy_data_rate_set(&dev_ctx, ISM330DLC_GY_ODR_416Hz);
    /*
     * Set full scale
     */
    ism330dlc_xl_full_scale_set(&dev_ctx, ISM330DLC_16g);
    ism330dlc_gy_full_scale_set(&dev_ctx, ISM330DLC_125dps);
    
    /*
     * Offset to calibrate yaw, pitch, roll value to zero when device is level
     * Applicable only to ISM330DLC chip mounted on module.
     */
    ism330dlc_xl_offset_weight_set(&dev_ctx, ISM330DLC_LSb_1mg);
    int8_t offset[3] = {6, 26, 19};
    //ism330dlc_xl_usr_offset_set(&dev_ctx, (uint8_t *) offset);

    /*
     * Configure filtering chain(No aux interface)
     */
    /* Accelerometer - analog filter */
    //ism330dlc_xl_filter_analog_set(&dev_ctx, ISM330DLC_XL_ANA_BW_400Hz);

    /* Accelerometer - LPF1 path ( LPF2 not used )*/
    //ism330dlc_xl_lp1_bandwidth_set(&dev_ctx, ISM330DLC_XL_LP1_ODR_DIV_4);

    /* Accelerometer - LPF1 + LPF2 path */
    //ism330dlc_xl_lp2_bandwidth_set(&dev_ctx, ISM330DLC_XL_LOW_NOISE_LP_ODR_DIV_100);

    /* Accelerometer - High Pass / Slope path */
    //ism330dlc_xl_reference_mode_set(&dev_ctx, PROPERTY_DISABLE);
    //ism330dlc_xl_hp_bandwidth_set(&dev_ctx, ISM330DLC_XL_HP_ODR_DIV_400);

    /* Gyroscope - filtering chain */
    //ism330dlc_gy_band_pass_set(&dev_ctx, ISM330DLC_HP_260mHz_LP1_STRONG);

    /* FIFO settings */
    /*
      ism330dlc_fifo_mode_set(&dev_ctx,ISM330DLC_BYPASS_MODE);
      ism330dlc_auto_increment_set(&dev_ctx,PROPERTY_ENABLE);
      ism330dlc_fifo_xl_batch_set(&dev_ctx,ISM330DLC_FIFO_XL_NO_DEC);
      ism330dlc_fifo_gy_batch_set(&dev_ctx,ISM330DLC_FIFO_GY_NO_DEC);
      ism330dlc_fifo_dataset_3_batch_set(&dev_ctx,ISM330DLC_FIFO_DS3_DISABLE);
      ism330dlc_fifo_dataset_4_batch_set(&dev_ctx,ISM330DLC_FIFO_DS4_DISABLE);
      ism330dlc_fifo_data_rate_set(&dev_ctx,ISM330DLC_FIFO_416Hz);
      //ism330dlc_fifo_xl_gy_8bit_format_set(&dev_ctx,PROPERTY_DISABLE);
      //ism330dlc_fifo_temp_batch_set(&dev_ctx,PROPERTY_DISABLE);
      //ism330dlc_timestamp_set(&dev_ctx,PROPERTY_DISABLE);
      ism330dlc_fifo_watermark_set(&dev_ctx,FIFO_THRESHOLD);

      //ism330dlc_fifo_write_trigger_set(&dev_ctx,ISM330DLC_TRG_XL_GY_DRDY);
      ism330dlc_fifo_mode_set(&dev_ctx,ISM330DLC_STREAM_MODE);
    */
    // Bypass FIFO buffer
    ism330dlc_fifo_mode_set(&dev_ctx,ISM330DLC_BYPASS_MODE);

    ism330dlc_xl_power_mode_set(&dev_ctx,ISM330DLC_XL_HIGH_PERFORMANCE);
    ism330dlc_gy_power_mode_set(&dev_ctx,ISM330DLC_GY_HIGH_PERFORMANCE);

    /* Set Interrupt Mode */
    ism330dlc_pin_mode_set(&dev_ctx,ISM330DLC_PUSH_PULL);
    ism330dlc_pin_polarity_set(&dev_ctx,ISM330DLC_ACTIVE_LOW);
    ism330dlc_int_notification_set(&dev_ctx, ISM330DLC_INT_PULSED);
    ism330dlc_data_ready_mode_set(&dev_ctx,ISM330DLC_DRDY_PULSED);
    
    /* Interrupt on data ready */
    ism330dlc_int1_route_t int1_cfg;
//    ism330dlc_pin_int1_route_get(&dev_ctx,&int1_cfg);
//    int1_cfg.int1_drdy_xl=PROPERTY_ENABLE;
//    int1_cfg.int1_drdy_g=PROPERTY_ENABLE;
//    int1_cfg.int1_inact_state=PROPERTY_ENABLE;
//    int1_cfg.int1_wu=PROPERTY_DISABLE;
//    ism330dlc_pin_int1_route_set(&dev_ctx,int1_cfg);

    	//comment out
    //uint8_t INT = 0x37;
   // ism330dlc_write_reg(&dev_ctx, 0x0D, &INT, 1);
   // ism330dlc_all_on_int1_set(&dev_ctx, 1);
  
    /* Interrupt on FIFO threshold */
 //   ism330dlc_int1_route_t int1_cfg;
  //    int1_cfg.int1_fth = PROPERTY_DISABLE;
   //   ism330dlc_pin_int1_route_set(&dev_ctx,int1_cfg);
      uint8_t nointr = 0x00;
      	ism330dlc_write_reg(&dev_ctx, 0x0D, &nointr, 1);


   // xEventGroupSetBits(deviceStatusGroup, ISM330DLC_INITIALISED); // This will allow associated tasks to start
    ESP_LOGI(__func__, "ISM330DLC initialised");
  }
  return 0;
}

uint32_t unint_is330dlc_activity_detect_mode(void){
  ism330dlc_act_mode_set(&dev_ctx, ISM330DLC_PROPERTY_DISABLE);
  
  ism330dlc_gy_data_rate_set(&dev_ctx, ISM330DLC_GY_ODR_416Hz);
  
  // Restore xlgy interrupt only
  ism330dlc_int1_route_t int1_cfg;
  ism330dlc_pin_int1_route_get(&dev_ctx,&int1_cfg);
  int1_cfg.int1_drdy_xl=PROPERTY_ENABLE;
  int1_cfg.int1_drdy_g=PROPERTY_ENABLE;
  int1_cfg.int1_inact_state=PROPERTY_DISABLE;
  int1_cfg.int1_wu=PROPERTY_DISABLE;
  ism330dlc_pin_int1_route_set(&dev_ctx,int1_cfg);
  ism330dlc_data_ready_mode_set(&dev_ctx,ISM330DLC_DRDY_PULSED);

    ESP_LOGI(__func__, "Disabled acitvity detect mode, returning to full speed");
  return 0;
}

uint32_t init_ism330dlc_activity_detect_mode(void){
// Reference AN 5125 Section 5.3

  //if((xEventGroupGetBits(deviceStatusGroup) & ISM330DLC_DEV_FOUND) == ISM330DLC_DEV_FOUND)

  // Set activity interrupt only
  ism330dlc_int1_route_t int1_cfg;
  ism330dlc_pin_int1_route_get(&dev_ctx,&int1_cfg);
  int1_cfg.int1_drdy_xl=PROPERTY_DISABLE;
  int1_cfg.int1_drdy_g=PROPERTY_DISABLE;
  int1_cfg.int1_inact_state=PROPERTY_ENABLE;
  int1_cfg.int1_wu=PROPERTY_DISABLE;
  ism330dlc_pin_int1_route_set(&dev_ctx,int1_cfg);


    /* Set activity thresholds */
    // 1LSB = FS/64
    // @16G FS 1G = 4
    ism330dlc_wkup_threshold_set(&dev_ctx, 4);
    // 1LSb = 1 / ODR[set]
    // @12.5Hz 1s = 12.5
    ism330dlc_wkup_dur_set(&dev_ctx, 4);
    ESP_LOGI(__func__, "Activty threshold >1G, inactivity threshold <1G for >5s");


    // Start activity detect mode
    ism330dlc_gy_data_rate_set(&dev_ctx, ISM330DLC_GY_ODR_12Hz5);
    ism330dlc_act_mode_set(&dev_ctx, ISM330DLC_XL_12Hz5_GY_NOT_AFFECTED);
      
    ESP_LOGI(__func__, "ISM330DLC set to detect activity mode");

  return 0;
}

uint32_t init_ism330dlc_ulp_mode(void){
  //  if((xEventGroupGetBits(deviceStatusGroup) & ISM330DLC_DEV_FOUND) == ISM330DLC_DEV_FOUND){

      /* Interrupt on wakeup only */
      ism330dlc_int1_route_t int1_cfg;
      ism330dlc_pin_int1_route_get(&dev_ctx,&int1_cfg);
      int1_cfg.int1_drdy_xl=PROPERTY_DISABLE;
      int1_cfg.int1_drdy_g=PROPERTY_DISABLE;
      int1_cfg.int1_inact_state=PROPERTY_DISABLE;
      int1_cfg.int1_wu=PROPERTY_ENABLE;
      ism330dlc_pin_int1_route_set(&dev_ctx,int1_cfg);
      
      ESP_LOGI(__func__, "Increasing wakeup thresholds");
      /* Set activity thresholds */
      // 1LSB = FS/64
      // @16G FS 1G = 4
      ism330dlc_wkup_threshold_set(&dev_ctx, 8);
      // 1LSb = 1 / ODR[set]
      // @12.5Hz 1s = 12.5
      ism330dlc_wkup_dur_set(&dev_ctx, 6);
      ESP_LOGI(__func__, "Wakeup threshold >2G for >1s");

      ism330dlc_xl_hp_path_internal_set(&dev_ctx, ISM330DLC_USE_HPF);
      ism330dlc_xl_hp_bandwidth_set(&dev_ctx, ISM330DLC_XL_HP_ODR_DIV_9); // HP Fc = 12.5/9 = ~1.4Hz

      //ism330dlc_xl_hp_path_internal_set(&dev_ctx, ISM330DLC_USE_SLOPE);
      //ism330dlc_xl_ui_slope_set(&dev_ctx, uint8_t val);

      ism330dlc_xl_data_rate_set(&dev_ctx, ISM330DLC_XL_ODR_12Hz5);
      ism330dlc_gy_data_rate_set(&dev_ctx, ISM330DLC_GY_ODR_OFF);    

      // Start wakeup mode
      ESP_LOGI(__func__, "ISM330DLC set to wakeup mode");    
    //}
    return 0;
}
    


/**********************************************/
/* Read data from sensor upon ready interrupt */
/**********************************************/
//void ism330dlc_read_task(void * arg){
//  EventBits_t uxBits;
//  ism330dlc_reg_t reg;
//  measurement_handler_queue_package_t measurement_package = {0};
//  measurement_package.VIN_number = malloc(sizeof(char)*(MAX_VIN_LENGTH+1));
//  uint32_t  buffer_counter = 0;
//  algo_error_t err;
//  uxBits = xEventGroupWaitBits(deviceStatusGroup, start_condition, pdFALSE, pdTRUE, portMAX_DELAY);
//  while(1){
//    //ESP_LOGI(__func__,"Task active");
//    if(xSemaphoreTake(io32_int_semaphore, portMAX_DELAY) == pdTRUE){
//      //ESP_LOGI(__func__,"Take semaphore");
//      // Read output only if new value is available
//      ism330dlc_status_reg_get(&dev_ctx, &reg.status_reg);
//      if (reg.status_reg.xlda){
//	//ESP_LOGI(__func__,"New xl available");
//	// Read accel data
//	memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
//	ism330dlc_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
//	acceleration_mg[0] = ism330dlc_from_fs16g_to_mg(data_raw_acceleration.i16bit[0]);
//	acceleration_mg[1] = ism330dlc_from_fs16g_to_mg(data_raw_acceleration.i16bit[1]);
//	acceleration_mg[2] = ism330dlc_from_fs16g_to_mg(data_raw_acceleration.i16bit[2]);
//      }else{
//	ESP_LOGI(__func__, "No new xl data");
//	acceleration_mg[0] = 0;
//	acceleration_mg[1] = 0;
//	acceleration_mg[2] = 0;
//      }
//
//      if (reg.status_reg.gda){
//	// Read gyro data
//	memset(data_raw_angular_rate.u8bit, 0x00, 3*sizeof(int16_t));
//	ism330dlc_angular_rate_raw_get(&dev_ctx, data_raw_angular_rate.u8bit);
//	angular_rate_mdps[0] = ism330dlc_from_fs125dps_to_mdps(data_raw_angular_rate.i16bit[0]);
//	angular_rate_mdps[1] = ism330dlc_from_fs125dps_to_mdps(data_raw_angular_rate.i16bit[1]);
//	angular_rate_mdps[2] = ism330dlc_from_fs125dps_to_mdps(data_raw_angular_rate.i16bit[2]);
//      }else{
//	ESP_LOGI(__func__, "No new gy data");
//	angular_rate_mdps[0] = 0;
//	angular_rate_mdps[1] = 0;
//	angular_rate_mdps[2] = 0;
//      }
//
//      uxBits = xEventGroupGetBits(deviceStatusGroup);
//
//      if((uxBits & INERTIAL_RECORD_MODE == INERTIAL_RECORD_MODE) && (uxBits & INERTIAL_BUFFER_FULL != INERTIAL_BUFFER_FULL)){
//	buffer_counter++;
//	ESP_LOGI("ISM330DLC", "Added to buffer. Buffer counter: %d", buffer_counter);
//
//	if(buffer_counter == BUFFER_SIZE){
//	  xEventGroupSetBits(deviceStatusGroup, INERTIAL_BUFFER_FULL); // Stop recording
//
//	  ESP_LOGI("ISM330DLC", "Buffer full: %d. Stopping recording", buffer_counter);
//	}
//      }else{
//	//ESP_LOGI("ISM330DLC", "[mg] %+5.2f %+5.2f %+5.2f \t[mps] %+5.2f %+5.2f %+5.2f",  acceleration_mg[0], acceleration_mg[1], acceleration_mg[2], angular_rate_mdps[0], angular_rate_mdps[1], angular_rate_mdps[2]);
//
//	memset(&(measurement_package.analysis_result), 0, sizeof(analysis_result_t)); // Clear result
//	err = process_inertial_measurement(acceleration_mg, angular_rate_mdps, &(measurement_package.analysis_result)); // Add to processing queue
//	//measurement_package.analysis_result.measurement = 1.1234; // DEBUG Testing data flow okay
//	//ESP_LOGI(__func__, "New result - Measurement:%f\t Linear:%f\t Closed:%d",  measurement_package.analysis_result.measurement, measurement_package.analysis_result.linear_measurement, (int) measurement_package.analysis_result.measurement_is_closed);
//
//	if(err == ERROR_FLAG){
//	  // Error
//	  ESP_LOGE(__func__,"Failed to process measurement");
//	}else{
//	  // Add to results queue
//	  // TODO: define internal commands for measurement handler?
//	  //measurement_package.command = NEW_ANALYSIS_RESULT; // needed?
//	  memcpy(measurement_package.VIN_number, device_state.VIN_number, MAX_VIN_LENGTH+1);
//	  measurement_package.active_door = device_state.active_door;
//
//	  xQueueSendToBack(measurement_handler_queue, (void* ) &measurement_package, pdMS_TO_TICKS(1000)); //portMAX_DELAY
//	}
//      }
//    }else{
//      ESP_LOGI(__func__, "No new xlgy data");
//      acceleration_mg[0] = 0;
//      acceleration_mg[1] = 0;
//      acceleration_mg[2] = 0;
//      angular_rate_mdps[0] = 0;
//      angular_rate_mdps[1] = 0;
//      angular_rate_mdps[2] = 0;
//    }
//  }
//}
//
//// Old prototyping task to ensure buffer functionality. Function now merged into ism330dls_read_task
//// Delete when no longer necessary for reference
//void ism330dlc_read_into_buffer_task(void * arg){
//  static uint16_t count = 0, i;
//  EventBits_t uxBits;
//  while(1)
//    {
//      uxBits = xEventGroupWaitBits(deviceStatusGroup, start_condition, pdFALSE, pdTRUE, portMAX_DELAY);
//      if((uxBits & start_condition) == start_condition){
//	if(xSemaphoreTake(io32_int_semaphore, portMAX_DELAY) == pdTRUE){
//	  /*
//	   * Read output only if new value is available
//	   */
//	  ism330dlc_reg_t reg;
//	  ism330dlc_status_reg_get(&dev_ctx, &reg.status_reg);
//	  if (reg.status_reg.xlda)
//	    {
//	      /* Read accel data */
//	      memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
//	      ism330dlc_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
//	      buffer_xl[0][count] = data_raw_acceleration.i16bit[0];
//	      buffer_xl[1][count] = data_raw_acceleration.i16bit[1];
//	      buffer_xl[2][count] = data_raw_acceleration.i16bit[2];
//	      //printf("%d\r\n",buffer_xl[0][i]);
//	    }
//	  if (reg.status_reg.gda)
//	    {
//	      /* Read gyro data */
//	      memset(data_raw_angular_rate.u8bit, 0x00, 3*sizeof(int16_t));
//	      ism330dlc_angular_rate_raw_get(&dev_ctx, data_raw_angular_rate.u8bit);
//	      buffer_gy[0][count]  = data_raw_angular_rate.i16bit[0];
//	      buffer_gy[1][count] = data_raw_angular_rate.i16bit[1];
//	      buffer_gy[2][count] = data_raw_angular_rate.i16bit[2];
//	      //printf("%d\r\n",buffer_gy[0][i]);
//	    }
//	  count++;
//	  if(count == BUFFER_SIZE){
//	    for(i=0; i<count; i++){
//	      printf("%d\t%d\t%d\t%d\t%d\t%d\r\n", buffer_xl[0][i], buffer_xl[1][i],buffer_xl[2][i], buffer_gy[0][i], buffer_gy[1][i],buffer_gy[2][i]);
//	    }
//	    count = 0;
//	  }
//
//	}
//      }
//    }
//}
//
//void ism330dlc_read_fifo_task(void * arg){
//  EventBits_t uxBits;
//  while(1)
//    {
//      uxBits = xEventGroupWaitBits(deviceStatusGroup, start_condition, pdFALSE, pdTRUE, portMAX_DELAY);
//      if((uxBits & start_condition) == start_condition){
//	if(xSemaphoreTake(io32_int_semaphore, portMAX_DELAY) == pdTRUE){
//	  /*
//	   * Read output only if new value is available
//	   */
//	  float tmp;
//	  uint16_t fifo_level, fifo_pattern=0, i;
//	  uint8_t buffer[FIFO_THRESHOLD*2]={0};
//	  uint8_t reg;
//	  // ism330dlc_fifo_pattern_get(&dev_ctx,&fifo_pattern);
//	  // printf("pattern %d\t", fifo_pattern);
//	  ism330dlc_fifo_data_level_get(&dev_ctx,&fifo_level);
//	  if(fifo_level>0){
//	    ism330dlc_fifo_low_data_get(&dev_ctx, buffer, fifo_level);
//	    // View Data
//	    // for(i=0; i<fifo_level; i+=2){
//	    // if((i/2)%6<3){
//	    // tmp =ism330dlc_from_fs125dps_to_mdps((buffer[i+1]<<8) +(buffer[i]));
//
//	    // printf("sample %d gy %4.2f \r\n",i/2, tmp);
//	    // } else {
//	    // tmp = ism330dlc_from_fs2g_to_mg((buffer[i+1]<<8) +(buffer[i]));
//	    // printf("sample %d xl %4.2f \r\n",i/2, tmp);
//	    // }
//
//	    // }
//	    ESP_LOGI(__func__,"Retrieved %d samples from FIFO", fifo_level);
//	  }
//	}
//      }
//    }
//}
void ism_test_read(void *args)
{
	  dev_ctx.write_reg = platform_write;
	  dev_ctx.read_reg = platform_read;
	   dev_ctx.handle = NULL;
	   uint8_t outTempL = 0;
	   uint8_t outTempH = 0;
	   while(1)
	   {
	   ism330dlc_read_reg(&dev_ctx, 0x0F, &outTempL, 1);
	   ism330dlc_read_reg(&dev_ctx, 0x0F, &outTempH, 1);

	   ESP_LOGI(__func__," Z-value = %d", outTempH<<8 + outTempL);
	   vTaskDelay(pdMS_TO_TICKS(1000));
	   }
}

void inertialSensor_read(void *args)
{
	  ism330dlc_reg_t reg;
	  float a = 90.0;
	  measurement_handler_queue_package_t measurement_package;
	  measurement_package.VIN_number = malloc(sizeof(char)*(MAX_VIN_LENGTH+1));
	  device_state_t device_state;
	  char *tosendtobt = calloc(18, sizeof(uint8_t));
	  while(1){
	    //ESP_LOGI(__func__,"Task active");
	    if(xSemaphoreTake(inertialSensor_drdy_semaphore, portMAX_DELAY) == pdTRUE)
	    {
	      ESP_LOGI(__func__,"Take semaphore");
	      // Read output only if new value is available
	      ism330dlc_status_reg_get(&dev_ctx, &reg.status_reg);
	      if (reg.status_reg.xlda)
	      {
		ESP_LOGI(__func__,"New xl available");
		// Read accel data
		memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
		ism330dlc_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
		acceleration_mg[0] = ism330dlc_from_fs16g_to_mg(data_raw_acceleration.i16bit[0]);
		acceleration_mg[1] = ism330dlc_from_fs16g_to_mg(data_raw_acceleration.i16bit[1]);
		acceleration_mg[2] = ism330dlc_from_fs16g_to_mg(data_raw_acceleration.i16bit[2]);
//		ESP_LOGI(__func__," X-accerelation = %lf \n", acceleration_mg[0]);
//		ESP_LOGI(__func__," Y-accerelation = %lf \n", acceleration_mg[1]);
//		ESP_LOGI(__func__," Z-accerelation = %lf \n", acceleration_mg[2]);
	      }else
	      {
		ESP_LOGI(__func__, "No new xl data");
		acceleration_mg[0] = 0;
		acceleration_mg[1] = 0;
		acceleration_mg[2] = 0;
	      }

	      if (reg.status_reg.gda){
		// Read gyro data
		memset(data_raw_angular_rate.u8bit, 0x00, 3*sizeof(int16_t));
		ism330dlc_angular_rate_raw_get(&dev_ctx, data_raw_angular_rate.u8bit);
		angular_rate_mdps[0] = ism330dlc_from_fs125dps_to_mdps(data_raw_angular_rate.i16bit[0]);
		angular_rate_mdps[1] = ism330dlc_from_fs125dps_to_mdps(data_raw_angular_rate.i16bit[1]);
		angular_rate_mdps[2] = ism330dlc_from_fs125dps_to_mdps(data_raw_angular_rate.i16bit[2]);
	//	ESP_LOGI(__func__," angular-accerelation = %lf \n", angular_rate_mdps[1]);
	      }else
	      {
		ESP_LOGI(__func__, "No new gy data");
		angular_rate_mdps[0] = 0;
		angular_rate_mdps[1] = 0;
		angular_rate_mdps[2] = 0;
	      }



//	    esp_err_t err = ESP_OK;
	   esp_err_t err = process_inertial_measurement(acceleration_mg, angular_rate_mdps, &(measurement_package.analysis_result)); // Add to processing queue
	   if(err != OK_FLAG)
		   ESP_LOGW(__func__, "error adding measurment to the queue");
//	    	//measurement_package.analysis_result.measurement = 1.1234; // DEBUG Testing data flow okay
//    ESP_LOGI(__func__, "New result - Measurement:%f\t Linear:%f\t Closed:%d",  measurement_package.analysis_result.measurement, measurement_package.analysis_result.linear_measurement, (int) measurement_package.analysis_result.measurement_is_closed);
//
//	    	if(err == ERROR_FLAG){
//	    	  // Error
//	    	  ESP_LOGE(__func__,"Failed to process measurement");
//	    	}else{
//	    	  // Add to results queue
//	    	  // TODO: define internal commands for measurement handler?
//	    	  //measurement_package.command = NEW_ANALYSIS_RESULT; // needed?
//	   memcpy(measurement_package.VIN_number, device_state.VIN_number, MAX_VIN_LENGTH+1);
//	   measurement_package.active_door = device_state.active_door;
//	   ESP_LOGI(__func__,"measured value = %f \n", measurement_package.analysis_result.measurement);
	   xQueueSend(new_measurement_queue, (void* ) &acceleration_mg[2], pdMS_TO_TICKS(1000)); //portMAX_DELAY
	   //vTaskDelay(pdMS_TO_TICKS(300));
	   if(measurement_package.analysis_result.door_status.Moving == MOVING)
		   sprintf(tosendtobt, "MOVING%d\n", 0);
	   else
		   sprintf(tosendtobt, "NOT MOVING%d\n", 0);

	   EventBits_t uxBits;
	   uxBits = xEventGroupWaitBits(deviceStatusGroup, BT_ACTIVE, pdFALSE, pdFALSE, 100/portTICK_PERIOD_MS );
//	   if(uxBits&BT_ACTIVE == BT_ACTIVE)
//		   esp_err_t  ret = esp_spp_write(bt_handle,12,(uint8_t *)tosendtobt);

//		   	if (ret == ESP_OK) {
//		   	  ESP_LOGI(TRANSMIT_TASK_TAG, "Sent %3d bytes %s",18,(uint8_t *)tosendtobt);
//		   	  esp_log_buffer_hex(TRANSMIT_TASK_TAG, (uint8_t *)tosendtobt , 5);
//		   	} else {
//		   	  ESP_LOGE(TRANSMIT_TASK_TAG, "Failed to send %3d bytes %s\t %s",
//		   		   sizeof(float),
//		   		   tosendtobt, esp_err_to_name(ret));
//		   	  esp_log_buffer_hex(TRANSMIT_TASK_TAG, (uint8_t *)tosendtobt, 5);
//
//	    	}


	    }
	 }
}

	// Old prototyping task to ensure buffer functionality. Function now merged into ism330dls_read_task
	// Delete when no longer necessary for reference
//	void ism330dlc_read_into_buffer_task(void * arg)d{
//	  static uint16_t count = 0, i;
//	  EventBits_t uxBits;
//	  while(1)
//	    {
//	      uxBits = xEventGroupWaitBits(deviceStatusGroup, start_condition, pdFALSE, pdTRUE, portMAX_DELAY);
//	      if((uxBits & start_condition) == start_condition){
//		if(xSemaphoreTake(io32_int_semaphore, portMAX_DELAY) == pdTRUE){
//		  /*
//		   * Read output only if new value is available
//		   */
//		  ism330dlc_reg_t reg;
//		  ism330dlc_status_reg_get(&dev_ctx, &reg.status_reg);
//		  if (reg.status_reg.xlda)
//		    {
//		      /* Read accel data */
//		      memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
//		      ism330dlc_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
//		      buffer_xl[0][count] = data_raw_acceleration.i16bit[0];
//		      buffer_xl[1][count] = data_raw_acceleration.i16bit[1];
//		      buffer_xl[2][count] = data_raw_acceleration.i16bit[2];
//		      //printf("%d\r\n",buffer_xl[0][i]);
//		    }
//		  if (reg.status_reg.gda)
//		    {
//		      /* Read gyro data */
//		      memset(data_raw_angular_rate.u8bit, 0x00, 3*sizeof(int16_t));
//		      ism330dlc_angular_rate_raw_get(&dev_ctx, data_raw_angular_rate.u8bit);
//		      buffer_gy[0][count]  = data_raw_angular_rate.i16bit[0];
//		      buffer_gy[1][count] = data_raw_angular_rate.i16bit[1];
//		      buffer_gy[2][count] = data_raw_angular_rate.i16bit[2];
//		      //printf("%d\r\n",buffer_gy[0][i]);
//		    }
//		  count++;
//		  if(count == BUFFER_SIZE){
//		    for(i=0; i<count; i++){
//		      printf("%d\t%d\t%d\t%d\t%d\t%d\r\n", buffer_xl[0][i], buffer_xl[1][i],buffer_xl[2][i], buffer_gy[0][i], buffer_gy[1][i],buffer_gy[2][i]);
//		    }
//		    count = 0;
//		  }
//
//		}
//	      }
//	    }
//
//}
