#include <stdio.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdspi_host.h"
#include "driver/spi_common.h"
#include "sdmmc_cmd.h"
#include "sdkconfig.h"
#include "sd_card_reader.h"
#include "bt.h"
#include "main.h"
#include "esp_spp_api.h"
#include "driver/sdmmc_host.h"


static const char *TAG = "example";

#define MOUNT_POINT "/sdcard"

// This example can use SDMMC and SPI peripherals to communicate with SD card.
// By default, SDMMC peripheral is used.
// To enable SPI mode, uncomment the following line:
#define USE_SPI_MODE

// ESP32-S2 doesn't have an SD Host peripheral, always use SPI:
#ifdef CONFIG_IDF_TARGET_ESP32S2
#ifndef USE_SPI_MODE
#define USE_SPI_MODE
#endif // USE_SPI_MODE
// on ESP32-S2, DMA channel must be the same as host id
#define SPI_DMA_CHAN    host.slot
#endif //CONFIG_IDF_TARGET_ESP32S2

// DMA channel to be used by the SPI peripheral
#ifndef SPI_DMA_CHAN
#define SPI_DMA_CHAN    VSPI_HOST
#endif //SPI_DMA_CHAN

// When testing SD and SPI modes, keep in mind that once the card has been
// initialized in SPI mode, it can not be reinitialized in SD mode without
// toggling power to the card.

#ifdef USE_SPI_MODE
// Pin mapping when using SPI mode.
// With this mapping, SD card can be used both in SPI and 1-line SD mode.
// Note that a pull-up on CS line is required in SD mode.
#define PIN_NUM_MISO 23
#define PIN_NUM_MOSI 22
#define PIN_NUM_CLK  21
#define PIN_NUM_CS   2
#endif //USE_SPI_MODE

sdmmc_card_t* card;
const char mount_point[] = MOUNT_POINT;
FILE* f;
sdmmc_host_t host =   SDSPI_HOST_DEFAULT();

void sd_card_init(void)
{
	char ESP_TAG[25] = "Error";
    esp_err_t ret;
    // Options for mounting the filesystem.
    // If format_if_mount_failed is set to true, SD card will be partitioned and
    // formatted in case when mounting fails.
    esp_vfs_fat_sdmmc_mount_config_t mount_config = {
#ifdef CONFIG_EXAMPLE_FORMAT_IF_MOUNT_FAILED
        .format_if_mount_failed = true,
#else
        .format_if_mount_failed = false,
#endif // EXAMPLE_FORMAT_IF_MOUNT_FAILED
        .max_files = 5,
        .allocation_unit_size = 16 * 1024
    };

    ESP_LOGI(TAG, "Initializing SD card");
    strncpy(ESP_TAG, "Initializing SD card", 25);
    esp_spp_write(bt_handle, 25, (uint8_t *)ESP_TAG);
    // Use settings defined above to initialize SD card and mount FAT filesystem.
    // Note: esp_vfs_fat_sdmmc/sdspi_mount is all-in-one convenience functions.
    // Please check its source code and implement error recovery when developing
    // production applications.

    ESP_LOGI(TAG, "Using SPI peripheral");


    spi_bus_config_t bus_cfg = {
        .mosi_io_num = PIN_NUM_MOSI,
        .miso_io_num = PIN_NUM_MISO,
        .sclk_io_num = PIN_NUM_CLK,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 4000,
    };
    host.slot = VSPI_HOST;
//    host.flags = SDMMC_HOST_FLAG_SPI;
    host.max_freq_khz = 400;

    ret = spi_bus_initialize(host.slot, &bus_cfg, SPI_DMA_CHAN);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to initialize bus.");
        strncpy(ESP_TAG, "Failed to initialize", 25);
        esp_spp_write(bt_handle, 25, (uint8_t *)ESP_TAG);
        return;
    }

    // This initializes the slot without card detect (CD) and write protect (WP) signals.
    // Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals.
    sdspi_device_config_t slot_config =  SDSPI_DEVICE_CONFIG_DEFAULT();
    slot_config.gpio_cs = PIN_NUM_CS;
    slot_config.host_id = host.slot;
//      sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
//      slot_config.gpio_miso = (gpio_num_t)PIN_NUM_MISO;
//      slot_config.gpio_mosi = (gpio_num_t)PIN_NUM_MOSI;
//      slot_config.gpio_sck  = (gpio_num_t)PIN_NUM_CLK;
//      slot_config.gpio_cs   = (gpio_num_t)PIN_NUM_CS;
//      slot_config.gpio_cd   = SDMMC_SLOT_NO_CD;
//      slot_config.gpio_wp   = SDMMC_SLOT_NO_WP;
  //    card->host.command_timeout_ms = 2000;

      ret = esp_vfs_fat_sdspi_mount("/sdcard", &host, &slot_config, &mount_config, &card);
    //USE_SPI_MODE

    if (ret != ESP_OK) {
    	 strncpy(ESP_TAG, esp_err_to_name(ret),25);
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount filesystem. "
                "If you want the card to be formatted, set the EXAMPLE_FORMAT_IF_MOUNT_FAILED menuconfig option.");

            esp_spp_write(bt_handle, 25, (uint8_t *)ESP_TAG);
        } else {
            ESP_LOGE(TAG, "Failed to initialize the card (%s). "
                "Make sure SD card lines have pull-up resistors in place.", esp_err_to_name(ret));
                esp_spp_write(bt_handle, 25, (uint8_t *)ESP_TAG);
        }
        return;
    }

    // Card has been initialized, print its properties
    strncpy(ESP_TAG, "Initialization OK", 25);
        esp_spp_write(bt_handle, 25, (uint8_t *)ESP_TAG);
    sdmmc_card_print_info(stdout, card);
    strncpy(ESP_TAG, "Initialization OK", 25);
    esp_spp_write(bt_handle, 25, (uint8_t *)ESP_TAG);
}

void sd_card_write(char *msg)
{
    // Use POSIX and C standard library functions to work with files.
    // First create a file.
	unsigned char ESP_TAG[25] ="at writing \n";
    ESP_LOGI(TAG, "Opening file");
    esp_spp_write(bt_handle, 25, ESP_TAG);
    f = fopen(MOUNT_POINT"/hello.txt", "w");
    if (f == NULL) {
        ESP_LOGE(TAG, "Failed to open file for writing");
        esp_spp_write(bt_handle, 25, ESP_TAG);
        return;
    }
    for(int i = 0; i<500; i++)
    	fprintf(f, "Hello! I am the hero %s!\n", card->cid.name);
    fclose(f);
    ESP_LOGI(TAG, "File written");

    // Check if destination file exists before renaming
    struct stat st;
    if (stat(MOUNT_POINT"/foo.txt", &st) == 0) {
        // Delete it if it exists
        unlink(MOUNT_POINT"/foo.txt");
    }

}

void sd_card_file_rename(char *new_name)
{
	// Rename original file
    ESP_LOGI(TAG, "Renaming file");
    if (rename(MOUNT_POINT"/hello.txt", MOUNT_POINT"/foo.txt") != 0) {
        ESP_LOGE(TAG, "Rename failed");
        return;
    }
}

void sd_card_read(char *msg)
{
    // Open renamed file for reading
    ESP_LOGI(TAG, "Reading file");
    f = fopen(MOUNT_POINT"/Hello.txt", "r");
    if (f == NULL) {
        ESP_LOGE(TAG, "Failed to open file for reading");
        return;
    }
    char line[64];
    fgets(line, sizeof(line), f);
    fclose(f);
    // strip newline
    char* pos = strchr(line, '\n');
    if (pos) {
        *pos = '\0';
    }
    ESP_LOGI(TAG, "Read from file: '%s'", line);
    memcpy(msg, line, 64);
}

void sd_card_unmount()
{
    // All done, unmount partition and disable SDMMC or SPI peripheral
    esp_vfs_fat_sdcard_unmount(mount_point, card);
    ESP_LOGI(TAG, "Card unmounted");
#ifdef USE_SPI_MODE
    //deinitialize the bus after all devices are removed
    spi_bus_free(host.slot);
#endif
}
void sd_card_test(void *args)
{

	vTaskDelay(pdMS_TO_TICKS(20000));
	esp_err_t ret;
	sd_card_init();
	char *msg = calloc(64, sizeof(char));
//	sd_card_write(msg);
//	sd_card_read(msg);
//
//	ret = esp_spp_write(bt_handle, 10, (unsigned char*)msg);
//	char *OK = calloc(4, sizeof(char));
//	OK = "OK\n";
//	if(ret == ESP_OK)
//	{
//		esp_spp_write(bt_handle, 4, (unsigned char*)OK);
//	}
//	else
//	{
//		OK = "Err";
//		esp_spp_write(bt_handle, 4, (unsigned char*)OK);
//
//	}
//
//	free(msg);
//	free(OK);

}

void test_record_batt()
{
	char *msg = calloc(64, sizeof(char));
	unsigned char ESP_TAG[25] ="at writing \n";
	ESP_LOGI(TAG, "Opening file");
	f = fopen(MOUNT_POINT"/hello.txt", "w");
	if (f == NULL) {
	   ESP_LOGE(TAG, "Failed to open file for writing");
	   esp_spp_write(bt_handle, 25, ESP_TAG);
	   return;
	}
	int i  =0 ;
	while(temp_soc1> 25 && temp_soc2>25)
	{
		sprintf(msg , "%d	%d\n", temp_soc1, temp_soc2);
		fprintf(f, msg, card->cid.name);
		vTaskDelay(pdMS_TO_TICKS(120000));

	}
	  fclose(f);
	    ESP_LOGI(TAG, "File written");
	    // Check if destination file exists before renaming
	    struct stat st;
	    if (stat(MOUNT_POINT"/foo.txt", &st) == 0) {
	        // Delete it if it exists
	        unlink(MOUNT_POINT"/foo.txt");
	    }
}
