#ifndef __ALGO_H__
#define __ALGO_H__

#include <stdbool.h>

typedef enum{
	ERROR_FLAG = -1,
	OK_FLAG = 0
} algo_error_t;

typedef enum{
	SLAM = 1,
	WIGGLE = 2
} test_t;

typedef enum{
	UNMOUNTED = 0,
	MOUNTED = 1
} mount_t;

typedef enum{
	NOT_MOVING = 0,
	MOVING = 1
} move_t;

typedef enum{
	BACKWARDS = -1,
	STEADY = 0,
	FORWARDS = 1
} direction_t;

typedef struct{
	mount_t VectorMounting;
	mount_t AccelMounting;
	direction_t Direction;
	move_t Moving;
	bool ReallyClosing;
	bool ReallyOpening;
	test_t TestType;
	bool Opening;
	bool Closing;
} door_status_t;

typedef struct{
	door_status_t door_status;
	float measurement;
	float linear_measurement;
	bool measurement_is_closed;
} analysis_result_t;

float Result_Measurement;
float Result_LinearMeasurement;
bool Result_MeasurementIsClosed;

void series1_test(void);
void init_buffers(void);
void move_buffers_to_next(void);
algo_error_t process_inertial_measurement(float *acceleration_mg, float *angular_rate_mdps, analysis_result_t *analysis_result);
void send_algo_result(float Result_Measurement, bool Result_MeasurementIsClosed, float Result_LinearMeasurement);

#endif
