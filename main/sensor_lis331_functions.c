#include "sensor_lis331_functions.h"
#include "driver/spi_common.h"
#include "driver/spi_master.h"
#include <string.h>
#include "main.h"
#include "freertos/FreeRTOS.h"

#define SENSOR1

#ifdef SENSOR1
	#define CS_PIN 27
	#define CS_PIN_MASK 1ULL<<27
#endif 
#ifdef SENSOR2
	#define CS_PIN PIN_NUM_SENSOR2_CS
	#define CS_PIN_MASK PIN_NUM_SENSOR2_CS_MASK
#endif
#ifdef SENSOR3
	#define CS_PIN PIN_NUM_SENSOR3_CS
	#define CS_PIN_MASK PIN_NUM_SENSOR3_CS_MASK
#endif
#ifdef SENSOR4
	#define CS_PIN PIN_NUM_SENSOR4_CS
	#define CS_PIN_MASK PIN_NUM_SENSOR4_CS_MASK
#endif
/* ================ */
/* NOTE
 * Most of this code is straight from sparkfun library
 * Additional code is used as a shim between library and ESP code
 */
void init_lis331h(void){
//	spi_device_interface_config_t lis331h_dev_config = { .clock_speed_hz = 10
//								 * 1000 * 1000, .mode = 0, .spics_io_num=-1, .queue_size = 20,.flags = SPI_TRANS_VARIABLE_ADDR, .address_bits=8};
//	spi_bus_add_device(VSPI_HOST, &lis331h_dev_config, &lis331h_handle);
	begin(USE_SPI);
	setODR(DR_400HZ);
	intPinMode(PUSH_PULL);
	intActiveHigh(false);
	intSrcConfig(DRDY, 1);
	intSrcConfig(DRDY, 2);
}

uint8_t LISSendReceiveByte(uint8_t dataTx){
	esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));					//Zero out the transaction
	t.flags = SPI_TRANS_USE_RXDATA;
	t.length = 8; //Len is in bytes, transaction length is in bits.
	t.rxlength = 8;
	t.tx_buffer = &dataTx;
	ret=   spi_device_polling_transmit(h3lis100dl_handle, &t);  //Transmit!
	assert(ret==ESP_OK);            //Should have had no issues.
	return t.rx_data[0];
}

static int32_t h1lis330_write(void *handle, uint8_t Reg, uint8_t *Bufp,
			      uint16_t len)
{
  esp_err_t ret = ESP_OK;
  static spi_transaction_t t;
  memset(&t, 0, sizeof(t));					//Zero out the transaction
  t.length=len*8;	//Len is in bytes, transaction length is in bits.
  t.addr = Reg;
  t.tx_buffer = Bufp;               		//TXData
  t.rx_buffer = NULL;
  //ret= spi_device_polling_transmit(ism330dlc_handle, &t);  //Transmit!
  ret= spi_device_polling_transmit(h3lis100dl_handle, &t);  //Transmit!
  assert(ret==ESP_OK);            //Should have had no issues.
  return 0;
}
// Read len number of btyes into *Bufp from register address, Reg
static int32_t h1lis330_read(void *handle, uint8_t Reg, uint8_t *Bufp,
			     uint16_t len)
{
 // Reg |= 0x80; // Required by ISM chip to set read/write

  esp_err_t ret = ESP_OK;
  static spi_transaction_t t;
  memset(&t, 0, sizeof(t));					//Zero out the transaction

  t.length=len*8;	//Len is in bytes, transaction length is in bits.
  t.addr = Reg;
  t.tx_buffer = NULL;
  t.rx_buffer = Bufp;               		//TXData
  //ret= spi_device_polling_transmit(ism330dlc_handle, &t);  //Transmit!
  ESP_LOGW(__func__, "kk %0x ", *(uint8_t *)t.rx_buffer);
  ret= spi_device_polling_transmit(h3lis100dl_handle, &t);
  ESP_LOGW(__func__, "kk %0x ", *(uint8_t *)t.rx_buffer);
  assert(ret==ESP_OK);            //Should have had no issues.

  return 0;
}

void begin(comm_mode bus_type)
{
  mode = bus_type;
  setPowerMode(NORMAL);
  axesEnable(true);
  uint8_t data = 0;
  for (int i = 0x21; i < 0x25; i++) LIS331_write(i,&data,1);
  for (int i = 0x30; i < 0x37; i++) LIS331_write(i,&data,1);
}

void setI2CAddr(uint8_t dev_address)
{
  address = dev_address;
}

// void setSPICS_PIN(uint8_t cs_pin)
// {
  // CS_PIN = cs_pin;
// }

void axesEnable(bool enable)
{
  uint8_t data = 0;
  LIS331_read(CTRL_REG1, &data, 1);
  if (enable)
  {
    data |= 0x07;
  }
  else
  {
    data &= ~0x07;
  }
  LIS331_write(CTRL_REG1, &data, 1);
}

void setPowerMode(power_mode pmode)
{
  uint8_t data = 0;
  LIS331_read(CTRL_REG1, &data, 1);

  // The power mode is the high three bits of CTRL_REG1. The mode 
  //  constants are the appropriate bit values left shifted by five, so we 
  //  need to right shift them to make them work. We also want to mask off the
  //  top three bits to zero, and leave the others untouched, so we *only*
  //  affect the power mode bits.
  data &= ~0xe0; // Clear the top three bits
  data |= pmode<<5; // set the top three bits to our pmode value
  LIS331_write(CTRL_REG1, &data, 1); // write the new value to CTRL_REG1
}

void setODR(data_rate drate)
{
  uint8_t data = 0;
  LIS331_read(CTRL_REG1, &data, 1);
 // ESP_LOGI(__func__, " setODR = %0x", data); //comment out -
  // The data rate is bits 4:3 of CTRL_REG1. The data rate constants are the
  //  appropriate bit values; we need to right shift them by 3 to align them
  //  with the appropriate bits in the register. We also want to mask off the
  //  top three and bottom three bits, as those are unrelated to data rate and
  //  we want to only change the data rate.
  data &=~0x18;     // Clear the two data rate bits
  data |= drate<<3; // Set the two data rate bits appropriately.
  LIS331_write(CTRL_REG1, &data, 1);
  LIS331_read(CTRL_REG1, &data, 1);
  //ESP_LOGI(__func__, " setODR = %0x", data); // write the new value to CTRL_REG1
}

void readAxes(int16_t *x, int16_t *y, int16_t *z)
{
  uint8_t data[6] = {0}; // create a buffer for our incoming data
  LIS331_read(OUT_X, &data[0], 1);
  data[1]=  0x00;
  //LIS331_read(OUT_X_H, &data[1], 1);
  LIS331_read(OUT_Y, &data[2], 1);
  data[3] = 0x00;
  //LIS331_read(OUT_Y_H, &data[3], 1);
  LIS331_read(OUT_Z, &data[4], 1);
  data[5] = 0x00;
  //LIS331_read(OUT_Z_H, &data[5], 1);
  //printf("raw: %d %d %d %d %d %d\r\n", data[0],data[1],data[2],data[3],data[4],data[5]);
  // The data that comes out is 12-bit data, left justified, so the lower
  //  four bits of the data are always zero. We need to right shift by four,
  //  then typecase the upper data to an integer type so it does a signed
  //  right shift.
  *x = data[0] | data[1] << 8;
  *y = data[2] | data[3] << 8;
  *z = data[4] | data[5] << 8;
  *x = *x >> 4;
  *y = *y >> 4;
  *z = *z >> 4;
}

uint8_t readReg(uint8_t reg_address)
{
  uint8_t data = 0;
  LIS331_read(reg_address, &data, 1);
  return data;
}

float convertToG(int maxScale, int reading)
{
  float result = ((float)(maxScale) * (float)(reading))/2047;
  return result;
}

void setHighPassCoeff(high_pass_cutoff_freq_cfg hpcoeff)
{
  // The HPF coeff depends on the output data rate. The cutoff frequency is
  //  is approximately fs/(6*HPc) where HPc is 8, 16, 32 or 64, corresponding
  //  to the various constants available for this parameter.
  uint8_t data = 0;
  LIS331_read(CTRL_REG2, &data, 1);
  data &= ~0xfc;  // Clear the two low bits of the CTRL_REG2
  data |= hpcoeff;
  LIS331_write(CTRL_REG2, &data, 1);
}

void enableHPF(bool enable)
{
  // Enable the high pass filter
  uint8_t data = 0;
  LIS331_read(CTRL_REG2, &data, 1);
  if (enable)
  {
    data |= 1<<5;
  }
  else
  {
    data &= ~(uint8_t)(1<<5);
  }
  LIS331_write(CTRL_REG2, &data, 1);
}

void HPFOnIntPin(bool enable, uint8_t pin)
{
  // Enable the hpf on signal to int pins 
  uint8_t data = 0;
  LIS331_read(CTRL_REG2, &data, 1);
  if (enable)
  {
    if (pin == 1)
    {
      data |= 1<<3;
    }
    if (pin == 2)
    {
      data |= 1<<4;
    }
  }
  else
  {
    if (pin == 1)
    {
      data &= ~1<<3;
    }
    if (pin == 2)
    {
      data &= ~1<<4;
    }
  }
  LIS331_write(CTRL_REG2, &data, 1);
}

void intActiveHigh(bool enable)
{
  // Are the int pins active high or active low?
  uint8_t data = 0;
  LIS331_read(CTRL_REG3, &data, 1);
  // Setting bit 7 makes int pins active low
  if (!enable)
  {
    data |= 1<<7;
  }
  else
  {
    data &= ~(1<<7);
  }
  LIS331_write(CTRL_REG3, &data, 1);
}

void intPinMode(pp_od _pinMode)
{
  uint8_t data = 0;
  LIS331_read(CTRL_REG3, &data, 1);
  // Setting bit 6 makes int pins open drain.
  if (_pinMode == OPEN_DRAIN)
  {
    data |= 1<<6;
  }
  else
  {
    data &= ~(1<<6);
  }
  LIS331_write(CTRL_REG3, &data, 1);
}

void latchInterrupt(bool enable, uint8_t intSource)
{
  // Latch mode for interrupt. When enabled, you must read the INTx_SRC reg
  //  to clear the interrupt and make way for another.
  uint8_t data = 0; 
  LIS331_read(CTRL_REG3, &data, 1);
  // Enable latching by setting the appropriate bit.
  if (enable)
  {
    if (intSource == 1)
    {
      data |= 1<<2;
    }
    if (intSource == 2)
    {
      data |= 1<<5;
    }
  }
  else
  {
    if (intSource == 1)
    {
      data &= ~1<<2;
    }
    if (intSource == 2)
    {
      data &= ~1<<5;
    }
  }
  LIS331_write(CTRL_REG3, &data, 1);
}

void intSrcConfig(int_sig_src src, uint8_t pin)
{

  uint8_t data = 0; 
  LIS331_read(CTRL_REG3, &data, 1);
  // Enable latching by setting the appropriate bit.
  if (pin == 1)
  {
    data &= ~0xfc; // clear the low two bits of the register
    data |= src;
  }
  if (pin == 2)
  {
    data &= ~0xe7; // clear bits 4:3 of the register
    data |= src<<4;
  }
  LIS331_write(CTRL_REG3, &data, 1);
}

void setFullScale(fs_range range)
{
  uint8_t data = 0; 
  LIS331_read(CTRL_REG4, &data, 1);
  data &= ~0xcf;
  data |= range<<4;
  LIS331_write(CTRL_REG4, &data, 1);
}

bool newXData()
{
  uint8_t data = 0;
  LIS331_read(STATUS_REG, &data, 1);
  if (data & 1<<0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool newYData()
{
  uint8_t data = 0;
  LIS331_read(STATUS_REG, &data, 1);
  if (data & 1<<1)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool newZData()
{
  uint8_t data = 0;
  LIS331_read(STATUS_REG, &data, 1);
  if (data & 1<<2)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void enableInterrupt(int_axis axis, trig_on_level trigLevel,
                     uint8_t interrupt, bool enable)
{
  uint8_t data = 0, reg, mask; 
  mask = 0;
  if (interrupt == 1)
  {
    reg = INT1_CFG;
  }
  else
  {
    reg = INT2_CFG;
  }
  LIS331_read(reg, &data, 1);
  if (trigLevel == TRIG_ON_HIGH)
  {
    mask = 1<<1;
  }
  else
  {
    mask = 1;
  }
  if (axis == Z_AXIS) mask = mask<<4;
  if (axis == Y_AXIS) mask = mask<<2;
  if (enable)
  {
    data |= mask;
  }
  else
  {
    data &= ~mask;
  }
  LIS331_write(reg, &data, 1);
}

void setIntDuration(uint8_t duration, uint8_t intSource)
{
  if (intSource == 1)
  {
    LIS331_write(INT1_DURATION, &duration, 1);
  }
  else
  {
    LIS331_write(INT2_DURATION, &duration, 1);
  }
}

void setIntThreshold(uint8_t threshold, uint8_t intSource)
{
  if (intSource == 1)
  {
    LIS331_write(INT1_THS, &threshold, 1);
  }
  else
  {
    LIS331_write(INT2_THS, &threshold, 1);
  }
}

void LIS331_write(uint8_t reg_address, uint8_t *data, uint8_t len)
{
  if (mode == USE_I2C)
  {
    // I2C write handling code
    // Wire.beginTransmission(address);
    // Wire.write(reg_address);
    // for(int i = 0; i<len; i++)
    // {
      // Wire.write(data[i]);
    // }
    // Wire.endTransmission();
  }
  else
  {
    // SPI write handling code
	if(xSemaphoreTake(spi_bus_mutex, portMAX_DELAY)){
		digitalWrite(CS_PIN, LOW);
	//	h1lis330_write(lis331h_handle, reg_address|0x40, data, len);
	//	SPItransfer(reg_address | 0x40);
		for (int i=0; i<len; i++)
		{
		  SPItransfer(data[i]);
		}
		digitalWrite(CS_PIN, HIGH);
		xSemaphoreGive(spi_bus_mutex);
	}
  }
}

void LIS331_read(uint8_t reg_address, uint8_t *data, uint8_t len)
{
  if (mode == USE_I2C)
  {
    // I2C read handling code
    // Wire.beginTransmission(address);
    // Wire.write(reg_address);
    // Wire.endTransmission();
    // Wire.requestFrom(address, len);
    // for (int i = 0; i<len; i++)
    // {
      // data[i] = Wire.read();
    // }
  }
  else
  {
    // SPI read handling code
	if(xSemaphoreTake(spi_bus_mutex, portMAX_DELAY)){
		digitalWrite(CS_PIN, LOW);
		//h1lis330_read(lis331h_handle, reg_address|0xC0, data, len);
		SPItransfer(reg_address | 0x80);
		for (int i=0; i<len; i++)
		{
		  data[i] = SPItransfer(0);
		  //ESP_LOGW(__func__, "%d", SPItransfer(0));
		}
		digitalWrite(CS_PIN, HIGH);
		xSemaphoreGive(spi_bus_mutex);
	}
  }
}
void lis_test_read(void *args)
{
	axesEnable(1);
	uint8_t whoamI= 0;
	while(1)
	{
//	int16_t x,y,z;
//	readAxes(&x, &y, &z);
//	ESP_LOGW(__func__," %d %d %d ", x, y, z);
	LIS331_read(WHO_AM_I,&whoamI, sizeof(uint8_t));
	ESP_LOGI(__func__, " I am %0x \n", whoamI);
	vTaskDelay(pdMS_TO_TICKS(500));
	}

}
