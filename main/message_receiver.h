/*
 * message_receiver.h
 *
 *  Created on: Dec 24, 2020
 *      Author: Ankit koirala
 */

#ifndef MAIN_MESSAGE_RECEIVER_H_
#define MAIN_MESSAGE_RECEIVER_H_

void receive_from_bt(void *args);
void receive_from_wifi(void *args);

#endif /* MAIN_MESSAGE_RECEIVER_H_ */
