/*
 * i2c_perpiherals.c
 *
 *  Created on: Nov 13, 2020
 *      Author: Ankit koirala
 */
#include "i2c_peripherals.h"
#include "driver/i2c.h"
#include "driver/gpio.h"
#include "time.h"
#include "sys/time.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "main.h"
#include "I2Cbus.h"

//
//static int driver_initialized = 1;

int i2cset_cmd(uint8_t chip_addr, uint8_t register_addr, uint8_t data)
{
  // This needs to be rewritten for specific peripherals with different message patterns
  // Currently used for IO exanpder on dev board
//  printf("%d at led  ", driver_initialized);
//  if(driver_initialized)
//  {
//	  printf("LED intialized");
//	  printf("error = %d\n",i2c_master_driver_initialize());
//	  driver_initialized = 0;
//  }
  if(xSemaphoreTake(i2c_semaphore, (TickType_t)100))
  {
	  esp_err_t err;
	  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	  i2c_master_start(cmd);
	  i2c_master_write_byte(cmd, (chip_addr<<1)|WRITE_BIT, ACK_CHECK_EN);
	  i2c_master_write_byte(cmd, register_addr, ACK_CHECK_EN);
	  i2c_master_write_byte(cmd, data, ACK_CHECK_EN);
	  i2c_master_stop(cmd);
	  err = i2c_master_cmd_begin((1), cmd, 100/portTICK_RATE_MS);
	  // if(err!= ESP_OK) printf("LED");
	  i2c_cmd_link_delete(cmd);
	  if(xSemaphoreGive(i2c_semaphore) != pdTRUE)
	  {

	  }
	  return 0;
  }

//   if (ret == ESP_OK) {
//   printf("Write OK");
//   } else if (ret == ESP_ERR_TIMEOUT) {
//   printf("Bus is busy");
//   } else {
//   printf("Write Failed");
//   }
   //printf("I2C set: Chip: 0x%02x Reg 0x%02x Data: 0x%02x\r\n", chip_addr, register_addr, data);

  	  return -1;

}

esp_err_t i2c_rtc_read(uint8_t device_addr, uint8_t register_addr, uint8_t *data)
{
	printf(" at rtc read \n");
	if(xSemaphoreTake(i2c_semaphore, (TickType_t)100))
	 {

		  esp_err_t  err;
	      i2c_cmd_handle_t cmd = i2c_cmd_link_create();
		  i2c_master_start(cmd);
		  i2c_master_write_byte(cmd, (uint8_t)0xD0|I2C_MASTER_WRITE, I2C_MASTER_ACK_EN); // Write Slave address
		  i2c_master_write_byte(cmd, (uint8_t)0x01, I2C_MASTER_ACK_EN); // Register
		  i2c_master_start(cmd);
		  i2c_master_write_byte(cmd, (uint8_t)0xD0|I2C_MASTER_READ, I2C_MASTER_ACK_EN); // Read Slave address
		  i2c_master_read(cmd, data, 1, I2C_MASTER_LAST_NACK);
		  i2c_master_stop(cmd);
		  err = i2c_master_cmd_begin((1), cmd, 100/portTICK_RATE_MS);
		  i2c_cmd_link_delete(cmd);
		//  vTaskDelay(pdMS_TO_TICKS(1000));

	  if(xSemaphoreGive(i2c_semaphore) != pdTRUE)
	  {

	  }
	  return err;
	 }
	 return -1;
}

esp_err_t i2c_rtc_write(uint8_t device_addr, uint8_t register_addr, uint8_t data)
{
	printf(" at rtc write \n");
		if(xSemaphoreTake(i2c_semaphore, (TickType_t)100))
		 {

			  esp_err_t err;
		      i2c_cmd_handle_t cmd = i2c_cmd_link_create();
			  i2c_master_start(cmd);
			  i2c_master_write_byte(cmd, (0xD0)| 0x00, ACK_CHECK_DIS); // Write Slave address
			  i2c_master_write_byte(cmd, 0x01, ACK_CHECK_DIS); // Register
			  i2c_master_write_byte(cmd, data, ACK_CHECK_DIS); // Read Slave address
			  i2c_master_stop(cmd);
			  err= i2c_master_cmd_begin((1), cmd, 100/portTICK_RATE_MS); // This acutally sends above
			  i2c_cmd_link_delete(cmd);
			//  return err;
//			  vTaskDelay(pdMS_TO_TICKS(400));
//			  if(err == ESP_OK) printf(" Complete Writing \n");
//			  vTaskDelay(pdMS_TO_TICKS(400));
		  if(xSemaphoreGive(i2c_semaphore) != pdTRUE)
		  {
			 // printf(" Semaphore Give F \n");

		  }
		  return err;
		 }
		 return -1;
}

int i2cset_fuel_gauge_cmd(uint8_t chip_addr, uint8_t register_addr, uint8_t data)
{


	  return 0;
}

esp_err_t read_battery_value(uint8_t battery_read_reg, uint8_t cell_status_code_1, uint8_t cell_status_code_2, uint8_t *retData)
{
	printf(" bat before %0x%0x\n", retData[0], retData[1]);
	if(xSemaphoreTake(i2c_semaphore ,(TickType_t)100))
	{
		esp_err_t err;
		i2c_cmd_handle_t cmd= i2c_cmd_link_create();
		i2c_master_start(cmd);
		i2c_master_write_byte(cmd, 0x55<<1|0, ACK_CHECK_EN);
	    i2c_master_write_byte(cmd, 0x14, ACK_CHECK_EN);
		i2c_master_start(cmd);
		err = i2c_master_cmd_begin(1, cmd, pdMS_TO_TICKS(100));
	//	vTaskDelay(10);
		i2c_master_start(cmd);
		i2c_master_write_byte(cmd, 0x55<<1|1, ACK_CHECK_EN);
		i2c_master_read(cmd, &retData[0], 1, I2C_MASTER_NACK);
		i2c_master_stop(cmd);
		err =  i2c_master_cmd_begin(1, cmd, pdMS_TO_TICKS(100));
//		vTaskDelay(10);
//		i2c_master_start(cmd);
//		i2c_master_write_byte(cmd, 0x55<<1|1, ACK_CHECK_EN);
//		i2c_master_read(cmd, &retData[1], 1, I2C_MASTER_LAST_NACK);
//		i2c_master_stop(cmd);
//		err = i2c_master_cmd_begin(1, cmd, pdMS_TO_TICKS(100));
//		i2c_cmd_link_delete(cmd);

		if(xSemaphoreGive(i2c_semaphore)!= pdTRUE)
			{
				return err;
			}
			return err;

	}
	return -1;
}

void turn_on_LED(uint8_t LEDs_map)
{
	//i2cset_cmd(IO_EXPANDER_LEDS, IO_REG_S2_ADDR, LEDs_map);
	i2cset_cmd(IO_EXPANDER_LEDS, IO_REG_S2_ADDR, LEDs_map);
}

esp_err_t read_battery(uint8_t fuel_gauge_reg, uint16_t *value)
{
    uint8_t *data = calloc(2, sizeof(uint8_t));
	esp_err_t err = ESP_OK;
	if(xSemaphoreTake(i2c_semaphore ,(TickType_t)100))
	 {
		i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	    i2c_master_start(cmd);
	    i2c_master_write_byte(cmd, I2C_FUEL_GAUGE<<1|I2C_MASTER_WRITE, ACK_CHECK_EN);
	    i2c_master_write_byte(cmd, fuel_gauge_reg, ACK_CHECK_EN);
	    i2c_master_start(cmd);
	    i2c_master_write_byte(cmd, I2C_FUEL_GAUGE<<1|I2C_MASTER_READ, ACK_CHECK_EN);
	    i2c_master_read(cmd, &data[0],1, I2C_MASTER_ACK);
	    i2c_master_read(cmd, &data[1], 1, I2C_MASTER_NACK);
	    i2c_master_stop(cmd);
	    err = i2c_master_cmd_begin((1),cmd, 1000/portTICK_RATE_MS);
	    i2c_cmd_link_delete(cmd);

	if(err == ESP_OK)
	{
		*value = *value + data[1];
		*value = ((*value)<<8) + data[0];
		//printf(" Battery Voltage = %d  \n", voltage);

	}
	else
		ESP_LOGI(__func__, "ESP ERROR = %d", err);

	free(data);
	if(xSemaphoreGive(i2c_semaphore)!= pdTRUE)
	{
					return -1;
	}
		return err;
	 }
	return -1;


}
esp_err_t read_battery_cntl(uint8_t cntl_cmd, uint16_t *value)
{
	uint8_t *data = calloc(2, sizeof(uint8_t));
	esp_err_t err = ESP_OK;
	if(xSemaphoreTake(i2c_semaphore, (TickType_t)100))
	{
		i2c_cmd_handle_t cmd = i2c_cmd_link_create();
		i2c_master_start(cmd);
		i2c_master_write_byte(cmd, I2C_FUEL_GAUGE<<1|I2C_MASTER_WRITE, I2C_MASTER_ACK_EN);
		i2c_master_write_byte(cmd, 0x00, I2C_MASTER_ACK_EN);
		i2c_master_write_byte(cmd, cntl_cmd, I2C_MASTER_ACK_EN);
		i2c_master_write_byte(cmd, 0x00, I2C_MASTER_ACK_EN);
		i2c_master_stop(cmd);
		err = i2c_master_cmd_begin(1, cmd, 100/portTICK_RATE_MS);
		i2c_cmd_link_delete(cmd);

		if(err != ESP_OK)
			printf(" First command failed");
		cmd = i2c_cmd_link_create();
		i2c_master_start(cmd);
		i2c_master_write_byte(cmd, I2C_FUEL_GAUGE<<1|I2C_MASTER_WRITE, I2C_MASTER_ACK_EN);
		i2c_master_write_byte(cmd, 0x00, I2C_MASTER_ACK_EN);
		i2c_master_start(cmd);
		i2c_master_write_byte(cmd, I2C_FUEL_GAUGE<<1|I2C_MASTER_READ, I2C_MASTER_ACK_EN);
		i2c_master_read(cmd, &data[0],1,I2C_MASTER_ACK);
		i2c_master_read(cmd, &data[1],1, I2C_MASTER_NACK);
		i2c_master_stop(cmd);
		err = i2c_master_cmd_begin(1,cmd, 100/portTICK_RATE_MS);
		i2c_cmd_link_delete(cmd);
	}
	if(err == ESP_OK)
	{
		*value = *value +data[1];
		*value = ((*value)<<8)+ data[0];

	}
	else
		ESP_LOGI(__func__, "ESP ERROR = %s", esp_err_to_name(err));
	free(data);
    if(xSemaphoreGive(i2c_semaphore) != pdTRUE)
    {
    	return -1;
    }
    return err;
    return -1;
}


void rtc_read()
{
	unsigned char *data = calloc(1, sizeof(unsigned char));
	esp_err_t err;
	err= i2c_rtc_read((uint8_t)0xD0, (uint8_t)0x01, data);
	if(err == ESP_OK)
	{
		printf("%0x", *data);
	}
	else
		printf("err %s\n", esp_err_to_name(err));

//	 if(I2C_readByte(i2c, 0x68, 0x01, data, 0)== ESP_OK)
//	 {
//		 *data = (*data) & 0x0F;
//		 printf(" time elasped in seconds = %0x\n", (uint8_t)*data);
//	 }

}
void rtc_write(uint8_t time)
{
	if(i2c_rtc_write((uint8_t)0xD0, (uint8_t)0x02, (uint8_t)time)==ESP_OK)
	{

		printf(" OK \n");
	}
}
