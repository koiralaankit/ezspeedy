/*****************************************************************************
* | File      	:   DEV_Config.h
* | Author      :   Waveshare team
* | Function    :   Hardware underlying interface
* | Info        :
*                Used to shield the underlying layers of each master 
*                and enhance portability
*----------------
* |	This version:   V2.0
* | Date        :   2018-10-30
* | Info        :
* 1.add:
*   UBYTE\UWORD\UDOUBLE
* 2.Change:
*   EPD_RST -> EPD_RST_PIN
*   EPD_DC -> EPD_DC_PIN
*   EPD_CS -> EPD_CS_PIN
*   EPD_BUSY -> EPD_BUSY_PIN
* 3.Remote:
*   EPD_RST_1\EPD_RST_0
*   EPD_DC_1\EPD_DC_0
*   EPD_CS_1\EPD_CS_0
*   EPD_BUSY_1\EPD_BUSY_0
* 3.add:
*   #define DEV_Digital_Write(_pin, _value) bcm2835_gpio_write(_pin, _value)
*   #define DEV_Digital_Read(_pin) bcm2835_gpio_lev(_pin)
*   #define DEV_SPI_WriteByte(__value) bcm2835_spi_transfer(__value)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documnetation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to  whom the Software is
# furished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
******************************************************************************/
#ifndef _DEV_CONFIG_H_
#define _DEV_CONFIG_H_

#include <string.h>
#include "main.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "soc/gpio_struct.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
//#include "peripheral_max17048.h"

#define EXTERNAL_SCREEN
//#define EXTERNAL_SCREEN

/**
 * data
**/
#define UBYTE   uint8_t

#define UWORD   uint16_t
#define UDOUBLE uint32_t

/**
 * e-Paper GPIO
**/
#ifdef INTERNAL_SCREEN
#define EDP_MOSI_PIN 	22
#define EDP_CLK_PIN  	21

// EPD control pins vi IOexpander
#define EPD_RST_PIN     5
#define EPD_DC_PIN      18
#define EPD_CS_PIN      4
#define EPD_BUSY_PIN    19
#define EDP_CS_MASK   	1<<4
#define EDP_RST_MASK 	1<<6
#define EDP_DC_MASK 	1<<5
#define EDP_BUSY_MASK 	1<<7
/**
 * GPIO read and write
**/
// EPD control pins via IOExpander
#define DEV_Digital_Write(_pin, _value) i2cset_cmd(IO_EXPANDER_EDP_IO, -1, ((i2cget_cmd(IO_EXPANDER_EDP_IO, -1) & ~(1<<_pin)) | _value<<_pin))
#define DEV_Digital_Read(_pin) (i2cget_cmd(IO_EXPANDER_EDP_IO, -1) & (1<<_pin)) > 0 ? HIGH : LOW
#endif

#ifdef EXTERNAL_SCREEN

#define EDP_MOSI_PIN 	22
#define EDP_CLK_PIN  	21
// EPD control pins directly connected to GPIO
#define EPD_RST_PIN     5
#define EPD_DC_PIN      18
#define EPD_CS_PIN      4
#define EPD_BUSY_PIN    19
#define EDP_CS_MASK   	1ULL<<EDP_CS_PIN
#define EDP_RST_MASK 	1ULL<<EDP_RST_PIN
#define EDP_DC_MASK 	1ULL<<EDP_RESET_PIN
#define EDP_BUSY_MASK 	1ULL<<EDP_BUSY_PIN
// NOTE: THESE ARE GPIO NUMBERS NOT PHYSICAL PIN NUMBERS! e.g. Pin 4 = GPIO36

// EPD control pins when directly connected to GPIO
#define DEV_Digital_Write(_pin, _value)   gpio_set_level(_pin, _value)
#define DEV_Digital_Read(_pin) gpio_get_level(_pin)
#endif
/**
 * delay x ms
**/
#define DEV_Delay_ms(__xms) vTaskDelay(pdMS_TO_TICKS(__xms))

// Gauge Parameters
#define MAX_SPEED	2.000 // m/s
#define MAX_ANGLE	3.665 // rads

spi_device_handle_t edp;
xQueueHandle screen_handler_queue;



extern SemaphoreHandle_t pb1_semaphore, pb2_semaphore;

void DEV_SPI_WriteByte(UBYTE value);
void init_epd_bus(void);
void update_edp_task(void* command);
void init_edp(void);
void init_screen_buffer(void);
#endif
