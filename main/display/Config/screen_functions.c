/*****************************************************************************
 * | File      	:   DEV_Config.c
 * | Author      :   Waveshare team
 * | Function    :   Hardware underlying interface
 * | Info        :
 *                Used to shield the underlying layers of each master 
 *                and enhance portability
 *----------------
 * |	This version:   V2.0
 * | Date        :   2018-10-30
 * | Info        :
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documnetation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to  whom the Software is
# furished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
******************************************************************************/
#include "screen_functions.h"

#include "EPD_1in54.h"
#include "GUI_Paint.h"
#include "image.h"
#include "driver/spi_common.h"
#include "driver/spi_master.h"
#include "freertos/FreeRTOS.h"
#include "screens.h"
#include <math.h>
//#include <peripheral_max17048.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include "../../sensor_algorithm.h"

extern spi_device_handle_t edp;
//extern battery_status_t battery_status;
// Screen buffer handles
UBYTE * test_screen_buffer;
UBYTE * sleep_screen_buffer;

void init_screen_buffer(void);

void DEV_SPI_WriteByte(UBYTE value){
  spi_transaction_t t;
  memset(&t, 0, sizeof(t));       //Zero out the transaction
  t.length=8;                     //Command is 8 bits
  t.tx_data[0]=value;             //The data is the cmd itself
  t.flags=SPI_TRANS_USE_TXDATA;
  //spi_device_acquire_bus(edp, portMAX_DELAY); // Guard SPI bus from other devices
  spi_device_polling_transmit(edp, &t);
  //ESP_LOGI("EPD", "SPI transmit");
  //spi_device_release_bus(edp);
}

IRAM_ATTR void set_epd_cs(void){
  //ESP_LOGI(__func__,"Set EPD CS");
  DEV_Digital_Write(EPD_CS_PIN, 1);
}
IRAM_ATTR void unset_epd_cs(void){
  //ESP_LOGI(__func__,"Unset EPD CS");
  DEV_Digital_Write(EPD_CS_PIN, 0);
}

void init_epd_bus(void){
  esp_err_t ret;
#ifdef INTERNAL_SCREEN
  
  spi_bus_config_t buscfg = { .miso_io_num = -1, .mosi_io_num = EDP_MOSI_PIN,
                              .sclk_io_num = EDP_CLK_PIN, .quadwp_io_num = -1,
			      .quadhd_io_num = -1, .max_transfer_sz = 100 };
  ESP_LOGI(__func__,"Initing HSPI Bus");
  ret = spi_bus_initialize(HSPI_HOST, &buscfg, 0);
  ESP_ERROR_CHECK(ret);
#endif

  spi_device_interface_config_t edp_devcfg={
					    .clock_speed_hz=4*1000*1000,    //Clock out
					    .mode=0,                        //SPI mode 0
					    .spics_io_num=-1,               //CS pin controlled manually
					    .queue_size=100,
					    /*.pre_cb=set_epd_cs,
					      .post_cb=unset_epd_cs,*/
  };

  //Attach EDP to the SPI bus
#ifdef INTERNAL_SCREEN
  ret=spi_bus_add_device(HSPI_HOST, &edp_devcfg, &edp);
  ESP_LOGI(__func__,"Attaching EPD to HSPI bus");
#endif
#ifdef EXTERNAL_SCREEN
//  spi_bus_config_t buscfg = { .miso_io_num = -1, .mosi_io_num = EDP_MOSI_PIN,
//                                .sclk_io_num = EDP_CLK_PIN, .quadwp_io_num = -1,
//  			      .quadhd_io_num = -1, .max_transfer_sz = 100 };
//    ESP_LOGI(__func__,"Initiating VSPI Bus");
//    ret = spi_bus_initialize(VSPI_HOST, &buscfg, 0);
//    ESP_ERROR_CHECK(ret);
  ret=spi_bus_add_device(VSPI_HOST, &edp_devcfg, &edp);
  ESP_LOGI(__func__,"Attaching EPD to VSPI bus");
#endif
  ESP_ERROR_CHECK(ret);
}
void add_circular_border(uint16_t radius)
{
	UWORD Width, Height;
	uint8_t shift_bits = 0x80, shift_by;
	Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
	Height = EPD_HEIGHT;
	UDOUBLE Addr = 0;
	radius = pow(radius, 2); //squarring the radius

	for (UWORD j = 0; j < Height; j++)
		for (UWORD i = 0; i <EPD_WIDTH ; i++)
		{
			    if(pow(i-EPD_WIDTH/2,2)+pow(j-EPD_HEIGHT/2, 2)<=radius)
			    {
			    	  //Paint_DrawPoint(i, j, WHITE,  DOT_PIXEL_1X1, DOT_FILL_AROUND);

			    }

			    else
			    	{
			    	Paint_DrawPoint(i,j, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND);

			    	}
		}
}
void draw_rectangle(uint8_t rect_height, uint8_t rect_width, uint8_t pos_x, uint8_t pos_y, uint8_t color)
{
	UWORD Width, Height, figWidth;
    Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
    Height = EPD_HEIGHT;
    figWidth =(rect_width%8 == 0)?(rect_width/8):(rect_width/8+1);
    uint8_t fig_height = rect_height;
	uint8_t POSX = (uint8_t) pos_x/8;
	uint8_t POSY = pos_y;
	UDOUBLE Addr = 0, Addr_fig = 0;
		  // UDOUBLE Offset = ImageName;
	for (UWORD j = 0; j < fig_height; j++) {
		for (UWORD i = 0; i < figWidth; i++) {
		      Addr = (i+POSX) + (j+POSY)* Width;
		      Paint.Image[Addr] = color;
		    }
		  }
}
void add_figure(unsigned char *fig, uint8_t fig_width, uint8_t fig_height, uint8_t pos_x, uint8_t pos_y)
{
	  UWORD Width, Height, figWidth;
	  Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
	  Height = EPD_HEIGHT;
	  figWidth =(fig_width%8 == 0)?(fig_width/8):(fig_width/8+1);
	  uint8_t POSX = (uint8_t) pos_x/8;
	  uint8_t POSY = pos_y;
	  UDOUBLE Addr = 0, Addr_fig = 0;
	  // UDOUBLE Offset = ImageName;
	  for (UWORD j = 0; j < fig_height; j++) {
	    for (UWORD i = 0; i < figWidth; i++) {
	      Addr = (i+POSX) + (j+POSY)* Width;
	      Addr_fig = i + j * figWidth;
	      Paint.Image[Addr] = fig[Addr_fig];
	    }
	  }
}

double speed_to_angle(double speed){
  double angle_ratio = speed / MAX_SPEED;

  return angle_ratio * MAX_ANGLE;
}

double unit_conversion(double speed, double door_radius, int unit){
	switch(unit){
		case MM_PER_SEC:
			return speed;
		case M_PER_SEC:
			return speed / 1000;
		case FEET_PER_SEC:
			return speed / 304.8;
		case DEGS_PER_SEC:
			return speed / (2 * 3.142 * door_radius) / 360;
		default:
			return speed;
	}
}

/*******************************************************************
	Function: Draw the gauge's outline

	Note: Screen split into quadrants in algorithm, layout:

			Q2	|	Q1
		________|________
				|
			Q3	|

*******************************************************************/
void draw_gauge_edges(){
  //Gauge Cutoffs
  Paint_DrawLine(50, 187, 65, 160, BLACK, LINE_STYLE_SOLID, DOT_PIXEL_2X2);
  Paint_DrawLine(187, 50, 160, 65, BLACK, LINE_STYLE_SOLID, DOT_PIXEL_2X2);

  int x1 = 0;
  int x2 = 0;
  int x3 = 0;
  int x4 = 0;
  double slope;

  for(int y = 0; y < 100; y++){
    //Radius calculations with outlier conditions
    if((y)*(y) < 4900){
      x1 = sqrt(4900 - (y)*(y));
    }
    else {
      x1 = 0;
    }
    if((y)*(y) < 5625){
      x2 = sqrt(5625 - (y)*(y));
    }
    else{
      x2 = 0;
    }
    if((y)*(y) <= 9025){
      x3 = sqrt(9025 - (y)*(y));
    }
    else{
      x3 = 0;
    }
    if((y)*(y) <= 10000){
      x4 = sqrt(10000 - (y)*(y));
    }
    else{
      x4 = 0;
    }

    //Inner Circle
    for(int x = x1; x < x2; x++){

      //Quadrant 1 with cutoff (defined by slope)
      slope = (50.0) / (87.0);

      if(x <= (y) / slope){
	Paint_DrawPoint(x + 100, -y + 100, BLACK, DOT_PIXEL_2X2, DOT_FILL_AROUND); //Q1
      }

      //Quadrant 2
      Paint_DrawPoint(100 - x, -y + 100, BLACK, DOT_PIXEL_2X2, DOT_FILL_AROUND); //Q2

      //Quadrant 3 with cutoff
      slope = (87.0) / (50.0);

      if(x >= (y) / slope){
	Paint_DrawPoint(100 - x, y + 100, BLACK, DOT_PIXEL_2X2, DOT_FILL_AROUND); //Q3
      }

    }

    //Outer Circle
    for(int x = x3; x < x4; x++){

      //Quadrant 1 with cutoff
      slope = (50.0) / (87.0);

      if(x <= (y) / slope){
	Paint_DrawPoint(x + 100, -y + 100, BLACK, DOT_PIXEL_2X2, DOT_FILL_AROUND); //Q1
      }

      //Quadrant 2
      Paint_DrawPoint(100 - x, -y + 100, BLACK, DOT_PIXEL_2X2, DOT_FILL_AROUND); //Q2

      //Quadrant 3 with cutoff
      slope = (87.0) / (50.0);

      if(x >= (y) / slope){
	Paint_DrawPoint(100 - x, y + 100, BLACK, DOT_PIXEL_2X2, DOT_FILL_AROUND); //Q3
      }
    }
  }
}

/*******************************************************************
	Function: Draw the gauge's contents

	Parameters: open_angle represents greatest non-closed speed
				close_angle represents lowest closed speed

	Note: Screen split into quadrants in algorithm, layout:

			Q2	|	Q1
		________|________
				|
			Q3	|

*******************************************************************/
void draw_gauge_filling(double open_angle, double close_angle){
  int x2 = 0;
  int x3 = 0;

  double slope;

  for(int y = 0; y < 100; y++){
    //Radius calculations with outlier conditions
    if((y)*(y) < 5625){
      x2 = sqrt(5625 - (y)*(y));
    }
    else{
      x2 = 0;
    }
    if((y)*(y) <= 9025){
      x3 = sqrt(9025 - (y)*(y));
    }
    else{
      x3 = 0;
    }

    //Fill-in Circle
    for(int x = x2; x < x3; x++){
      if((x + y) % 2 == 0){
	//Quadrant 3
	slope = (87.0) / (50.0);
	//Gauge Cutoff Condition
	if(x >= (y) / slope){
	  //Door Not Closed
	  //Full Draw
	  if(open_angle >= 1.047) {
	    Paint_DrawPoint(100 - x, y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q3
	  }
	  //Partial Draw
	  else{
	    if(x <= (y) / tan(1.047 - open_angle)){
	      Paint_DrawPoint(100 - x, y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q3
	    }
	  }

	  //Door Closed
	  //No Draw
	  if(close_angle >= 1.047) {
	  }
	  //Partial Draw
	  else{
	    if(x >= (y) / tan(1.047 - close_angle)){
	      Paint_DrawPoint(100 - x, y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q3
	    }
	  }

	}

	//Quadrant 2
	//Door Not Closed
	//No Draw
	if(open_angle < 1.047){
	}
	//Full Draw
	else if(open_angle >= 2.618) {
	  Paint_DrawPoint(100 - x, -y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q2
	}
	//Partial Draw
	else{
	  if(x >= (y) / tan(open_angle - 1.047)){
	    Paint_DrawPoint(100 - x, -y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q2
	  }
	}

	//Door Closed
	//Full Draw
	if(close_angle < 1.047){
	  Paint_DrawPoint(100 - x, -y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q2
	}
	//No Draw
	else if(close_angle >= 2.618) {
	}
	//Partial Draw
	else{
	  if(x <= (y) / tan(close_angle - 1.047)){
	    Paint_DrawPoint(100 - x, -y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q2
	  }
	}

	//Quadrant 1
	slope = (50.0) / (87.0);
	//Gauge Limit Condition
	if(x <= (y) / slope){
	  //Door Not Closed
	  //No Draw
	  if(open_angle < 2.618){
	  }
	  //Partial Draw
	  else{
	    if(x <= (y) / tan(4.189 - open_angle)){
	      Paint_DrawPoint(x + 100, -y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q1
	    }
	  }

	  //Door Closed
	  //Full Draw
	  if(close_angle < 2.618){
	    Paint_DrawPoint(x + 100, -y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q1
	  }
	  //Partial Draw
	  else{
	    if(x >= (y) / tan(4.189 - close_angle)){
	      Paint_DrawPoint(x + 100, -y + 100, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND); //Q1
	    }
	  }

	}
      }
    }
  }
}

void hex_draw(int starting_x, int starting_y, int img_width, int img_height, const unsigned char character[], bool black){
  int x = starting_x;
  int y = starting_y;
  int x_boundary = starting_x + img_width;
  int img_size = img_width * img_height / 8;
  //Fix divisible by 8 issue

  // Iterate through all hexs
  for(int a = 0; a < img_size; a++){
    //Iterate through all bits of each hex
    for (int i = 7; i >= 0; i--) {
      //Condition for reaching end of width, reset x
      if (x == x_boundary){
		x = starting_x;
		y++;
		  }
		  //Print black/white depending on bit
		  if ((1 << i) & character[a]) {
			  if (black) {
				  Paint_DrawPoint(x, y, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND);
			  }
			  else{
				  Paint_DrawPoint(x, y, WHITE, DOT_PIXEL_1X1, DOT_FILL_AROUND);
			  }
		  }
		  else {
			  if (black) {
				  Paint_DrawPoint(x, y, WHITE, DOT_PIXEL_1X1, DOT_FILL_AROUND);
			  }
			  else{
				  Paint_DrawPoint(x, y, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND);
			  }
		  }
		  x++;
		}
  }
}

char * num_to_string(float num, char * numStr, int unit){
	if(unit == MM_PER_SEC){
		if(num < 1000){
			snprintf(numStr, 8, "%d", 3);
			snprintf(numStr + 1, 7, "%.0f", num);
			return numStr;
		}
		else {
			snprintf(numStr, 8, "%d", 4);
			snprintf(numStr, 7, "%.0f", num);
			return numStr;
		}
	}

	else if(unit == M_PER_SEC){
		snprintf(numStr, 8, "%d", 5);
		snprintf(numStr + 1, 7, "%.3f", num);
		return numStr;
	}

	else if(unit == FEET_PER_SEC){
		if(num < 10.000){
			snprintf(numStr, 8, "%d", 4);
			snprintf(numStr + 1, 7, "%.2f", num);
			return numStr;
		}
		else{
			snprintf(numStr, 8, "%d", 5);
			snprintf(numStr + 1, 7, "%.2f", num);
			return numStr;
		}
	}

	else if(unit == DEGS_PER_SEC){
		if (num < 10.00){
			snprintf(numStr, 8, "%d", 3);
			snprintf(numStr + 1, 7, "%.1f", num);
			return numStr;
		}
		else if (num >= 10.00 && num < 100.00){
			snprintf(numStr, 8, "%d", 4);
			snprintf(numStr + 1, 7, "%.1f", num);
			return numStr;
		}
		else if (num >= 100.00){
			snprintf(numStr, 8, "%d", 5);
			snprintf(numStr + 1, 7, "%.1f", num);
			return numStr;
		}
		else{
			return numStr;
		}
	}

	else{
		return numStr;
	}
}

// Calculates where starting_x should be to center the number on screen
int starting_x_for_center(char * numStr) {
	int pixel_width = 0;
	int array_size = (int) numStr[0] - 48;

	for(int i = 1; i < array_size + 1; i++){
		if(numStr[i] == '.'){
			pixel_width += 8;
		}
		else{
			pixel_width += 32;
		}
	}

	return 100 - pixel_width / 2;
}

// Calculates where starting_x should be to keep decimal fixed near gauge
int starting_x_for_decimal_stabilization(char * numStr, int unit){
	int pixel_width = 0;
	int array_size = (int) numStr[0] - 48;

	if(unit == MM_PER_SEC){
		if (array_size == 3){
			return 82;
		}
		else if (array_size == 4) {
			return 50;
		}
		else{
			return 50;
		}
	}

	else if(unit == M_PER_SEC){
		return 50;
	}

	else if(unit == FEET_PER_SEC){
		if (array_size == 4){
			return 82;
		}
		else if (array_size == 5) {
			return 50;
		}
		else{
			return 50;
		}
	}

	else if(unit == DEGS_PER_SEC){
		if (array_size == 3){
			return 114;
		}
		else if (array_size == 4){
			return 82;
		}
		else if (array_size == 5){
			return 50;
		}
		else{
			return 114;
		}
	}

	else{
		return 50;
	}
}

void multi_char_draw(int starting_x, int starting_y, char * numStr, bool black){
	int array_size = (int) numStr[0] - 48;

	for(int i = 1; i < array_size + 1; i++){
		switch(numStr[i]){
			case '0':
				hex_draw(starting_x, starting_y, 32, 64, zero_char, black);
				starting_x += 32;
				break;
			case '1':
				hex_draw(starting_x, starting_y, 32, 64, one_char, black);
				starting_x += 32;
				break;
			case '2':
				hex_draw(starting_x, starting_y, 32, 64, two_char, black);
				starting_x += 32;
				break;
			case '3':
				hex_draw(starting_x, starting_y, 32, 64, three_char, black);
				starting_x += 32;
				break;
			case '4':
				hex_draw(starting_x, starting_y, 32, 64, four_char, black);
				starting_x += 32;
				break;
			case '5':
				hex_draw(starting_x, starting_y, 32, 64, five_char, black);
				starting_x += 32;
				break;
			case '6':
				hex_draw(starting_x, starting_y, 32, 64, six_char, black);
				starting_x += 32;
				break;
			case '7':
				hex_draw(starting_x, starting_y, 32, 64, seven_char, black);
				starting_x += 32;
				break;
			case '8':
				hex_draw(starting_x, starting_y, 32, 64, eight_char, black);
				starting_x += 32;
				break;
			case '9':
				hex_draw(starting_x, starting_y, 32, 64, nine_char, black);
				starting_x += 32;
				break;
			case '.':
				hex_draw(starting_x, starting_y + 56, 8, 8, decimal_char, black);
				starting_x += 8;
				break;
			case ':':
				hex_draw(starting_x, starting_y, 32, 64, colon_char, black);
				starting_x += 32;
				break;
			default:
				hex_draw(starting_x, starting_y, 32, 64, invalid_char, black);
				starting_x += 32;
				break;
		}
	}
}

void black_screen(){
	for (int i = 0; i < 200; i++){
		for (int j = 0; j < 200; j++){
			Paint_DrawPoint(i, j, BLACK, DOT_PIXEL_1X1, DOT_FILL_AROUND);
		}
	}
}

// Currently used for testing
// init only needs
//  EPD_Init(lut_partial_update/lut_full_update);
//  EPD_Clear();
void init_edp(void){
  static uint8_t current = 1;
  static UWORD Width, Height,i = 0, j=0, Imagesize;
  uint32_t angle = 0, time = 0;
  UBYTE *TestImage;
  UBYTE *RotImage;
  UBYTE *BlackImage;

 //init_screen_buffers();
  Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
  Height = EPD_HEIGHT;

  if(EPD_Init(lut_full_update) != 0) {
      printf("e-Paper init failed\r\n");
    }

	//EPD_Clear();
}

void init_screen_buffer(void){
  // Measurement screen

  uint32_t Imagesize = ((EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1)) * EPD_HEIGHT;
  if((test_screen_buffer = (UBYTE *)calloc(Imagesize, sizeof(UBYTE))) == NULL) {
    ESP_LOGE(__func__,"Cannot alloc buffer");
  }
  Paint_NewImage(test_screen_buffer, EPD_WIDTH, EPD_HEIGHT, 270, WHITE);
  Paint_SelectImage(test_screen_buffer);
  Paint_Clear(WHITE);
  // Load GUI

  // Sleep screen
  if((sleep_screen_buffer = (UBYTE *)calloc(Imagesize, sizeof(UBYTE))) == NULL) {
    ESP_LOGE(__func__,"Cannot alloc buffer");
  }
  printf("  \n at starting of the screen function \n");
  Paint_NewImage(sleep_screen_buffer, EPD_WIDTH, EPD_HEIGHT, 270, WHITE);
  Paint_SelectImage(sleep_screen_buffer);
  Paint_Clear(WHITE);
 // black_screen();
  //EPD_Display(&EZMlogo[0]);
  // Load image

  //measurement screen test
  Paint_SelectImage(test_screen_buffer);
  Paint_Clear(WHITE);
 // EPD_Display(measurement_screen_buffer);
  	/* sprintf((char *) results_measurement_string, "%.*f",FLOAT_TO_STRING_DIGITS , screen_queue_package.analysis_result.measurement); */
  	/* ESP_LOGI(__func__, "Float %f, String %s", screen_queue_package.analysis_result.measurement, results_measurement_string); */
  	/* Paint_DrawString_EN(5, 5,(char *) results_measurement_string, &Font20, WHITE, BLACK); */
  	// Porbably good idea to save what is on the screen somewhere
  	//current_screen_measurement = screen_queue_package.analysis_result.measurement;*/
//
//  uint8_t FLOAT_TO_STRING_DIGITS = 4;
//  float measurement_value = (rand() % 1001) + 500;
//  char toPrint = {'4','0','9'};
//  uint8_t results_measurement_string[4] = {0};
//  double angle_ex = speed_to_angle(800);
//  draw_gauge_edges();
//  draw_gauge_filling(angle_ex, 3.660);
//  memset(results_measurement_string, 0, FLOAT_TO_STRING_DIGITS);
//  num_to_string(measurement_value, (char *) results_measurement_string, MM_PER_SEC);
//  int starting_x = starting_x_for_center((char *) "1234");
//  starting_x = starting_x_for_decimal_stabilization((char *) "1234", MM_PER_SEC);
//  multi_char_draw(starting_x, 80,&toPrint ,true);
//  EPD_Display(EZMlogo);
//  vTaskDelay(pdMS_TO_TICKS(5000));
  EPD_Clear();
  EPD_TurnOnDisplay();
  EPD_Init(lut_full_update);
  vTaskDelay(pdMS_TO_TICKS(2000));
  UWORD Width, Height;
 	    Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
 	    Height = EPD_HEIGHT;

 	    UDOUBLE Addr = 0;
 	    // UDOUBLE Offset = ImageName;
 	    EPD_SetWindows(0,0, EPD_WIDTH, EPD_HEIGHT);

 	    for (UWORD j = 0; j < Height; j++) {
 	      EPD_SetCursor(0, j);
 	      EPD_SendCommand(WRITE_RAM);
 	      for (UWORD i = 0; i < Width; i++) {
 	        //Addr = i + j * Width;
 	       // EPD_SendData(Image[Addr]);
 	        //testing- comment out
 	        	EPD_SendData(0x00);
 	      }
 	    }
 	    Paint_SelectImage(white_screen);
 	    EPD_Display(white_screen);
 	    add_circular_border(95);
 	    add_figure(battery_icon,32,32,130,15);
 	    draw_rectangle(17, 4 , 131, 22, BLACK);
 	    add_figure(wifi_icon, 32, 32, 100, 15);
 	    add_figure(bt_icon, 32, 32, 70, 15);
 	    EPD_Init(lut_partial_update);
 	    EPD_Display(white_screen);
 	    vTaskDelay(pdMS_TO_TICKS(1000));
// 	    Paint_SetRotate(90);
 	    EPD_Display(white_screen);
 	    vTaskDelay(pdMS_TO_TICKS(3000));

 	    UBYTE *output = (UBYTE *) calloc(5000, sizeof(UBYTE));
 	    Paint_SelectImage(output);
 	    for(int i = 0; i<360; i= i+60){
 	    	RotateWithClip(white_screen, output, i);
 	    	add_circular_border(95);
 	    	EPD_Display(output);
 	    	memset(output, 0x00, 5000);
 	    	vTaskDelay(pdMS_TO_TICKS(1000));}


// 	    memset(output, 0xff, 5000);
// 	    RotateWithClip(white_screen, output, 90);
// 	   EPD_Display(output);


// 	    EPD_TurnOnDisplay();
// 	   vTaskDelay(pdMS_TO_TICKS(2000));
//	    // UDOUBLE Offset = ImageName;
// 	    EPD_SetWindows(0,0, EPD_WIDTH, EPD_HEIGHT);
//
// 	    for (UWORD j = 0; j < Height; j++) {
// 	      EPD_SetCursor(0, j);
// 	      EPD_SendCommand(WRITE_RAM);
// 	      for (UWORD i = 0; i < Width; i++) {
// 	        	EPD_SendData(0x77);
// 	      }
// 	    }
// 	 //   EPD_Display(EZMlogo);
// 	    EPD_TurnOnDisplay();


}

// Main task to paint updates
void update_edp_task(void * command){
 // EPD_SetWindows(0, 0, EPD_WIDTH, EPD_HEIGHT);
  measurement_handler_queue_package_t measurement_package;
  uint8_t FLOAT_TO_STRING_DIGITS = 4;
  //EPD_Init(lut_partial_update);
  UBYTE *screen_buffer;
  uint32_t Imagesize = ((EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1)) * EPD_HEIGHT;
  if((screen_buffer = (UBYTE *)calloc(Imagesize, sizeof(UBYTE))) == NULL)
     ESP_LOGE(__func__,"Cannot alloc buffer");
  float measurement_value;
  char measured_value[4] ={'1','3','4','5'};
  for(;;){
	  	  if(xQueueReceive(new_measurement_queue,(void *)&measurement_value, portMAX_DELAY )== pdTRUE)
	  	  {
	  	  	  EPD_Init(lut_partial_update);
	  	  	  Paint_SelectImage(screen_buffer);
	  		  Paint_Clear(WHITE);
	  		  //ESP_LOGI(__func__,"measured value = %f \n", (float)measurement_value);

//	  		  float measurement_value = (rand() % 1001) + 500;
	  		  char toPrint[3] = {'4','0','9'};
	  		  uint8_t results_measurement_string[4] = {0};
	  		  double angle_ex = speed_to_angle(measurement_value);
	  		  draw_gauge_edges();
	  		  draw_gauge_filling(angle_ex, 3.600);
	  		  memset(results_measurement_string, 0, FLOAT_TO_STRING_DIGITS);
	  		  num_to_string(measurement_value, (char *) toPrint, M_PER_SEC);
	  		  int starting_x = starting_x_for_center((char *) toPrint);
	  		  starting_x = starting_x_for_decimal_stabilization((char *)toPrint , M_PER_SEC);
	  		  multi_char_draw(50, 80, &toPrint[0] ,true);
	  		  EPD_Display(screen_buffer);
	  		  vTaskDelay(pdMS_TO_TICKS(333));
	  	  }

  	  	 }
}


//void update_edp_bt_state(void * command){
//  EventBits_t uxBits;
//  const EventBits_t start_condition = BT_ACTIVE;
//
//  for(;;){
//    uxBits = xEventGroupWaitBits(deviceStatusGroup, start_condition, pdTRUE, pdTRUE, portMAX_DELAY);
//    if((uxBits & start_condition) == start_condition){
//      // Update icon
//    }
//  }
//}
//
//void update_edp_wifi_state(void * command){
//  EventBits_t uxBits;
//  const EventBits_t start_condition = WIFI_ACTIVE;
//
//  for(;;){
//    uxBits = xEventGroupWaitBits(deviceStatusGroup, start_condition, pdTRUE, pdTRUE, portMAX_DELAY);
//    if((uxBits & start_condition) == start_condition){
//      // Update icon
//    }
//  }
//}
//void update_edp_battery_state(void * command){
//  EventBits_t uxBits;
//  const EventBits_t start_condition = BATTERY_SOC_CHANGE;
//
//  for(;;){
//    uxBits = xEventGroupWaitBits(deviceStatusGroup, start_condition, pdTRUE, pdTRUE, portMAX_DELAY);
//    if((uxBits & start_condition) == start_condition){
//      // Update battery soc
//    }
//  }
//}
