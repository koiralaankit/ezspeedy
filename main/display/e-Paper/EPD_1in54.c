/*****************************************************************************
 * | File      	:	EPD_1in54.c
 * | Author      :   Waveshare team
 * | Function    :   Electronic paper driver
 * | Info        :
 *----------------
 * |	This version:   V2.0
 * | Date        :   2018-10-30
 * | Info        :   
 * 1.Remove:ImageBuff[EPD_HEIGHT * EPD_WIDTH / 8]
 * 2.Change:EPD_Display(UBYTE *Image)
 *   Need to pass parameters: pointer to cached data
 * 3.Change:
 *   EPD_RST -> EPD_RST_PIN
 *   EPD_DC -> EPD_DC_PIN
 *   EPD_CS -> EPD_CS_PIN
 *   EPD_BUSY -> EPD_BUSY_PIN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documnetation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to  whom the Software is
# furished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
******************************************************************************/
/*======================
 * NOTE
 * This code does not respect SPI bux mutex as it is on a separate bus on the dev board
 * The NGEA will share the perihperal bus with the screen.
 * This code will need mutex code; use code from adc code for example
 * Read eink datahsset, IL3820, and SSD1608_1.2 datasheet to understand what commands mean
 * ===================
 * This code is adapted from Waveshare library.
 * https://www.waveshare.com/wiki/1.54inch_e-Paper_Module
 * Use the correct library from vendor for the chosen screen. Not all screens are directly cross-compatible
 */
#define MIKROE_SCREEN
//#define WAVESHARE_SCREEN
#include "EPD_1in54.h"
#include "Debug.h"
#include "esp_log.h"
const unsigned char lut_full_update[] = {
  0x02, 0x02, 0x01, 0x11, 0x12, 0x12, 0x22, 0x22,
  0x66, 0x69, 0x69, 0x59, 0x58, 0x99, 0x99, 0x88,
  0x00, 0x00, 0x00, 0x00, 0xF8, 0xB4, 0x13, 0x51,
  0x35, 0x51, 0x51, 0x19, 0x01, 0x00
};

const unsigned char lut_partial_update[] = {
  0x10, 0x18, 0x18, 0x08, 0x18, 0x18, 0x08, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x13, 0x14, 0x44, 0x12,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
/******************************************************************************
function :	Software reset
parameter:
******************************************************************************/
static void EPD_Reset(void)
{
  DEV_Digital_Write(EPD_RST_PIN, 1);
  DEV_Delay_ms(200);
  DEV_Digital_Write(EPD_RST_PIN, 0);
  DEV_Delay_ms(200);
  DEV_Digital_Write(EPD_RST_PIN, 1);
  DEV_Delay_ms(200);
}

/******************************************************************************
function :	send command
parameter:
     Reg : Command register
******************************************************************************/
void EPD_SendCommand(UBYTE Reg)
{
  DEV_Digital_Write(EPD_DC_PIN, 0);
  DEV_Digital_Write(EPD_CS_PIN, 0);
  DEV_SPI_WriteByte(Reg);
  DEV_Digital_Write(EPD_CS_PIN, 1);
}

/******************************************************************************
function :	send data
parameter:
    Data : Write data
******************************************************************************/
void EPD_SendData(UBYTE Data)
{
  DEV_Digital_Write(EPD_DC_PIN, 1);
  DEV_Digital_Write(EPD_CS_PIN, 0);
  DEV_SPI_WriteByte(Data);
  DEV_Digital_Write(EPD_CS_PIN, 1);
}

/******************************************************************************
function :	Wait until the busy_pin goes LOW
parameter:
******************************************************************************/
void EPD_WaitUntilIdle(void)
{
  Debug("e-Paper busy\r\n");
  while(DEV_Digital_Read(EPD_BUSY_PIN) == 1) {      //LOW: idle, HIGH: busy
    DEV_Delay_ms(10);
  }
  ESP_LOGI("","e-Paper busy release\r\n");
}

/******************************************************************************
function :	Setting the display window
parameter:
******************************************************************************/
void EPD_SetWindows(UWORD Xstart, UWORD Ystart, UWORD Xend, UWORD Yend)
{
  EPD_SendCommand(SET_RAM_X_ADDRESS_START_END_POSITION);
  EPD_SendData((Xstart >> 3) & 0xFF);
  EPD_SendData((Xend >> 3) & 0xFF);

  EPD_SendCommand(SET_RAM_Y_ADDRESS_START_END_POSITION);
  EPD_SendData(Ystart & 0xFF);
  EPD_SendData((Ystart >> 8) & 0xFF);
  EPD_SendData(Yend & 0xFF);
  EPD_SendData((Yend >> 8) & 0xFF);
}

/******************************************************************************
function :	Set Cursor
parameter:
******************************************************************************/
void EPD_SetCursor(UWORD Xstart, UWORD Ystart)
{
  EPD_SendCommand(SET_RAM_X_ADDRESS_COUNTER);
  EPD_SendData((Xstart >> 3) & 0xFF);

  EPD_SendCommand(SET_RAM_Y_ADDRESS_COUNTER);
  EPD_SendData(Ystart & 0xFF);
  EPD_SendData((Ystart >> 8) & 0xFF);

}

/******************************************************************************
function :	Turn On Display
parameter:
******************************************************************************/
void EPD_TurnOnDisplay(void)
{
//  EPD_SendCommand(DISPLAY_UPDATE_CONTROL_1);
//  EPD_SendData(0x80);
  EPD_SendCommand(DISPLAY_UPDATE_CONTROL_2);

#ifdef MIKROE_SCREEN
 EPD_SendData(0xC4);// EPD_SendData(0xC4);
#endif
#ifdef WAVESHARE_SCREEN
  EPD_SendData(0xFF);
#endif
  ESP_LOGI("","Master activation");
  EPD_SendCommand(MASTER_ACTIVATION);
  //EPD_SendCommand(TERMINATE_FRAME_READ_WRITE);

  EPD_WaitUntilIdle();


}

/******************************************************************************
function :	Initialize the e-Paper register
parameter:
******************************************************************************/
#ifdef MIKROE_SCREEN
UBYTE EPD_Init(const unsigned char* lut)
{
  EPD_Reset();

  EPD_SendCommand(DRIVER_OUTPUT_CONTROL);
  EPD_SendData((EPD_HEIGHT - 1) & 0xFF);
  EPD_SendData(((EPD_HEIGHT - 1) >> 8) & 0xFF);
  EPD_SendData(0x00);                     // GD = 0; SM = 0; TB = 0;
  EPD_SendCommand(BOOSTER_SOFT_START_CONTROL);
  EPD_SendData(0xD7);
  EPD_SendData(0xD6);
  EPD_SendData(0x9D);
  EPD_SendCommand(WRITE_VCOM_REGISTER);
  EPD_SendData(0xA8);                     // VCOM 7C
  EPD_SendCommand(SET_DUMMY_LINE_PERIOD);
  EPD_SendData(0x1A);                     // 4 dummy lines per gate
  EPD_SendCommand(SET_GATE_TIME);
  EPD_SendData(0x08);                     // 2us per line
  EPD_SendCommand(DATA_ENTRY_MODE_SETTING);
  EPD_SendData(0x03);

  //set the look-up table register
  EPD_SendCommand(WRITE_LUT_REGISTER);
  for (UWORD i = 0; i < 30; i++) {
    EPD_SendData(lut[i]);
  }

  return 0;
}
#endif

#ifdef WAVESHARE_SCREEN
// For second screen from waveshare (green sticker)
UBYTE EPD_Init(const unsigned char* lut)
{
  EPD_Reset();
  EPD_WaitUntilIdle();
  EPD_SendCommand(0x12);  //SWRESET
  EPD_WaitUntilIdle();

  EPD_SendCommand(0x01); //Driver output control
  EPD_SendData(0xC7);
  EPD_SendData(0x00);
  EPD_SendData(0x01);

  EPD_SendCommand(0x11); //data entry mode
  EPD_SendData(0x3);

  EPD_SendCommand(0x44); //set Ram-X address start/end position
  EPD_SendData(0x00);
  EPD_SendData(0x18);    //0x0C-->(18+1)*8=200

  EPD_SendCommand(0x45); //set Ram-Y address start/end position
  EPD_SendData(0x00);   //0xC7-->(199+1)=200
  EPD_SendData(0x00);
  EPD_SendData(0xC7);
  EPD_SendData(0x00);

  EPD_SendCommand(0x3C); //BorderWavefrom
  EPD_SendData(0x01);

  EPD_SendCommand(0x18);
  EPD_SendData(0x80);

  EPD_SendCommand(0x22); // //Load Temperature and waveform setting.
  EPD_SendData(0XB1);
  EPD_SendCommand(0x20);

  EPD_SendCommand(0x4E);   // set RAM x address count to 0;
  EPD_SendData(0x00);
  EPD_SendCommand(0x4F);   // set RAM y address count to 0X199;
  EPD_SendData(0xC7);
  EPD_SendData(0x00);
  EPD_WaitUntilIdle();


  return 0;
}
#endif

/******************************************************************************
function :	Clear screen
parameter:
******************************************************************************/
void EPD_Clear(void)
{
  UWORD Width, Height;
  Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
  Height = EPD_HEIGHT;
  EPD_SetWindows(0, 0, EPD_WIDTH, EPD_HEIGHT);
  for (UWORD j = 0; j < Height; j++) {
    EPD_SetCursor(0, j);
    EPD_SendCommand(WRITE_RAM);
    for (UWORD i = 0; i < Width; i++) {
      EPD_SendData(0XFF);
    }
  }
  EPD_TurnOnDisplay();
}

/******************************************************************************
function :	Sends the image buffer in RAM to e-Paper and displays
parameter:
******************************************************************************/
#ifdef MIKROE_SCREEN
void EPD_Display(UBYTE *Image)
{
  UWORD Width, Height;
  Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
  Height = EPD_HEIGHT;

  UDOUBLE Addr = 0;
  // UDOUBLE Offset = ImageName;
  EPD_SetWindows(0, 0, EPD_WIDTH, EPD_HEIGHT);

  for (UWORD j = 0; j < Height; j++) {
    EPD_SetCursor(0, j);
    EPD_SendCommand(WRITE_RAM);
    for (UWORD i = 0; i < Width; i++) {
      Addr = i + j * Width;
      EPD_SendData(Image[Addr]);
      //EPD_SendData(0x00);
    }
  }
  EPD_TurnOnDisplay();



}
#endif

#ifdef WAVESHARE_SCREEN
// For second screen - wave share screen
void EPD_Display(UBYTE *Image)
{
  UWORD Width, Height;
  Width = (EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1);
  Height = EPD_HEIGHT;

  UDOUBLE Addr = 0;
  EPD_SendCommand(0x24);
  for (UWORD j = 0; j < Height; j++) {
    for (UWORD i = 0; i < Width; i++) {
      Addr = i + j * Width;
      EPD_SendData(Image[Addr]);
    }
  }

  EPD_TurnOnDisplay();
}
#endif
/******************************************************************************
function :	Enter sleep mode
parameter:
******************************************************************************/
void EPD_Sleep(void)
{
  EPD_SendCommand(DEEP_SLEEP_MODE);
  EPD_SendData(0x01);
  EPD_WaitUntilIdle();
}
