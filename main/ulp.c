/*
* ulp.c
*
*  Created on: Jul 10, 2020
*      Author: JFung
*/

#include "ulp.h"
#include "main.h"
#include "flash_functions.h"
#include "driver/gpio.h"

// ULP
#include "soc/rtc_cntl_reg.h"
#include "soc/sens_reg.h"
#include "soc/rtc_periph.h"
#include "driver/rtc_io.h"
#include "esp32/ulp.h"
#include "ulp_main.h"
#include "esp_sleep.h"
#include "soc/rtc_cntl_reg.h"

/* ===============
* ULP CODE
* ================ */
extern const uint8_t ulp_main_bin_start[] asm("_binary_ulp_main_bin_start");
extern const uint8_t ulp_main_bin_end[]   asm("_binary_ulp_main_bin_end");

void init_ulp_program(void)
{
esp_err_t err = ulp_load_binary(0, ulp_main_bin_start,
                (ulp_main_bin_end - ulp_main_bin_start) / sizeof(uint32_t));
ESP_ERROR_CHECK(err);

/* GPIO used for pulse counting. */
gpio_num_t sleep_wakeup_gpio_num = GPIO_NUM_36; // Change later to actual pin on EZSpeedy board
//gpio_num_t transit_mode_wakeup_gpio_num = GPIO_NUM_35; // Change later to actual pin on EZSpeedy board
int sleep_wakeup_rtcio_num = rtc_io_number_get(sleep_wakeup_gpio_num);
assert(rtc_gpio_is_valid_gpio(sleep_wakeup_gpio_num) && "GPIO used for pulse counting must be an RTC IO");
//int transit_mode_wakeup_rtcio_num = rtc_io_number_get(transit_mode_wakeup_gpio_num);

//int transit_mode_disabled = 0, completed;
//xQueueHandle trans_completed = xQueueCreate(1,sizeof(uint32_t)); // Queue to block code during read op
//nvs_package_t nvs_package = {.nvs_key = TRANSIT_MODE_KEY, .data_type=NVS_TYPE_U32, .data_to_write=NULL,
 //               .read_data = &transit_mode_disabled, .read_completed_notif = trans_completed};
//xQueueSendToBack(nvs_transaction_queue, &nvs_package, portMAX_DELAY);
//if(xQueueReceive(trans_completed, &completed,5000/portTICK_PERIOD_MS)){ // Wait until value read forma
  //  ESP_LOGI(__func__, "Transit mode disabled");
   // }else{
   // ESP_LOGI(__func__, "Transit mode enabled");
   // }
//}
//ulp_transit_mode_disabled = transit_mode_disabled;

/* Initialize some variables used by ULP program.
* Each 'ulp_xyz' variable corresponds to 'xyz' variable in the ULP program.
* These variables are declared in an auto generated header file,
* 'ulp_main.h', name of this file is defined in component.mk as ULP_APP_NAME.
* These variables are located in RTC_SLOW_MEM and can be accessed both by the
* ULP and the main CPUs.
*
* Note that the ULP reads only the lower 16 bits of these variables.
*/
ulp_debounce_counter = 3;
ulp_debounce_max_count = 3;
ulp_next_edge = 0;
ulp_sleep_wakeup_io_number = sleep_wakeup_rtcio_num; /* map from GPIO# to RTC_IO# */
ulp_edge_count_to_wake_up = 2;

//ulp_transit_mode_wakeup_io_number = transit_mode_wakeup_rtcio_num;

/* Initialize selected GPIO as RTC IO, enable input, disable pullup and pulldown */
rtc_gpio_init(sleep_wakeup_gpio_num);
rtc_gpio_set_direction(sleep_wakeup_gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
rtc_gpio_pulldown_dis(sleep_wakeup_gpio_num);
rtc_gpio_pullup_dis(sleep_wakeup_gpio_num);
rtc_gpio_hold_en(sleep_wakeup_gpio_num);

// rtc_gpio_init(transit_mode_wakeup_gpio_num);
// rtc_gpio_set_direction(transit_mode_wakeup_gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
// rtc_gpio_pulldown_dis(transit_mode_wakeup_gpio_num);
// rtc_gpio_pullup_dis(transit_mode_wakeup_gpio_num);
// rtc_gpio_hold_en(transit_mode_wakeup_gpio_num);

/* Disconnect GPIO12 and GPIO15 to remove current drain through
* pullup/pulldown resistors.
* GPIO12 may be pulled high to select flash voltage.
*/
rtc_gpio_isolate(GPIO_NUM_12);
rtc_gpio_isolate(GPIO_NUM_15);
esp_deep_sleep_disable_rom_logging(); // suppress boot messages

/* Set ULP wake up period to T = 20ms.
* Minimum pulse width has to be T * (ulp_debounce_counter + 1) = 80ms.
*/
ulp_set_wakeup_period(0, 20000);

/* Start the program */
err = ulp_run(&ulp_entry - RTC_SLOW_MEM);
ESP_ERROR_CHECK(err);
}
/* Routine after wakeup */
//void update_pulse_count(void)
//{
//// VV Example code
//const char* namespace = "plusecnt";
//const char* count_key = "count";
//
//ESP_ERROR_CHECK(nvs_flash_init());
//nvs_handle_t handle;
//ESP_ERROR_CHECK( nvs_open(namespace, NVS_READWRITE, &handle));
//uint32_t pulse_count = 0;
//esp_err_t err = nvs_get_u32(handle, count_key, &pulse_count);
//assert(err == ESP_OK || err == ESP_ERR_NVS_NOT_FOUND);
//printf("Read pulse count from NVS: %5d\n", pulse_count);
//
///* ULP program counts signal edges, convert that to the number of pulses */
//uint32_t pulse_count_from_ulp = (ulp_edge_count & UINT16_MAX) / 2;
///* In case of an odd number of edges, keep one until next time */
//ulp_edge_count = ulp_edge_count % 2;
//printf("Pulse count from ULP: %5d\n", pulse_count_from_ulp);
//
///* Save the new pulse count to NVS */
//pulse_count += pulse_count_from_ulp;
//ESP_ERROR_CHECK(nvs_set_u32(handle, count_key, pulse_count));
//ESP_ERROR_CHECK(nvs_commit(handle));
//nvs_close(handle);
//printf("Wrote updated pulse count to NVS: %5d\n", pulse_count);
//// ^^
//
//// Disable transit mode
//int transit_mode_disabled = 0, completed;
//xQueueHandle trans_completed = xQueueCreate(1,sizeof(uint32_t)); // Queue to block code during read op
//nvs_package_t nvs_package = {.nvs_key = TRANSIT_MODE_KEY, .data_type=NVS_TYPE_U32, .data_to_write=NULL,
//                .read_data = &transit_mode_disabled, .read_completed_notif = trans_completed};
//xQueueSendToBack(nvs_transaction_queue, &nvs_package, portMAX_DELAY);
//if(xQueueReceive(trans_completed, &completed,5000/portTICK_PERIOD_MS)){ // Wait until value read form NVS
//    if(transit_mode_disabled){
//    ESP_LOGI(__func__, "Transit mode disabled");
//    }else{
//    transit_mode_disabled = 1;
//    nvs_package_t nvs_package = {.nvs_key = TRANSIT_MODE_KEY, .data_type=NVS_TYPE_U32, .data_to_write=&transit_mode_disabled,
//                .read_data = NULL, .read_completed_notif = trans_completed};
//xQueueSendToBack(nvs_transaction_queue, &nvs_package, portMAX_DELAY);
//if(xQueueReceive(trans_completed, &completed,5000/portTICK_PERIOD_MS)){ // Wait until NVS transaction finishes
//ESP_LOGI(__func__, "Transit mode disabled");
//
//}
//    }
//}
//
//}
