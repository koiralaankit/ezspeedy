/*
 * sd_card_reader.h
 *
 *  Created on: Dec 23, 2020
 *      Author: Ankit koirala
 */

#ifndef MAIN_SD_CARD_READER_H_
#define MAIN_SD_CARD_READER_H_
void sd_card_init();
void sd_card_write(char *msg);
void sd_card_file_rename(char *new_name);
void sd_card_read(char *msg);
void sd_card_test();
void sd_card_start(void *args);
void test_record_batt();


#endif /* MAIN_SD_CARD_READER_H_ */
