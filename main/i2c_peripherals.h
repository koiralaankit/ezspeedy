/*
 * i2c_peripherals.h
 *
 *  Created on: Nov 13, 2020
 *      Author: Ankit koirala
 */
#include "esp_err.h"
#include <stdint.h>

#ifndef MAIN_I2C_PERIPHERALS_H_
#define MAIN_I2C_PERIPHERALS_H_

/* I2C Defines */

#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define WRITE_BIT 0  /*!< I2C master write */
#define READ_BIT  1   /*!< I2C master read */
#define ACK_CHECK_EN 0x1            /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0           /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                 /*!< I2C ack value */
#define NACK_VAL 0x1                /*!< I2C nack value */

/* I2C BUS */
#define I2C_SDA_GPIO_NUM 	25
#define I2C_SCL_GPIO_NUM 	33
#define I2C_BUS_FREQ 		50000 //MAX I2C bus frequency
#define I2C_PORT 		(1)
#define MAX_I2C_TIMEOUT  1048575

/* I2C DEVICE ADDRESSES */
#define IO_EXPANDER_LEDS	(uint8_t) 0x45 //LED IOEXPANDER ADDRESS
#define IO_REG_S2_ADDR (uint8_t)0x02
#define I2C_FUEL_GAUGE (uint8_t)0x55
#define FUEL_GAUGE_VOLTAGE_REG 0x08
#define FUEL_GAUGE_SOC 0x020

/*I2C State Variables */
//static int driver_initialized;

int i2cset_cmd(uint8_t, uint8_t, uint8_t);
int readi2c_cmd(uint8_t, uint8_t, uint8_t);
int i2cset_fuel_gauge_cmd(uint8_t, uint8_t, uint8_t);
void send_fuel_gauge_cmd();
void read_fuel_gauge_cmd();
int read_fuel_gaugeI2C(uint8_t, uint8_t, uint8_t);
void turn_on_LED(uint8_t);
void rtc_read();
int i2c_rtc_read(uint8_t, uint8_t, uint8_t *data);
int i2c_rtc_write(uint8_t, uint8_t, uint8_t);
void rtc_write(uint8_t);
esp_err_t read_battery(uint8_t fuel_gauge_reg, uint16_t *value);
esp_err_t read_battery_value(uint8_t, uint8_t, uint8_t, uint8_t*);
esp_err_t read_battery_cntl(uint8_t, uint16_t*);


#endif /* MAIN_I2C_PERIPHERALS_H_ */
