/*
 * bt.h
 *
 *  Created on: Oct 29, 2020
 *      Author: Ankit koirala
 */

#ifndef MAIN_BT_H_
#define MAIN_BT_H_

#include "esp_spp_api.h"
#include "esp_gap_bt_api.h"

/* ================= */
/* BT Server Defines */
/* ================= */
#define SPP_TAG "SPP_DAEMON"
#define TRANSMIT_TASK_TAG "TRANSMIT TASK"
#define READ_MESSAGE_QUEUE_TASK_TAG "READ TASK"
#define SPP_SERVER_NAME "SPP_SERVER"
#define BT_DEVICE_NAME "EZ Pinch 9001"

//uint32_t bt_handle;

void init_bt(void);
void esp_spp_cb(esp_spp_cb_event_t event, esp_spp_cb_param_t *param);
void esp_bt_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param);

void print(uint8_t *a, uint8_t);
void send_soc_to_bt(void *args);



#endif /* MAIN_BT_H_ */
