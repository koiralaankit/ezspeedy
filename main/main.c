#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "time.h"
#include "sys/time.h"
#include "esp_err.h"
#include "driver/i2c.h"
#include "i2c_peripherals.h"
#include "i2cbus.h"
#include "screen_functions.h"
#include "main.h"
#include "sensor_ism330dlc_functions.h"
#include "bt.h"
#include "ism330dlc_reg.h"
#include "sensor_lis331_functions.h"
#include "wifi.h"
#include "h3lis100dl_functions.h"
#include "sd_card_reader.h"
#include "message_receiver.h"
#include "flash_functions.h"
#include "ota_utils.h"
#include "esp_sleep.h"

#include "soc/rtc_cntl_reg.h"
#include "soc/sens_reg.h"
#include "soc/rtc_periph.h"
#include "driver/rtc_io.h"
#include "esp32/ulp.h"
#include "soc/rtc_cntl_reg.h"
#include "ulp.h"
//esp_err_t i2c_master_driver_initialize();

static int int_from_ismcount = 0;

/*Pins used for SPI device*/
gpio_config_t io_conf, spi_epd_cs_pin, spi_epd_busy_pin, spi_epd_rst_pin, spi_epd_dc_pin;
gpio_config_t spi_h3lis_cs_pin;
gpio_config_t ism330_int_pin,h1lis300_int_pin;;
gpio_config_t battery_full, battery_pg;


TimerHandle_t ulp_sleep_timeout_handle;
/* ================= */
/* Wifi Variables */
/* ================= */
int client_sock;
TaskHandle_t tcp_server_task_handle = NULL, wifi_mode_change_task_handle = NULL;
wifi_mode_t wifi_mode; //Keeps track of current mode - AP/ STA

#define ESP_INTR_FLAG_DEFAULT 0

void init_InertialSensorPins()
{
	 ism330_int_pin.intr_type = GPIO_PIN_INTR_NEGEDGE;
	 ism330_int_pin.pin_bit_mask = (1ULL<<36);
	 ism330_int_pin.mode = GPIO_MODE_INPUT;
	 ism330_int_pin.pull_up_en = 0;
	 ism330_int_pin.pull_down_en = 0;
	 gpio_config(&ism330_int_pin);

}

void initialize_SPI_pins()
{
   spi_epd_cs_pin.intr_type  = GPIO_INTR_DISABLE;
   spi_epd_cs_pin.pin_bit_mask = (1ULL<<4);
   spi_epd_cs_pin.mode = GPIO_MODE_OUTPUT;
   spi_epd_cs_pin.pull_up_en = 0;
   spi_epd_cs_pin.pull_down_en = 0;
   gpio_config(&spi_epd_cs_pin);

//   spi_h3lis_cs_pin.intr_type = GPIO_INTR_DISABLE;
//   spi_h3lis_cs_pin.pin_bit_mask = (1ULL<<27);
//   spi_h3lis_cs_pin.mode = GPIO_MODE_OUTPUT;
//   spi_h3lis_cs_pin.pull_up_en = 0;
//   spi_h3lis_cs_pin.pull_down_en = 0;
//   gpio_config(&spi_h3lis_cs_pin);


   spi_epd_busy_pin.intr_type = GPIO_INTR_DISABLE;
   spi_epd_busy_pin.pin_bit_mask = 1ULL<<19;
   spi_epd_busy_pin.mode = GPIO_MODE_INPUT_OUTPUT;
   spi_epd_busy_pin.pull_up_en = 1;
   spi_epd_busy_pin.pull_down_en =0;
   gpio_config(&spi_epd_busy_pin);


    spi_epd_rst_pin.intr_type = GPIO_INTR_DISABLE;
    spi_epd_rst_pin.pin_bit_mask = 1ULL<<5;
    spi_epd_rst_pin.mode = GPIO_MODE_INPUT_OUTPUT;
    spi_epd_rst_pin.pull_up_en = 1;
    spi_epd_rst_pin.pull_down_en = 0;
    gpio_config(&spi_epd_rst_pin);


    spi_epd_dc_pin.intr_type = GPIO_INTR_DISABLE;
    spi_epd_dc_pin.pin_bit_mask = 1Ull<<18;
    spi_epd_dc_pin.mode = GPIO_MODE_INPUT_OUTPUT;
    spi_epd_dc_pin.pull_up_en = 1;
    spi_epd_dc_pin.pull_down_en = 0;
    gpio_config(&spi_epd_dc_pin);


    //printf(" END OF SPI PIN INTIALIZATION");
}
I2C_t *i2c = &i2c0;

esp_err_t i2c_master_driver_initialize(){
  i2c_config_t conf = {
		       .mode = 1,
		       .sda_io_num = I2C_SDA_GPIO_NUM,
		       .sda_pullup_en = 0x0,
		       .scl_io_num = I2C_SCL_GPIO_NUM,
		       .scl_pullup_en = 0x0,
		       .master.clk_speed = I2C_BUS_FREQ
  };
  i2c_param_config((1), &conf);
  i2c_driver_install((1), I2C_MODE_MASTER, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
  i2c_set_timeout(1, MAX_I2C_TIMEOUT);
  return 0;
}

void displayHelloTask(void *args)
{
	uint8_t led= 0x0f;
	uint8_t whoamI = 0;
	uint16_t voltage = 0;
	uint16_t soc = 0 ;
	turn_on_LED(0x00);
	uint8_t cmd = 0x17;
	bool t = false;
	while(1)
	{

uxBits = xEventGroupGetBits(deviceStatusGroup);
//ESP_LOGI(__func__, " groupbits = %x", uxBits);

if(uxBits == BT_ACTIVE)
{
	//	 turn_on_LED(0x03);
	//	 ESP_LOGW(__func__, " ESP LOGW");
}
        gpio_set_level(32,0);

	//	I2C_scanner(i2c);
		//printf("hello!\n");
	  //   turn_on_LED(status_bits<<1);
	//	read_battery(0x20, &soc);
	//	   read_battery(0x20, &soc);
	//    printf("soc %d = %d \n",t, soc);
	    if(!t)
	  	    temp_soc1 = soc;
	  	    else
	  	    	temp_soc2 = soc;
	    t = !t;
//	    soc = 0x00;
//	    read_battery(0x22, &soc);
//	    printf("soc = %d \n", soc);

	    temp_full =  gpio_get_level(39);
	    temp_pg = gpio_get_level(35);
	 //   printf("temp full = %d", temp_full);
	 //   printf("temp_pg = %d\n", temp_pg);
	//	 show_boot_count();
	//	 ESP_LOGI(__func__," number of interupt = %d", int_from_ismcount);
	     voltage = 0;
	     soc = 0;
	     vTaskDelay(pdMS_TO_TICKS(1000));
	  //   status_bits = 0x00;
	   //   led = led<<1;
  // readi2c_cmd((uint8_t)0xAA, 0x02, 1);

	//rtc_write(0x80);
	    //  if(led == 0x00) led = 0x01;

	//	rtc_read();
	//	rtc_write(0x68);
	//	 vTaskDelay(pdMS_TO_TICKS(1000));

	 //   send_fuel_gauge_cmd();


	 //   vTaskDelay(pdMS_TO_TICKS(3000));
	   // gpio_set_level(19,0);



	}
}
void inertialSensor_ISR(void *args)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	ets_printf("W");
	xSemaphoreGiveFromISR(inertialSensor_drdy_semaphore, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR();
}
void inertialSensor_2(void *args)
{
  ets_printf("interupt \n");
}
void gpio_state()
{
	while(1)
	{
		//printf("GPIO VALUE = %d\n", gpio_get_level(19));
//		 if(ESP_OK== gpio_set_level(32, 0))
//			 printf(" Without config");
	//	printf("Number of interrupt received = %d ", int_from_ismcount);
	//	printf(" pin = %d", gpio_get_level(36));
		vTaskDelay(pdMS_TO_TICKS(1000));
	}

}


void init_spi_bus(void){
  esp_err_t ret;
  //Initialize the SPI bus

  spi_bus_config_t buscfg = { .miso_io_num = SPI_MISO_Pin, .mosi_io_num =
    SPI_MOSI_Pin, .sclk_io_num = SPI_CLK_Pin, .quadwp_io_num = -1,
    .quadhd_io_num = -1, .max_transfer_sz = 100 };

  ret = spi_bus_initialize(VSPI_HOST, &buscfg, 1);
  ESP_ERROR_CHECK(ret);

  // VVVV to move into corresponding .c files
//  // Configure lis331h spi
//  spi_device_interface_config_t lis331h_dev_config = { .clock_speed_hz = 4
//    * 1000 * 1000, .mode = 3, .spics_io_num = -1, // Manually setting CS
//    .queue_size = 10 };
  spi_device_interface_config_t h3lis100dl_dev_config = { .clock_speed_hz = 4
  							 * 1000 * 1000, .mode = 0, .spics_io_num = 27, .queue_size = 20,.flags = SPI_TRANS_VARIABLE_ADDR, .address_bits=8};

  //Attach the Load Cell to the SPI bus
  ret=spi_bus_add_device(VSPI_HOST, &h3lis100dl_dev_config, &h3lis100dl_handle);
  ESP_ERROR_CHECK(ret);

}

void test_nvs_struct()
{
	wifi_info_t wifi = {.ssid ="EZMetrology\0", .password="EZMetrology200006\0"};
	nvs_handle_t nvs_handle;
	esp_err_t err;

	err = nvs_open(NVS_TEST_KEY, NVS_READWRITE, &nvs_handle);
	if(err != ESP_OK)
		ESP_LOGW(__func__, "NVS: %s", esp_err_to_name(err));
	size_t required_size = 64;
	err = nvs_get_str(nvs_handle,"wifi_info_pass", NULL, &required_size);
	if(err == ESP_OK)
	{
		printf("required size = %d", required_size);

	}
	else
	{
		ESP_LOGW(__func__, "NVS: %s", esp_err_to_name(err));
		return;
	}
	if(required_size >0)
	{
		char *password = malloc(required_size);


		err = nvs_get_str(nvs_handle, "wifi_info_pass", password, &required_size);
		if(err == ESP_OK)
		{
			printf(" stored string %s: ",password);


		}
	}
	else
		return;

}
void firmware_update_diagnosis(void){
  // Set flag to indicate OTA image valid
  // Make sure enable app rollback support option is enable in idf.py menuconfig &
  // Make sure partitions are setup for OTA, before using the below routine

  const esp_partition_t *running = esp_ota_get_running_partition();
  esp_ota_img_states_t ota_state;
  if (esp_ota_get_state_partition(running, &ota_state) == ESP_OK) {
    if (ota_state == ESP_OTA_IMG_PENDING_VERIFY) {

      // run diagnostic function ...
     esp_ota_mark_app_valid_cancel_rollback();

    }
  }
}

void ulp_sleep_timer_timer_cb()
{
	gpio_num_t transit_mode_wakeup_gpio_num = GPIO_NUM_36;
	int transit_mode_wakeup_rtcio_num = rtc_io_number_get(transit_mode_wakeup_gpio_num);
	assert(rtc_gpio_is_valid_gpio(transit_mode_wakeup_gpio_num));
	rtc_gpio_init(transit_mode_wakeup_gpio_num);
	rtc_gpio_set_direction(transit_mode_wakeup_gpio_num, RTC_GPIO_MODE_INPUT_ONLY);
	rtc_gpio_pulldown_dis(transit_mode_wakeup_gpio_num);
	rtc_gpio_pullup_dis(transit_mode_wakeup_gpio_num);
	rtc_gpio_hold_en(transit_mode_wakeup_gpio_num);

	/* Disconnect GPIO12 and GPIO15 to remove current drain through
	* pullup/pulldown resistors.
	* GPIO12 may be pulled high to select flash voltage.
	*/
	rtc_gpio_isolate(GPIO_NUM_12);
	rtc_gpio_isolate(GPIO_NUM_15);
	esp_deep_sleep_disable_rom_logging(); // suppress boot messages

	esp_sleep_enable_timer_wakeup(120*1000*1000);
//	esp_sleep_enable_ext0_wakeup(transit_mode_wakeup_gpio_num, 0);
	printf("Entering Deep Sleep \n");
	esp_deep_sleep_start();
}

void app_main(void)
{
    nvs_flash_init();

    firmware_update_diagnosis();
	#if MAIN_BT == ENABLE
    	init_bt();
	#endif
    deviceStatusGroup = xEventGroupCreate();

    #if MAIN_WIFI == DISABLE
	   ESP_ERROR_CHECK(esp_netif_init()); // Init wifi periph */
	   ESP_ERROR_CHECK(esp_event_loop_create_default()); // Sorta like the wifi driver */
	//   xTaskCreate(wifi_mode_change_task, "wifi mode", 8192, NULL, 5, &wifi_mode_change_task_handle); // change from sta to ap and vv, if needed */
//	   xTaskCreate(tcp_server_handler_task, "tcp handler", 4096, NULL, 20, NULL);
	   wifi_mode = WIFI_MODE_STA; //or WIFI_MODE_STA
	  xTaskCreate(wifi_connect_task, "WIFI conenct", 4096, NULL, 2, NULL);
    #endif
    status_bits = 0x00;
 //   test_nvs_struct();
    //init_nvs_tasks();
    receive_queue = xQueueCreate(10, sizeof(message_package_t));
    bt_message_queue = xQueueCreate(20, sizeof(message_package_t));
    wifi_message_queue = xQueueCreate(20, sizeof(message_package_t));
    transmit_queue = xQueueCreate(10, sizeof(message_package_t));
    xTaskCreate(receive_from_bt, "BT message receive", 8192, NULL, 1, NULL);
    xTaskCreate(receive_from_wifi, "WIFI message receive", 8192, NULL, 1, NULL);
  //  bt_handle = 255;
    i2c_master_driver_initialize();
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pin_bit_mask = (1ULL<<32);
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pull_up_en = 0;
    io_conf.pull_down_en = 0;
    gpio_config(&io_conf);
    gpio_set_level(32,0);

    battery_full.intr_type = GPIO_INTR_DISABLE;
    battery_full.pin_bit_mask = 1ULL <<39;
    battery_full.mode = GPIO_MODE_INPUT;
    battery_full.pull_up_en = 1;
    battery_full.pull_down_en = 0;
    gpio_config(&battery_full);

    battery_pg.intr_type = GPIO_INTR_DISABLE;
    battery_pg.pin_bit_mask = 1ULL <<35;
    battery_pg.mode = GPIO_MODE_INPUT;
    battery_pg.pull_up_en = 0;
    battery_pg.pull_down_en = 0;
    gpio_config(&battery_pg);

    inertialSensor_drdy_semaphore = xSemaphoreCreateBinary();
    spi_bus_mutex = xSemaphoreCreateMutex();
    i2c_master_driver_initialize();
    initialize_SPI_pins();
    init_spi_bus();

    gpio_config_t ism330_int_pin;
    ism330_int_pin.intr_type = GPIO_PIN_INTR_NEGEDGE;
   	ism330_int_pin.pin_bit_mask = (1ULL<<36);
   	ism330_int_pin.mode = GPIO_MODE_INPUT;
   	ism330_int_pin.pull_up_en = 0;
   	ism330_int_pin.pull_down_en = 0;
   	gpio_config(&ism330_int_pin);

    gpio_config_t h3lis100dl_int_pin;
    h3lis100dl_int_pin.intr_type = GPIO_PIN_INTR_NEGEDGE;
   	h3lis100dl_int_pin.pin_bit_mask = (1ULL<<34);
   	h3lis100dl_int_pin.mode = GPIO_MODE_INPUT;
   	h3lis100dl_int_pin.pull_up_en = 1;
   	h3lis100dl_int_pin.pull_down_en =0;
   	gpio_config(&h3lis100dl_int_pin);

//    init_InertialSensorPins();
    gpio_isr_handler_add(36, inertialSensor_ISR, (void*)0);
  //    gpio_isr_handler_add(34, inertialSensor_2, (void*)1);





    new_measurement_queue = xQueueCreate(5, sizeof(float));
    i2c_semaphore = xSemaphoreCreateMutex();

   //   begin_I2C(i2c, GPIO_NUM_25, GPIO_NUM_33, 400000);
   // gpio_set_level(19,1);
  //  init_ism330dlc_activity_detect_mode();

//    xTaskCreate(gpio_state, "gpio", 4096, NULL, 2, NULL);
      xTaskCreate(displayHelloTask, "display", 8192, NULL, 1, NULL);
      measurement_handler_queue = xQueueCreate(20, sizeof(measurement_handler_queue_package_t));
   //   xTaskCreate(ota_server_task, "OTA",OTA_TASK_STACK_SIZE, NULL,OTA_TASK_PRIORITY,NULL);
//      xTaskCreate(send_soc_to_bt, "Send SOC via BT", 4096, NULL, 2, NULL);
      init_ism330dlc();
   // init_ism330dlc_activity_detect_mode();
      xTaskCreate(inertialSensor_read, "Inertial Sensor Read", 8192, NULL, 1, NULL);
  //  xTaskCreate(ism_test_read, "ism read", 8192, NULL,2 ,NULL);

//    init_epd_bus(); // Needed for screen, another spi bus
//    init_edp();
//    init_screen_buffer();
//
  //   init_lis331h();
   //  xTaskCreate(lis_test_read, "lis test read", 8192, NULL, 2, NULL);
  //  xTaskCreate(init_h3lis100dl, "init test", 8192, NULL, 2, NULL);
   //   xTaskCreate(h3lis100dl_free_fall, "lis read", 8192, NULL, 2, NULL);
 //  xTaskCreate(h3lis100dl_read_data_polling, "h3lis100dl", 8192, NULL, 2, NULL);
 //   xTaskCreate(update_edp_task,"display ", 8192, NULL,3, NULL);
 //   i2c_master_driver_initialize();

  //  i2c_master_driver_initialize_0();
 //    sd_card_test();
  //   test_record_batt();
       init_ism330dlc_ulp_mode();
     //  ulp_sleep_timeout_handle = xTimerCreate("ulptimeout", pdMS_TO_TICKS(1*10*1000), pdFALSE, NULL, ulp_sleep_timer_timer_cb);
    //   xTimerStart(ulp_sleep_timeout_handle,0);
       init_ulp_program();

}

