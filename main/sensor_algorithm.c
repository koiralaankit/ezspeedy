//-----------------------------------------//
/* Standard C Includes */
#include "sensor_algorithm.h"

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//-----------------------------------------//
/* Program Includes */
#include "main.h"
#include "wiggle.h" // Data set
#include "main.h"
#include "misc.h"
//-----------------------------------------//
/* ESP32 Specific Includes */
#include "esp_task_wdt.h"
//-----------------------------------------//
/* Configuration Defines */
#define INERTIAL_BUFFER_SIZE   (uint32_t) (500)
#define VECTOR_BUFFER_SIZE     (uint32_t) (500)
#define VECTORZERO_THRESHOLD   (uint32_t) (100)
#define ACCEL_ZERO_THRESHOLD   (uint32_t) (100)
#define REF1                   (int32_t)  (-30+1) // +1 to account for index starting at 1 in matlab
#define REF2                   (int32_t)  (-60+1)
#define MAX_HISTORY            (uint32_t) (300)
#define MAG_AXIS_DISABLE
//-----------------------------------------//
/* Private Macros */
#define SQUARED_F(num)         (float)    ((num) * (num))
#define DEG_TO_RAD_F(num)      (float)    ((num) * 3.141593f / 180.0f)
//-----------------------------------------//
/* Private Defines */
#ifdef MAG_AXIS_ENABLE
#define AXIS_COUNT             (uint32_t) (9)
#else
#define AXIS_COUNT             (uint32_t) (6)
#endif
#define G_AXIS_COUNT           (uint32_t) (3)
#define XL_AXIS_COUNT          (uint32_t) (3)
#define VECTOR_AXIS_COUNT      (uint32_t) (3)
#define HEAD                   (int32_t)  (0)
#define CURRENT                (uint32_t) (0)
#define PREVIOUS               (uint32_t) (1)

//-----------------------------------------//
/* Private types */
typedef enum{
	     GX_AXIS = 0,
	     GY_AXIS = 1,
	     GZ_AXIS = 2,
	     AX_AXIS = 3,
	     AY_AXIS = 4,
	     AZ_AXIS = 5,
#ifdef MAG_AXIS_ENABLE
	     TX_AXIS = 6,
	     TY_AXIS = 7,
	     TZ_AXIS = 8
#endif
} data_axis_t;

typedef enum{
	     X_AXIS,
	     Y_AXIS,
	     Z_AXIS
} vector_axis_t;

typedef struct{
  uint32_t head, tail, size;
  float GyXl_buffer[AXIS_COUNT][INERTIAL_BUFFER_SIZE];
} inertial_buf_t;

typedef struct{
  uint32_t head, tail, size;
  float Vector_buffer[VECTOR_AXIS_COUNT][VECTOR_BUFFER_SIZE];
} vector_buf_t;

typedef struct{
  uint32_t head, tail, size;
  float *buffer;
} circ_buf_t;

typedef struct{
  float G_Xdps_Zero;
  float G_Ydps_Zero;
  float G_Zdps_Zero;

  float G_dps_Noise;

  float Vectortol;
  float Acceltol;

  float MinimumSpeed;
  float MinimumWiggleTime;
  uint32_t WiggleCount;
  float MinimumAngle;
  float SpeedToStopTime;
  float OvershootAngle;
} cal_params_t;
/* ========= */
/* variables */

inertial_buf_t FullBuffer;
door_status_t DoorStatus[2];

//-----------------------------------------//
/* Private variables */

static vector_buf_t SmallBuffer;
static circ_buf_t GdpsBuffer;
static circ_buf_t ClosingSum;
static circ_buf_t OpeningSum;

static const cal_params_t Cal = {
				 .G_Xdps_Zero = 0.23f,
				 .G_Ydps_Zero = -1.52f,
				 .G_Zdps_Zero = 0.83f,
				 .G_dps_Noise = 3.0f,           // Gdps below this is consider noise
				 .Vectortol = 1.0f,             //
				 .Acceltol = 500.0f,            // If change in mounted accel greater than this, consider unmounted
				 .MinimumSpeed = 5.0f,         // Min dps to be considered moving
				 .MinimumWiggleTime =50.0f,   // How many samples of closing before opening motion and vice versa
				 .WiggleCount = 5,           // Number of change from open to close and back before considering a wiggle test type
				 .MinimumAngle = 2000.0f,       // Minimum Integrated angle speeds to consider a valid motion
				 .SpeedToStopTime = 300.0f,     // Number of Samples between speed and stop to consider a valid slam
				 .OvershootAngle = 500.0f       // Angle integration limit to consider latched or not
};

//-----------------------------------------//
/* Private Functions */

// @function description
// Initialise buffer parameters, zero out buffer
void init_inertial_buffer(inertial_buf_t *buffer){
  buffer->head = 0;
  buffer->tail = 0;
  buffer->size = INERTIAL_BUFFER_SIZE;
  memset(buffer->GyXl_buffer, 0, sizeof(buffer->GyXl_buffer));
}

// @function description
// Initialise buffer parameters, zero out buffer
void init_vector_buffer(vector_buf_t *buffer){
  buffer->head = 0;
  buffer->tail = 0;
  buffer->size = VECTOR_BUFFER_SIZE;
  memset(buffer->Vector_buffer, 0, sizeof(buffer->Vector_buffer));
}

// @function description
// Initialise buffer parameters, zero out buffer
// @params
// uint32_t buf_size - number of elements in buffer
void init_circ_buffer(circ_buf_t *buffer, uint32_t buf_size){
  buffer->head = 0;
  buffer->tail = 0;
  buffer->size = buf_size;
  buffer->buffer = calloc(buf_size, sizeof(float));
}

// @function description
// Finds index relative to head position
// @params
// void *buffer     - buffer in which to find position
// int32_t position - position relative to head
//                    -ve position = history, +ve postion = unprocessed data, 0 = HEAD = head position
// @return
// Returns index if valid else ERROR_FLAG
int32_t find_buffer_index(void *buffer, int32_t position){
  int32_t ret = (int32_t) ERROR_FLAG;
  circ_buf_t *tmp = (circ_buf_t *)buffer; // We are only interested in the head, tail size variable

  // Check if requesting processed data or new data
  if(position < HEAD){
    // Request history
    if(abs(position) <= MAX_HISTORY){
      if((int32_t) tmp->head >= abs(position)){
	// No wrap around
	ret = (int32_t) tmp->head + position;
      }else{
	// Wrap around
	ret = (int32_t) tmp->size + tmp->head + position;
      }
    }else{
      // Requesting older history than MAX_HISTORY
    }
  }else if(position == HEAD){
    // Request head
    ret = (int32_t) tmp->head;
  }else if(position > HEAD){
    // Request unprocessed data
    if((tmp->head + position) < (int32_t) tmp->size){
      // No wrap around
      ret = (int32_t) tmp->head + position;
    }else{
      ret = (int32_t) position - (tmp->size - tmp->head - 1);
    }
  }else{
    // Error condition - Should never come here
    ret = (int32_t) ERROR_FLAG;
  }
  return ret;
}

// @function description
// Find the position of of last saved history i.e. wrap(buffer->head - MAX_HISTORY)
// @params
// void *buffer - buffer in which to find position
// @return
// Returns index if valid else ERROR_FLAG
int32_t find_last_history_index(void *buffer){
  circ_buf_t *tmp = (circ_buf_t *)buffer; // Cast as we are only interested in the head, tail, size variables
  int32_t last_history_pos = ERROR_FLAG;
  // Find position of the last history
  if(tmp->head >= MAX_HISTORY){
    // There are at least MAX_HISTORY values before head pos
    last_history_pos = tmp->head - (int32_t) MAX_HISTORY;
  }else if(tmp->head < MAX_HISTORY){
    // History position needs to wrap around end of buffer
    last_history_pos = tmp->size + tmp->head - (int32_t) MAX_HISTORY;
  }else{
    // Error condition - should never come here
    last_history_pos = (int32_t) ERROR_FLAG;
  }
  return last_history_pos;
}

// @function description
// Update head to point to next new sample
// @param
// void *buffer - buffer in which to advance head  position
// @return
// Returns OK_FLAG if head updated
algo_error_t move_head_to_next(void *buffer){
  circ_buf_t *tmp = (circ_buf_t *)buffer; // Cast as we are only interested in the head, tail, size variables
  algo_error_t ret = ERROR_FLAG;
  if((tmp->head + 1) < tmp->size){
    // No wrap around
    if(tmp->head == tmp->tail){
      // Make sure tail does to advance ahead of head
      tmp->tail++;
    }
    tmp->head++;
    ret = OK_FLAG;
  }else if((tmp->head + 1) >= tmp->size){
    // Wrap around
    if(tmp->head == tmp->tail){
      // Make sure tail does to advance ahead of head
      tmp->tail = 0;
    }
    tmp->head = 0;
    ret = OK_FLAG;
  }else{
    // Error condition - Should never come here
    ret = (int32_t) ERROR_FLAG;
  }
  return ret;
}

// @function description
// Save history  - copy buffer[0] to buffer[1]
// @param
// door_status_t *buffer - status buffer to save history
void move_status_buffer_next(door_status_t *buffer){
  buffer[PREVIOUS].VectorMounting = buffer[CURRENT].VectorMounting;
  buffer[PREVIOUS].AccelMounting = buffer[CURRENT].AccelMounting;
  buffer[PREVIOUS].Direction = buffer[CURRENT].Direction;
  buffer[PREVIOUS].Moving = buffer[CURRENT].Moving;
  buffer[PREVIOUS].ReallyClosing = buffer[CURRENT].ReallyClosing;
  buffer[PREVIOUS].ReallyOpening = buffer[CURRENT].ReallyOpening;
  buffer[PREVIOUS].TestType = buffer[CURRENT].TestType;
  buffer[PREVIOUS].Opening = buffer[CURRENT].Opening;
  buffer[PREVIOUS].Closing = buffer[CURRENT].Closing;
  buffer[CURRENT].Moving = FALSE;
  buffer[CURRENT].Opening = FALSE;
  buffer[CURRENT].Closing = FALSE;
}

// @function description
// Find number of positions in buffer than can be overwritten
// @params
// void *buffer - buffer to find free space in
// @return
// Returns ERROR_FLAG if history index not found else number of free spaces
int32_t find_free_space(void *buffer){
  int32_t last_history_index = 0, space = 0;
  circ_buf_t *tmp = (circ_buf_t *) buffer; // Cast as we are only interested in the head, tail, size variables
 // ESP_LOGW(__func__, " size = %d, tail = %d, head = %d", tmp->size, tmp->tail, tmp->head);

  // Find free space
  last_history_index = find_last_history_index(tmp);
//  ESP_LOGW(__func__, "last history index = %d ", last_history_index);

  if(last_history_index == (int32_t) ERROR_FLAG){
    return ERROR_FLAG;
  }

  if(last_history_index >= (int32_t) tmp->tail){
    // Case: |T    Hist---H....|
    space = last_history_index - tmp->tail;
  }else if(last_history_index < (int32_t) tmp->tail){
    // Case: |Hist---H....T    |
    space = tmp->size - tmp->tail + last_history_index;
  }else{
    space = ERROR_FLAG;
  }
  return space;
}


// @function description
// Appends data to end of inertial buffer and updates tail position
// @params
// inertial_buf_t *buffer  - append destination
// float *data             - data to be appended, assumed to be in pattern Gx, Gy, Gz, Ax, Ay, Az
// uint32_t len            - total number of samples
// @return
// Returns ERROR_FLAG if space in buffer
algo_error_t add_to_inertial_buffer(inertial_buf_t *buffer, float *data, uint32_t len){
  // Data is stored in a circular array where:
  //   head is the position of data to be processed next
  //   tail is position newest data + 1
  //   last_history_pos is position of last processed data that is still needed
  // e.g.
  //    Head  Tail  histpos = wrap(head - MAX_HISTORY)
  //---- V     V      V---
  //|a|b|c|d|e|x|xxxx|....|
  // 0 1 2 3 4 . ......  INERTIAL_BUFFER_SIZE
  int32_t space = 0;
  uint32_t count = 0, i = 0;

  space = find_free_space((void *) buffer);

  if(space <= 0){
	ESP_LOGW(__func__, "Error getting space %d ", space);
    return ERROR_FLAG;
  }

  // Append data to buffer
  // TODO: Overflow data to be handled, currently discarded
  while(count < space && count < len){
    // Keep track on which axis is next in data
    if(i == AXIS_COUNT){
      i = 0;
      count++;
      if((buffer->tail + 1) >= buffer->size){
	buffer->tail = 0;
      }else{
	buffer->tail++;
      }
    }else{
      // Wrap around tail pos is end of buffer
      buffer->GyXl_buffer[i][buffer->tail] = data[count+i];
      i++;
    }
  }
  return OK_FLAG;
}

// @function description
// Appends 1 set of x,y,z vector data to end of vector buffer and updates tail position
// @params
// vector_buf_t *buffer  - append destination
// float *data           - data to be appended, assumed to be in same order as vector_axis_t (x, y, z)
// @return
// Returns ERROR_FLAG if no space in buffer
algo_error_t add_to_vector_buffer(vector_buf_t *buffer, float *data){
  int32_t  space = 0;
  uint32_t i = 0;

  space = find_free_space((void *) buffer);
  if(space <= 0){
    return ERROR_FLAG;
  }
  // Append data to buffer
  // TODO: Overflow data to be handled
  for(i=0; i<VECTOR_AXIS_COUNT; i++){
    buffer->Vector_buffer[i][buffer->tail] = data[i];
  }
  if((buffer->tail + 1) >= buffer->size){
    // Wrap around tail pos is end of buffer
    buffer->tail = 0;
  }else{
    buffer->tail++;
  }
  return OK_FLAG;
}

// @function description
// Appends data to end of circular buffer and updates tail position
// @params
// circ_buf_t *buffer  - append destination
// float *data         - data to be appended
// @return
// Returns ERROR_FLAG if no space in buffer
algo_error_t add_to_circ_buffer(circ_buf_t *buffer, float *data){
  int32_t space = 0;

  space = find_free_space((void *) buffer);

  if(space <= 0){
    return ERROR_FLAG;
  }
  // Append data to buffer
  // TODO: Overflow data to be handled
  buffer->buffer[buffer->tail] = *(data);
  if((buffer->tail + 1) >= buffer->size){
    // Wrap around tail pos is end of buffer
    buffer->tail = 0;
  }else{
    buffer->tail++;
  }
  return OK_FLAG;
}

// @function description
// Write data to single axis at head postion of vector buffer without updating head or tail
// @params
// vector_buf_t *buffer - write destination
// vector_axis_t axis   - desired axis
// float *data          - data to be appended
void write_to_vector_buffer_head(vector_buf_t *buffer, vector_axis_t axis, float *data){
  buffer->Vector_buffer[axis][buffer->head] = *data;
}

// @function description
// Write data to single axis at head postion of circular without updating head or tail
// @params
// circ_buf_t *buffer   - append destination
// float *data          - data to be appended
void write_to_circ_buffer_head(circ_buf_t *buffer, float *data){
  buffer->buffer[buffer->head] = *data;
}

// @function description
// Read data from single axis of inertial buffer relative to head position
// @params
// int32_t position - position relative to head
//                    -ve position = history, +ve postion = unprocessed data, 0 = HEAD = head position
// @return
// Return read value
float read_inertial_buffer(inertial_buf_t *buffer, data_axis_t axis, int32_t position){
  int32_t index = find_buffer_index(buffer, position);
  return (buffer->GyXl_buffer[axis][index]);
}

// @function description
// Read data from single axis of vector buffer relative to head position
// @params
// int32_t position - position relative to head
//                    -ve position = history, +ve postion = unprocessed data, 0 = HEAD = head position
// @return
// Return read value
float read_vector_buffer(vector_buf_t *buffer, vector_axis_t axis, int32_t position){
  int32_t index = find_buffer_index(buffer, position);
  return (buffer->Vector_buffer[axis][index]);
}

// @function description
// Read data from circular buffer relative to head position
// @params
// int32_t position - position relative to head
//                    -ve position = history, +ve postion = unprocessed data, 0 = HEAD = head position
// @return
// Return read value
float read_circ_buffer(circ_buf_t *buffer, int32_t position){
  int32_t index = find_buffer_index(buffer, position);
  return (buffer->buffer[index]);
}

// @function description
// Find max value in circular buffer from right to left
// @params
float max_f(float *array, uint32_t start_index, uint32_t search_size, uint32_t array_size){
  float max = array[start_index];
  int32_t count;
  for(count = 0; count < search_size; count++){
    if(start_index == 0){
      // If at head of array wrap around to end
      start_index = array_size;
    }
    if(array[start_index] > max){
      max = array[start_index];
    }
    start_index--;
  }
  return max;
}

// @function description
// Find index of first value that is greater than comparison from right to left (newest to oldest) in circular buffer
// @params
// float *array         - array to search in
// float comparison     - check greater than this calue
// uint32_t start_index - index of array to start search from
// uint32_t search_size - how many comparisons to make
// uint32_t array_size  - to ensure we do not go out of bounds and wrap when necessary
// @return
// Return first match else ERROR_FLAG
int32_t find_first_gt_f(float *array, float comparison, uint32_t start_index, uint32_t search_size, uint32_t array_size){
  int32_t count;
  int32_t ret = (int32_t) ERROR_FLAG;

  for(count = 0; count < search_size && ret == ERROR_FLAG; count++){
    if(array[start_index] > comparison){
      ret = start_index;
    }
    if(start_index == 0){
      // If at head of array wrap around to end
      start_index = array_size - 1;
    }else{
      start_index--;
    }
  }
  return ret;
}

// @function description
// Find index of first value that is less than comparison from right to left in circular buffer
// @params
// float *array         - array to search in
// float comparison     - check greater than this calue
// uint32_t start_index - index of array to start search from
// uint32_t search_size - how many comparisons to make
// uint32_t array_size  - to ensure we do not go out of bounds and wrap when necessary
// @return
// Return first match else ERROR_FLAG
int32_t find_first_lt_f(float *array, float comparison,	uint32_t start_index, uint32_t search_size, uint32_t array_size){
  int32_t count;
  int32_t ret = (int32_t) ERROR_FLAG;

  for(count = 0; count < search_size && ret == ERROR_FLAG; count++){
    if(array[start_index] < comparison){
      ret = start_index;
    }
    if(start_index == 0){
      // If at head of array wrap around to end
      start_index = array_size - 1;
    }else{
      start_index--;
    }
  }
  return ret;
}

// @function description
// Sort float array from smallest to largest (insertion)
// @params
// float *array        - array to be sorted (array at pointed address will be modified)
// uint32_t array_size - to ensure we do not go out of bounds and wrap when necessary
void sort_array_f(float *array, uint32_t array_size){
  int32_t index, j;
  float tmp;

  for(index = 1; index < array_size; index++){
    tmp = array[index];
    j = index - 1;
    while(tmp > array[j] && j >= 0){
      // While current larger than (current-1) shift current back
      array[j + 1] = array[j];
      j--;
    }
    array[j + 1] = tmp;
  }

  /* printf("Sorted array============\r\n"); */
  /* for(index = 0; index < array_size; index++){ */
  /*   printf("%d\t%f\r\n", index, array[index]); */
  /* } */
  /* printf("Sorted array============\r\n"); */

}

// @function description
// Median of set is the middle of the sorted set
// @params
// float *array        - pre-sorted array in which to find median
// uint32_t array_size - to ensure we do not go out of bounds and wrap when necessary
// @return
// Returns median of array larger than 2 elements else first element
float find_median_of_sorted_array(float *array, uint32_t array_size){
  float median = array[0];

  if(array_size > 1){
    if(array_size % 2 == 0){
      // Even
      median  = (array[(array_size / 2) - 1] + array[array_size / 2]) / 2;
    }else{
      // Odd
      median = array[(array_size - 1) / 2];
    }
  }
  return median;
}

// @function description
// Returns median of float array without modifiying input array
// @params
// float *array        - array in which to find median
// uint32_t array_size - to ensure we do not go out of bounds and wrap when necessary
// @return
// Returns median of array larger than 2 elements else first element
float median_f(float *array, uint32_t array_size){
  float *tmp = NULL;
  float ret = array[0];
  if(array_size > 1){
    tmp = (float *) calloc(array_size, sizeof(float));
    if(tmp == NULL){
      // ERROR
    }
    // Need to copy to maintain order of original array
    memcpy(tmp, array, array_size * sizeof(float));
    sort_array_f(tmp, array_size);

    ret = find_median_of_sorted_array(tmp, array_size);
    free(tmp);
  }
  return ret;
}

// @function description
// Returns average of float array
// @params
// float *array        - array in which to find average
// uint32_t array_size - to ensure we do not go out of bounds and wrap when necessary
// @return
// Returns average of array
float average_f(float *array, uint32_t array_size){
  uint32_t i;
  float ret = 0;
  if(array_size > 1){
    for(i = 0; i < array_size; i++){
      ret += array[i];
      /* printf("%4d\t%f\r\n", i, array[i]); */
    }
    ret /= (i + 1);
  }
  /* printf("\r\n"); */
  return ret;
}

// @function description
// Find difference between adjacent elements of array, apporixmates derivative
// @params
// float *array        - array perform diff on
// float *output       - array to store result, does not need to be initialised
// uint32_t array_size - to ensure we do not go out of bound
void calc_diff_array(float *array, float *output_array, uint32_t array_size){
  uint32_t i;
  if(array_size > 1){
    output_array = calloc(array_size - 1, sizeof(float));
    for(i = 0; (i + 1) < array_size; i++){
      output_array[i] = array[i + 1] - array[i];
    }
  }
}

// @function description
// Determine current motion of door based on current and past data
// @params
// cal_params_t Cal - contains gyro null offsets, thresholds, and tolerances required to discriminate door events
static algo_error_t analysis(cal_params_t Cal, analysis_result_t *analysis_result){
  // Helpers
  algo_error_t ret;
  static float tmp1, tmp2;
  static float tmp_array[3];
  static int32_t tmp_int, tmp_int2;
  static bool newResult = FALSE;

  // Set Counters
  static uint32_t Count_AccelZero = 0;
  static uint32_t Count_VectorZero = 0;
  static uint32_t Count_Measurement = 0;
  static uint32_t Count_SmoothTransistion = 0;

  // Set Values
  static float MountingVectorX = 0.0f;
  static float MountingVectorY = 0.0f;
  static float MountingVectorZ = 0.0f;
  static float MountingAccelX = 0.0f;
  static float MountingAccelY = 0.0f;
  static float MountingAccelZ = 0.0f;

  static float Sum_GX_Stable = 0.0f;
  static float Sum_GY_Stable = 0.0f;
  static float Sum_GZ_Stable = 0.0f;

  static float Sum_AccX_Stable = 0.0f;
  static float Sum_AccY_Stable = 0.0f;
  static float Sum_AccZ_Stable = 0.0f;

  static direction_t Positive_G_X = FORWARDS;
  static direction_t Positive_G_Y = FORWARDS;
  static direction_t Positive_G_Z = FORWARDS;

  static uint32_t WiggleTime = 0;    // Number of Samples to recognize one motion of wiggle

  static float CurrentRadius = 1.0f;  // Radius in m
  static float SpeedAngle =  2500.0f; // 5deg  per second

  static float DeltaX01 = 0.0f, DeltaX30 = 0.0f, DeltaX60 = 0.0f;
  static float DeltaY01 = 0.0f, DeltaY30 = 0.0f, DeltaY60 = 0.0f;
  static float DeltaZ01 = 0.0f, DeltaZ30 = 0.0f, DeltaZ60 = 0.0f;

  static int32_t lastValue = 0;
  static int32_t SpeedIndex = 0;
  static float Overshoot = 0.0f;

  // Wiggle Variables
  static uint32_t WiggleCount = 0;
  float A_Xmg_offset = 0.0f, A_Ymg_offset = 0.0f, A_Zmg_offset = 0.0f;
  static float A_3dps = 0.0f, A_3dps_diff = 0.0f, A_3mg = 0.0f;
  static float R_temp[4000] = {0};
  static uint32_t radius_count = 0;
  static float lastRadius = 0.0f;
  static float lastLastRadius = 0.0f;

  Result_Measurement = 0.0f;
  Result_LinearMeasurement = 0.0f;
  Result_MeasurementIsClosed = FALSE;

  // Get 3D Speed sqrt(x^2 + y^2 + z^2)
  //  =========================
  tmp1 = read_inertial_buffer(&FullBuffer, GX_AXIS, HEAD);
  tmp1 = SQUARED_F(tmp1);
  tmp2 = read_inertial_buffer(&FullBuffer, GY_AXIS, HEAD);
  tmp2 = SQUARED_F(tmp2);
  tmp1 += tmp2;
  tmp2 = read_inertial_buffer(&FullBuffer, GZ_AXIS, HEAD);
  tmp2 = SQUARED_F(tmp2);
  tmp1 += tmp2;
  tmp1 = sqrtf(tmp1);
  write_to_circ_buffer_head(&GdpsBuffer, &tmp1);
  // Detect Movement
  //  =========================
  if(   (read_circ_buffer(&GdpsBuffer, HEAD) > Cal.G_dps_Noise)
	|| (read_circ_buffer(&GdpsBuffer, REF1) > Cal.G_dps_Noise)
	|| (read_circ_buffer(&GdpsBuffer, REF2) > Cal.G_dps_Noise)){
    DoorStatus[CURRENT].Moving = MOVING;
  }else{
    DoorStatus[CURRENT].Moving = NOT_MOVING;
  }

  // Set Vector Mounting Value
  //  =========================
  if(DoorStatus[CURRENT].Moving == MOVING){
    if(   (read_circ_buffer(&GdpsBuffer, HEAD) > Cal.MinimumSpeed)
	  && (read_circ_buffer(&GdpsBuffer, REF1) > Cal.MinimumSpeed)
	  && (read_circ_buffer(&GdpsBuffer, REF2) > Cal.MinimumSpeed)){

      tmp1 = read_circ_buffer(&GdpsBuffer, HEAD);

      tmp2 = read_inertial_buffer(&FullBuffer, GX_AXIS, HEAD);
      tmp2 = tmp2 / tmp1;
      write_to_vector_buffer_head(&SmallBuffer, X_AXIS, &tmp2);

      tmp2 = read_inertial_buffer(&FullBuffer, GY_AXIS, HEAD);
      tmp2 = tmp2 / tmp1;
      write_to_vector_buffer_head(&SmallBuffer, Y_AXIS, &tmp2);

      tmp2 = read_inertial_buffer(&FullBuffer, GZ_AXIS, HEAD);
      tmp2 = tmp2 / tmp1;
      write_to_vector_buffer_head(&SmallBuffer, Z_AXIS, &tmp2);

      if(DoorStatus[CURRENT].VectorMounting == UNMOUNTED){
	Count_VectorZero++;
	Sum_GX_Stable += read_vector_buffer(&SmallBuffer, X_AXIS, HEAD);
	Sum_GY_Stable += read_vector_buffer(&SmallBuffer, Y_AXIS, HEAD);
	Sum_GZ_Stable += read_vector_buffer(&SmallBuffer, Z_AXIS, HEAD);

	if(Count_VectorZero == VECTORZERO_THRESHOLD){
	  DoorStatus[CURRENT].VectorMounting = MOUNTED;
	  // Vector for current mounting
	  MountingVectorX = Sum_GX_Stable / (float) Count_VectorZero;
	  MountingVectorY = Sum_GY_Stable / (float) Count_VectorZero;
	  MountingVectorZ = Sum_GZ_Stable / (float) Count_VectorZero;

	  // Detemine main axis of motion
	  if((fabsf(MountingVectorX) > fabsf(MountingVectorY)) && (fabsf(MountingVectorX) > fabsf(MountingVectorZ))){
	    // CHECK EQUIVALENCE to matlab code : Positive_G_X = MountingVectorX / abs(MountingVectorX);
	    (MountingVectorX < (float) STEADY)? (Positive_G_X = BACKWARDS) : (Positive_G_X = FORWARDS);
	    Positive_G_Y = STEADY;
	    Positive_G_Z = STEADY;
	  }else if(fabsf(MountingVectorY) > fabsf(MountingVectorZ)){
	    Positive_G_X = STEADY;
	    (MountingVectorY < (float) STEADY)? (Positive_G_Y = BACKWARDS) : (Positive_G_Y = FORWARDS);
	    Positive_G_Z = STEADY;
	  }else{
	    Positive_G_X = STEADY;
	    Positive_G_Y = STEADY;
	    (MountingVectorZ < (float) STEADY)? (Positive_G_Z = BACKWARDS) : (Positive_G_Z = FORWARDS);
	  }
	}
      }else{
	// Check for a change in main axis of motion
	if(Positive_G_X != STEADY){
	  DeltaX01 = fabsf(fabsf(MountingVectorX) - fabsf(read_vector_buffer(&SmallBuffer, X_AXIS, HEAD)));
	  DeltaX30 = fabsf(fabsf(MountingVectorX) - fabsf(read_vector_buffer(&SmallBuffer, X_AXIS, REF1)));
	  DeltaX60 = fabsf(fabsf(MountingVectorX) - fabsf(read_vector_buffer(&SmallBuffer, X_AXIS, REF2)));
	  if((DeltaX01 > Cal.Vectortol) && (DeltaX30 > Cal.Vectortol) && (DeltaX60 > Cal.Vectortol)){
	    DoorStatus[CURRENT].VectorMounting = UNMOUNTED;
	    DoorStatus[CURRENT].AccelMounting = UNMOUNTED;
	    // This is the end of the mounting. Send end.
	  }
	}
	if(Positive_G_Y != STEADY){
	  DeltaY01 = fabsf(fabsf(MountingVectorY) - fabsf(read_vector_buffer(&SmallBuffer, Y_AXIS, HEAD)));
	  DeltaY30 = fabsf(fabsf(MountingVectorY) - fabsf(read_vector_buffer(&SmallBuffer, Y_AXIS, REF1)));
	  DeltaY60 = fabsf(fabsf(MountingVectorY) - fabsf(read_vector_buffer(&SmallBuffer, Y_AXIS, REF2)));
	  if((DeltaY01 > Cal.Vectortol) && (DeltaY30 > Cal.Vectortol) && (DeltaY60> Cal.Vectortol)){
	    DoorStatus[CURRENT].VectorMounting = UNMOUNTED;
	    DoorStatus[CURRENT].AccelMounting = UNMOUNTED;
	    // This is the end of the mounting. Send end.
	  }
	}
	if(Positive_G_Z != STEADY){
	  DeltaZ01 = fabsf(fabsf(MountingVectorZ) - fabsf(read_vector_buffer(&SmallBuffer, Z_AXIS, HEAD)));
	  DeltaZ30 = fabsf(fabsf(MountingVectorZ) - fabsf(read_vector_buffer(&SmallBuffer, Z_AXIS, REF1)));
	  DeltaZ60 = fabsf(fabsf(MountingVectorZ) - fabsf(read_vector_buffer(&SmallBuffer, Z_AXIS, REF2)));
	  if((DeltaZ01 > Cal.Vectortol) && (DeltaZ30 > Cal.Vectortol) && (DeltaZ60> Cal.Vectortol)){
	    DoorStatus[CURRENT].VectorMounting = UNMOUNTED;
	    DoorStatus[CURRENT].AccelMounting = UNMOUNTED;
	    // This is the end of the mounting. Send end.
	  }
	}
      }
    }else{
      Sum_GX_Stable = 0.0f;
      Sum_GY_Stable = 0.0f;
      Sum_GZ_Stable = 0.0f;
      Count_VectorZero = 0;
      tmp1 = 0.0f;
      write_to_vector_buffer_head(&SmallBuffer, X_AXIS, &tmp1);
      write_to_vector_buffer_head(&SmallBuffer, Y_AXIS, &tmp1);
      write_to_vector_buffer_head(&SmallBuffer, Z_AXIS, &tmp1);
    }
  }else{
    tmp1 = 0.0f;
    write_to_vector_buffer_head(&SmallBuffer, X_AXIS, &tmp1);
    write_to_vector_buffer_head(&SmallBuffer, Y_AXIS, &tmp1);
    write_to_vector_buffer_head(&SmallBuffer, Z_AXIS, &tmp1);
  }
  // Set Accel Mounting Value
  //  ========================
  //if(DoorStatus[CURRENT].VectorMounting == MOUNTED){ // Before Change. Needed to evaluated mountingaccel so that it is present when retquired later
  if(DoorStatus[CURRENT].AccelMounting == UNMOUNTED){
    if(DoorStatus[CURRENT].Moving == NOT_MOVING){
      Count_AccelZero++;
      Sum_AccX_Stable += read_inertial_buffer(&FullBuffer, AX_AXIS, HEAD);
      Sum_AccY_Stable += read_inertial_buffer(&FullBuffer, AY_AXIS, HEAD);
      Sum_AccZ_Stable += read_inertial_buffer(&FullBuffer, AZ_AXIS, HEAD);
    }else{
      Count_AccelZero = 0;
      Sum_AccX_Stable = 0.0f;
      Sum_AccY_Stable = 0.0f;
      Sum_AccZ_Stable = 0.0f;
    }
    if(Count_AccelZero != 0){
      MountingAccelX = Sum_AccX_Stable / (float) Count_AccelZero;
      MountingAccelY = Sum_AccY_Stable / (float) Count_AccelZero;
      MountingAccelZ = Sum_AccZ_Stable / (float) Count_AccelZero;
    }
    if(Count_AccelZero == ACCEL_ZERO_THRESHOLD){
      /* 	MountingAccelX = Sum_AccX_Stable / (float) Count_AccelZero;
		MountingAccelY = Sum_AccY_Stable / (float) Count_AccelZero;
		MountingAccelZ = Sum_AccZ_Stable / (float) Count_AccelZero; */
      DoorStatus[CURRENT].AccelMounting = MOUNTED;
    }
  }else{
    // check if accel moved
    if(DoorStatus[CURRENT].Moving == NOT_MOVING){
      if(   (fabsf(read_inertial_buffer(&FullBuffer, AX_AXIS, HEAD) - MountingAccelX) > Cal.Acceltol)
	    && (fabsf(read_inertial_buffer(&FullBuffer, AY_AXIS, HEAD) - MountingAccelY) > Cal.Acceltol)
	    && (fabsf(read_inertial_buffer(&FullBuffer, AZ_AXIS, HEAD) - MountingAccelZ) > Cal.Acceltol)
	    && (fabsf(read_inertial_buffer(&FullBuffer, AX_AXIS, REF1) - MountingAccelX) > Cal.Acceltol)
	    && (fabsf(read_inertial_buffer(&FullBuffer, AY_AXIS, REF1) - MountingAccelY) > Cal.Acceltol)
	    && (fabsf(read_inertial_buffer(&FullBuffer, AZ_AXIS, REF1) - MountingAccelZ) > Cal.Acceltol)
	    && (fabsf(read_inertial_buffer(&FullBuffer, AX_AXIS, REF2) - MountingAccelX) > Cal.Acceltol)
	    && (fabsf(read_inertial_buffer(&FullBuffer, AY_AXIS, REF2) - MountingAccelY) > Cal.Acceltol)
	    && (fabsf(read_inertial_buffer(&FullBuffer, AZ_AXIS, REF2) - MountingAccelZ) > Cal.Acceltol)){
	DoorStatus[CURRENT].VectorMounting = UNMOUNTED;
	DoorStatus[CURRENT].AccelMounting = UNMOUNTED;
	// This is the end of the mounting. Send end.
      }
    }
  }

  //} //Before change JF
  // Detect Open Or Close Direction
  //  ========================
  if(   (DoorStatus[CURRENT].Moving == MOVING) && (DoorStatus[CURRENT].VectorMounting == MOUNTED)){
    if(   (((float) Positive_G_X * read_inertial_buffer(&FullBuffer, GX_AXIS, HEAD)) > 0.0f)
	  || (((float) Positive_G_Y * read_inertial_buffer(&FullBuffer, GY_AXIS, HEAD)) > 0.0f)
	  || (((float) Positive_G_Z * read_inertial_buffer(&FullBuffer, GZ_AXIS, HEAD)) > 0.0f)){
      DoorStatus[CURRENT].Opening = TRUE;
      DoorStatus[CURRENT].Closing = FALSE;
      tmp1 = read_circ_buffer(&OpeningSum, HEAD - 1) + read_circ_buffer(&GdpsBuffer, HEAD);
      write_to_circ_buffer_head(&OpeningSum, &tmp1);
    }else{
      DoorStatus[CURRENT].Opening = FALSE;
      DoorStatus[CURRENT].Closing = TRUE;
      tmp1 = read_circ_buffer(&ClosingSum, HEAD - 1) + read_circ_buffer(&GdpsBuffer, HEAD);
      write_to_circ_buffer_head(&ClosingSum, &tmp1);
    }
  }
  // Detect Wiggle
  //  ========================
  if((DoorStatus[CURRENT].Moving == MOVING) && (DoorStatus[CURRENT].VectorMounting == MOUNTED)){
    if (   (DoorStatus[CURRENT].Opening == TRUE && DoorStatus[PREVIOUS].Closing == TRUE)
	   || (DoorStatus[PREVIOUS].Opening == TRUE && DoorStatus[CURRENT].Closing == TRUE)){
      if(WiggleTime > Cal.MinimumWiggleTime){
	Count_SmoothTransistion++;
      }
      WiggleTime = 0;
    }else{
      WiggleTime++;
    }
  }

  // Set Test Type
  //  ========================
  if(Count_SmoothTransistion > Cal.WiggleCount){
    DoorStatus[CURRENT].TestType = WIGGLE;
  }else{
    DoorStatus[CURRENT].TestType = SLAM;
  }

  // Integration of Movement
  //  ========================
  if((DoorStatus[CURRENT].Moving == MOVING) && (DoorStatus[CURRENT].VectorMounting == MOUNTED)){
    if(read_circ_buffer(&ClosingSum, HEAD) > Cal.MinimumAngle){
      DoorStatus[CURRENT].ReallyClosing = TRUE;
      DoorStatus[CURRENT].ReallyOpening = FALSE;
      if(DoorStatus[CURRENT].Closing == TRUE){
	tmp1 = 0.0f;
	write_to_circ_buffer_head(&OpeningSum, &tmp1);
      }
    }else if(read_circ_buffer(&OpeningSum, HEAD) > Cal.MinimumAngle){
      DoorStatus[CURRENT].ReallyClosing = FALSE;
      DoorStatus[CURRENT].ReallyOpening = TRUE;
      if(DoorStatus[CURRENT].Opening == TRUE){
	tmp1 = 0.0f;
	write_to_circ_buffer_head(&ClosingSum, &tmp1);
      }
    }else{
      DoorStatus[CURRENT].ReallyOpening = DoorStatus[PREVIOUS].ReallyOpening;
      DoorStatus[CURRENT].ReallyClosing = DoorStatus[PREVIOUS].ReallyClosing;
    }
  }
  // Detect Measurement
  //  ========================
  // THis section is really confusing - ask Tom for the logic; He wrote the initial matlab version.
  if(DoorStatus[CURRENT].Moving == NOT_MOVING){
    if(DoorStatus[CURRENT].TestType == SLAM){
      Count_SmoothTransistion = 0;
      if(DoorStatus[CURRENT].ReallyClosing == TRUE){
	// Find first from head to max history greater than 0
	tmp_int = find_buffer_index(&GdpsBuffer, HEAD);
	lastValue = find_first_gt_f(ClosingSum.buffer, 0.0f, tmp_int, (MAX_HISTORY + 1), ClosingSum.size);
	// Find num of measurement from 'first greater than 0' and oldest measurement
	tmp_int = find_last_history_index(&ClosingSum) - lastValue;
	(tmp_int > 0)? (tmp_int = ClosingSum.size - tmp_int + 1) : (tmp_int = abs(tmp_int) + 1);
	// Find value at 'first greater than 0' index minus threshold
	tmp1 = (ClosingSum.buffer[lastValue] - SpeedAngle);
	// Find number of values between above and next value which is less than lastValue
	SpeedIndex = find_first_lt_f(ClosingSum.buffer, tmp1, (uint32_t) lastValue, tmp_int, ClosingSum.size);
	tmp_int2 = find_buffer_index(&ClosingSum, HEAD) - SpeedIndex ;

	if(!(tmp_int2 > 0)) {
	  (tmp_int2 = ClosingSum.size + tmp_int2);
	}
	if(tmp_int2 < Cal.SpeedToStopTime){
	  Count_Measurement++;
	  tmp_int = find_buffer_index(&OpeningSum, HEAD);
	  Overshoot = max_f(OpeningSum.buffer, (uint32_t) tmp_int, tmp_int2, OpeningSum.size);
	  if(Overshoot < Cal.OvershootAngle){
	    Result_MeasurementIsClosed = TRUE;
	  }else{
	    Result_MeasurementIsClosed  = FALSE;
	  }
	  Result_Measurement = read_circ_buffer(&GdpsBuffer, HEAD - tmp_int2);
	  Result_LinearMeasurement = DEG_TO_RAD_F(Result_Measurement) * CurrentRadius;
	  newResult = TRUE;
	}
	DoorStatus[CURRENT].Direction = FORWARDS;
	DoorStatus[CURRENT].ReallyClosing = FALSE;
	DoorStatus[CURRENT].ReallyOpening = FALSE;
	tmp1 = 0.0f;
	write_to_circ_buffer_head(&ClosingSum, &tmp1);
	write_to_circ_buffer_head(&OpeningSum, &tmp1);
      }else if(DoorStatus[CURRENT].ReallyOpening == TRUE){
	// Stopped after opening, wrong direction
	tmp_int = find_buffer_index(&OpeningSum, HEAD);
	lastValue = find_first_gt_f(OpeningSum.buffer, 0.0f, (uint32_t) tmp_int, (MAX_HISTORY + 1), OpeningSum.size);
	tmp_int = find_last_history_index(&OpeningSum) - lastValue;
	// Number of elements between last history and last value inclusive
	(tmp_int > 0)? (tmp_int = OpeningSum.size - tmp_int + 1) : (tmp_int = abs(tmp_int) + 1);
	tmp1 = (OpeningSum.buffer[lastValue] - SpeedAngle);
	SpeedIndex = find_first_lt_f(OpeningSum.buffer, tmp1, (uint32_t) lastValue, tmp_int, OpeningSum.size);
	tmp_int = find_buffer_index(&OpeningSum, HEAD);
	tmp_int2 = tmp_int - SpeedIndex;
	// Number of element between head and speedIndex inclusive
	if(!(tmp_int2 > 0)){
	  (tmp_int2 = OpeningSum.size + tmp_int2);
	}

	if(tmp_int2 < Cal.SpeedToStopTime){
	  Count_Measurement++;
	  tmp_int = find_buffer_index(&ClosingSum, HEAD);
	  Overshoot = max_f(ClosingSum.buffer, (uint32_t) tmp_int, tmp_int2, ClosingSum.size);
	  if(Overshoot < Cal.OvershootAngle){
	    Result_MeasurementIsClosed = TRUE;
	  }else{
	    Result_MeasurementIsClosed = FALSE;
	  }
	  Result_Measurement = read_circ_buffer(&GdpsBuffer, HEAD - tmp_int2+3); // JF CHECK WHERE +3 COMES FROM

	  Result_LinearMeasurement = DEG_TO_RAD_F(Result_Measurement) * CurrentRadius;
	  Positive_G_X *= -1;
	  Positive_G_Y *= -1;
	  Positive_G_Z *= -1;

	  newResult = TRUE;
	}
	DoorStatus[CURRENT].Direction = FORWARDS;
	DoorStatus[CURRENT].ReallyClosing = FALSE;
	DoorStatus[CURRENT].ReallyOpening = FALSE;
	tmp1 = 0.0f;
	write_to_circ_buffer_head(&ClosingSum, &tmp1);
	write_to_circ_buffer_head(&OpeningSum, &tmp1);
      }
    }else if(DoorStatus[CURRENT].TestType == WIGGLE){
      if(DoorStatus[CURRENT].AccelMounting == MOUNTED){
	Count_SmoothTransistion = 0;
	WiggleCount = 0;
      }
    }
  }
  // Calculation of Wiggle radius
  //  ========================
  /*
    if(DoorStatus[CURRENT].TestType == WIGGLE){
    A_3dps = fabsf(read_circ_buffer(&GdpsBuffer, HEAD) - read_circ_buffer(&GdpsBuffer, HEAD - 1)) * 416.0;
    A_Xmg_offset = read_inertial_buffer(&FullBuffer, AX_AXIS, HEAD) - MountingAccelX;
    A_Ymg_offset = read_inertial_buffer(&FullBuffer, AY_AXIS, HEAD) - MountingAccelY;
    A_Zmg_offset = read_inertial_buffer(&FullBuffer, AZ_AXIS, HEAD) - MountingAccelZ;
    A_3mg = SQUARED_F(A_Xmg_offset) + SQUARED_F(A_Ymg_offset) + SQUARED_F(A_Zmg_offset);
    A_3mg = sqrtf(A_3mg) * 9.8;
    tmp1 = A_3mg / A_3dps;
    //tmp1 = (A_3mg/1000.0f*9.8f / SQUARED_F(A_3dps));
    //    tmp1 = A_3mg/1000.0*9.8/A_3dps*(100.0/416.0);
    if( tmp1 <= 4.0f && radius_count < 4000){
    //if(A_3dps <= 2 && A_3dps >= 0.05 &&  tmp1 >= 0.5 && radius_count < 4000){
    R_temp[radius_count] = tmp1;
    printf("radius %f\t", tmp1);
    printf("3dps %f\t",A_3dps);
    printf("3mg %f\t",A_3mg); 
    //	printf("Mounting X %f\t", MountingAccelX); 
    //	printf("Mounting Y %f\t", MountingAccelY); 
    //	printf("Mounting Z %f\r\t", MountingAccelZ);
    printf("\r\n");      
    radius_count++;
    }
    if(radius_count > 3999){
    DoorStatus[CURRENT].TestType = SLAM;
    WiggleTime = 0;
    Count_SmoothTransistion = 0;
    }
    WiggleCount++;
    }
  */
  if(DoorStatus[CURRENT].TestType == WIGGLE
     /*&& (DoorStatus[CURRENT].ReallyClosing == TRUE || DoorStatus[CURRENT].ReallyClosing == TRUE) */){
    if(WiggleCount > 3+1 ){
      // Average last three difference values for each axis
      for(tmp_int = 0; tmp_int < 3; tmp_int++){
	A_3dps_diff += read_circ_buffer(&GdpsBuffer, HEAD - tmp_int) - read_circ_buffer(&GdpsBuffer, HEAD - tmp_int - 1);
      }
      A_3dps_diff /= tmp_int;
      // Null sample with 'mounted & idle' acceleration measurement
      A_Xmg_offset = read_inertial_buffer(&FullBuffer, AX_AXIS, HEAD) - MountingAccelX;
      A_Ymg_offset = read_inertial_buffer(&FullBuffer, AY_AXIS, HEAD) - MountingAccelY;
      A_Zmg_offset = read_inertial_buffer(&FullBuffer, AZ_AXIS, HEAD) - MountingAccelZ;
      A_3mg = sqrtf((SQUARED_F(A_Xmg_offset) + SQUARED_F(A_Ymg_offset) + SQUARED_F(A_Zmg_offset))) / 1000.0 * 9.8;
      // Average last three values for each axis
      for(tmp_int = 0; tmp_int <3; tmp_int++){
	A_3dps += read_circ_buffer(&GdpsBuffer, HEAD - tmp_int);
      }
      A_3dps /= tmp_int;
      // Save previous results
      lastLastRadius = lastRadius;
      lastRadius = tmp1;
      // Calculate radius
      tmp1 = A_3mg / sqrt(SQUARED_F(SQUARED_F(A_3dps)) + SQUARED_F(A_3dps_diff)) ;

      R_temp[radius_count] = tmp1;
      // Only record if radius values make sense
      if(lastRadius > tmp1 && lastRadius >= lastLastRadius && tmp1 >= 0.5){
	// moved over peak - record last value i.e. peak
	radius_count++;
	printf("radius %f\t", tmp1);
	printf("3dpsdiff %f\t",A_3dps_diff);
	//printf("3mg %f\t",A_3mg);
	printf("\r\n");
      }
      if(radius_count > 1000){
	DoorStatus[CURRENT].TestType = SLAM;
	WiggleTime = 0;
	Count_SmoothTransistion = 0;
      }
    }
    WiggleCount++;
  }
  // Reset state for change to SLAM measurement
  if(DoorStatus[PREVIOUS].TestType == WIGGLE && DoorStatus[CURRENT].TestType == SLAM){
    CurrentRadius = median_f(R_temp, radius_count);
    printf(" Wiggle: Med CurrentRad = %f \t", CurrentRadius);
    //CurrentRadius = average_f(R_temp, radius_count);
    printf(" Wiggle: Avg CurrentRad = %f \t", CurrentRadius);
    radius_count = 0;
    A_3dps_diff = 0;
    A_Xmg_offset = 0;
    A_Ymg_offset = 0;
    A_Zmg_offset = 0;
    A_3dps_diff = 0;
    A_3dps = 0;
    WiggleCount = 0;
    printf("\r\n");
  }
  // Values to compare with Matlab output to check for output correctness
  /*
    printf("VectorM\t %d ", (int) DoorStatus[0].VectorMounting); 
    printf("AccelM\t %d ", (int) DoorStatus[0].AccelMounting); 
    printf("Direction\t %d ", (int) DoorStatus[0].Direction); 
    printf("Moving\t %d ", (int) DoorStatus[0].Moving); 
    printf("Opening\t %d ", (int) DoorStatus[0].Opening); 
    printf("Closing\t %d ", (int) DoorStatus[0].Closing); 
    printf("ReallyOpening\t %d ", (int) DoorStatus[0].ReallyOpening); 
    printf("ReallyClosing\t %d ", (int) DoorStatus[0].ReallyClosing); 
    printf("TestType\t %d ", (int) DoorStatus[0].TestType); 
    printf("Cnt Msr \t %d ", Count_Measurement); 
    printf("Result_MeasurementIsClosed\t %d ", (int) Result_MeasurementIsClosed); 
    printf("Result_Linear\t %d ", (int) Result_LinearMeasurement); 
    printf("Result_Measurement\t %d ", (int) Result_Measurement); 
  */
  //Section to compare output with matlab

  /* if( DoorStatus[CURRENT].VectorMounting == MOUNTED){ */
  /*   printf("Mounted YES *"); */
  /* }else{ */
  /*   printf("Mounted NO  *"); */
  /* } */

  /* if( DoorStatus[CURRENT].Opening == TRUE){ */
  /*   printf("Open  Moving *"); */
  /* }else if( DoorStatus[CURRENT].Closing == TRUE){ */
  /*   printf("Close Moving *"); */
  /* } else { */
  /*   printf("Not   Moving"); */
  /* } */
  /* if(DoorStatus[CURRENT].ReallyClosing == TRUE){ */
  /*   printf(" Closing Motion *"); */
  /* }else if( DoorStatus[CURRENT].ReallyOpening == TRUE){ */
  /*   printf(" Opening Motion *"); */
  /* }else{ */
  /*   printf(" Small   Motion *"); */
  /* } */
  /* if(DoorStatus[CURRENT].TestType == SLAM){ */
  /*   printf(" Slam Mode   *"); */
  /* }else{ */
  /*   printf(" Wiggle Mode *"); */
  /* } */
  /* printf("\n\r"); */
  if(Count_Measurement > 0 && newResult  == TRUE){
    printf(" Measurement [dps] :%f *", Result_Measurement);
    if(Result_MeasurementIsClosed == TRUE){
      printf(" Is  Latched *");
    }else{
      printf(" Not Latched *");
    }
    printf("Linear [m/s] : %f", Result_LinearMeasurement);
    newResult = FALSE;
    printf("\r\n");

    //xEventGroupSetBits(deviceStatusGroup, SCREEN_NEEDS_UPDATE); // Trigger screen update
    //send_algo_result(Result_Measurement, Result_MeasurementIsClosed, Result_LinearMeasurement);
  }else{
    //printf("No data\n\r");
  }
  /*
    printf("Full: %f/%f/%f ", read_vector_buffer(&SmallBuffer,X_AXIS, HEAD),read_vector_buffer(&SmallBuffer, Y_AXIS,HEAD),read_vector_buffer(&SmallBuffer,Z_AXIS, HEAD));
    printf("gpds head \t %f ", read_circ_buffer(&GdpsBuffer, HEAD));
    printf("cls sum \t %f ", read_circ_buffer(&ClosingSum, HEAD));
    printf("opn sum \t %f ", read_circ_buffer(&OpeningSum, HEAD));
  */

  //printf("\r\n");



  memcpy((void *) &(analysis_result->door_status), (void *) &DoorStatus[CURRENT], sizeof(door_status_t));
  analysis_result->measurement = Result_Measurement;
  analysis_result->linear_measurement = Result_LinearMeasurement;
  analysis_result->measurement_is_closed = Result_MeasurementIsClosed;
  // Copy current door status
  move_buffers_to_next();

  return OK_FLAG;
}
/*
 * Convert measurements into a byte array message and add to transmit queue - NOT IN STANDARD FORMAT, FOR DEBUG USE
 *
 */
void send_algo_result(float Result_Measurement, bool Result_MeasurementIsClosed, float Result_LinearMeasurement){
  message_package_t message_package;
  float_int_byte_union_t measurement;

  message_package.length = 15;
  message_package.message[MSG_HEADER_POS]  = MSG_HEADER;
  message_package.message[SERIAL_NUM_LOWER_BYTE_POS]  =  SERIAL_NUM_LOWER_BYTE;
  message_package.message[SERIAL_NUM_UPPER_BYTE_POS]  =  SERIAL_NUM_UPPER_BYTE;
  message_package.message[COMMAND_LOWER_BYTE_POS]  =  'M';
  message_package.message[COMMAND_UPPER_BYTE_POS]  =  'M';

  // Float to byte
  measurement.num_f = Result_Measurement;
  message_package.message[DATA_START_POS]  =  measurement.num_byte[3];
  message_package.message[6]  =  measurement.num_byte[2];
  message_package.message[7]  =  measurement.num_byte[1];
  message_package.message[8]  =  measurement.num_byte[0];

  message_package.message[9]  =  (Result_MeasurementIsClosed? 1:0);

  // Float to byte
  measurement.num_f = Result_LinearMeasurement;
  message_package.message[10]  =   measurement.num_byte[3];
  message_package.message[11]  =   measurement.num_byte[2];
  message_package.message[12]  =   measurement.num_byte[1];
  message_package.message[13]  =   measurement.num_byte[0];

  message_package.message[14]  =  MSG_FOOTER;


  //xQueueSendToBack(transmit_queue, (void *) &message_package, portMAX_DELAY);
}

void init_buffers(void){
  // Init buffers
  init_inertial_buffer(&FullBuffer);
  init_vector_buffer(&SmallBuffer);
  init_circ_buffer(&GdpsBuffer, INERTIAL_BUFFER_SIZE);
  init_circ_buffer(&ClosingSum, INERTIAL_BUFFER_SIZE);
  init_circ_buffer(&OpeningSum, INERTIAL_BUFFER_SIZE);
  memset(DoorStatus, 0, sizeof(DoorStatus));

}
void move_buffers_to_next(void){
  static float tmp;
  // Shift buffers
  if(move_head_to_next(&FullBuffer) != OK_FLAG){
    // Error handler
  }
  if(move_head_to_next(&SmallBuffer) != OK_FLAG){
    // Error handler
  }
  if(move_head_to_next(&GdpsBuffer) != OK_FLAG){
    // Error handler
  }
  if(move_head_to_next(&ClosingSum) != OK_FLAG){
    // Error handler
  }
  tmp = read_circ_buffer(&ClosingSum, HEAD - 1);
  write_to_circ_buffer_head(&ClosingSum, &tmp);
  if(move_head_to_next(&OpeningSum) != OK_FLAG){
    // Error handler
  }
  tmp = read_circ_buffer(&OpeningSum, HEAD - 1);
  write_to_circ_buffer_head(&OpeningSum, &tmp);
  move_status_buffer_next(DoorStatus);
  //printf("moved buffers\r\n");
}
// Function to add accel/gyro measurements from prerecorded samples to buffer and call analysis on values
// Prerecorded values in series xxx .h files
void series1_test(void){
  int i;
  float tmp_array[6] = {0};
  init_buffers();
  for(i = 0; i < sizeof(GX_dps)/sizeof(float); i++){
    //printf("sample %d \t", i);

    tmp_array[0] = GX_dps[i]-Cal.G_Xdps_Zero;
    tmp_array[1] = GY_dps[i]-Cal.G_Ydps_Zero;
    tmp_array[2] = GZ_dps[i]-Cal.G_Zdps_Zero;
    tmp_array[3] = AX_mg[i];
    tmp_array[4] = AY_mg[i];
    tmp_array[5] = AZ_mg[i];

    add_to_inertial_buffer(&FullBuffer, tmp_array, 1);
    //analysis(Cal, NULL);

  }
  printf("END\r\n");
  while(1);
}

// Function to add xlgy values to processing buffer then call processing algorithm
algo_error_t process_inertial_measurement(float *acceleration_mg, float *angular_rate_mdps, analysis_result_t *analysis_result){
  float tmp_array[6] = {0};
  tmp_array[0] = (angular_rate_mdps[0])/1000.0f - Cal.G_Xdps_Zero;
  tmp_array[1] = (angular_rate_mdps[1])/1000.0f - Cal.G_Ydps_Zero;
  tmp_array[2] = (angular_rate_mdps[2])/1000.0f - Cal.G_Zdps_Zero;
  tmp_array[3] = acceleration_mg[0];
  tmp_array[4] = acceleration_mg[1];
  tmp_array[5] = acceleration_mg[2];
  //printf("GY xyz %f\t%f\t%f\t XL xyz %f\t%f\t%f\r\n", tmp_array[0],tmp_array[1],tmp_array[2],tmp_array[3],tmp_array[4],tmp_array[5]);
  algo_error_t err = add_to_inertial_buffer(&FullBuffer, tmp_array, 1);

  if(err != OK_FLAG){
	ESP_LOGW(__func__, "Error Adding to the inertial buffer ");//comment out
    return err;
  }

  err = analysis(Cal, analysis_result);
  if(err != OK_FLAG){
	  ESP_LOGW(__func__," Error analysing");
    return err;
  }
  return OK_FLAG;
}
