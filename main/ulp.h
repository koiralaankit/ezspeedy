/*
 * ulp.h
 *
 *  Created on: Jul 10, 2020
 *      Author: JFung
 */

#ifndef MAIN_ULP_H_
#define MAIN_ULP_H_
// ULP
#include "soc/rtc_cntl_reg.h"
#include "soc/sens_reg.h"
#include "soc/rtc_periph.h"
#include "driver/rtc_io.h"
#include "esp32/ulp.h"
#include "ulp_main.h"
#include "esp_sleep.h"
#include "soc/rtc_cntl_reg.h"

#define TRANSIT_MODE_KEY "transmode"

void update_pulse_count(void);
void init_ulp_program(void);


#endif /* MAIN_ULP_H_ */
